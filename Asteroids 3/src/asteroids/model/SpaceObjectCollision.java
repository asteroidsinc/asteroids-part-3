/**
 * 
 */
package asteroids.model;

import asteroids.view.CollisionListener;

/**
 * A class for dealing with space object collisions, containing all methods affecting the participating space objects,
 * such as time to collision, collision position and resolving the collision.
 * 
 * @invar	The space objects of this collision must be valid space objects for a collision.
 * 			| hasProperSpaceObjects()
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 *
 */
public abstract class SpaceObjectCollision extends Collision {

	/**
	 * Initialise the new collision with given first and second space objects,
	 * 
	 * @param 	objectOne
	 * 			The first space object of the new collision.
	 * @param 	objectTwo
	 * 			The first space object of the new collision.
	 * @post	The first space object of the new collision is equal to the given space object.
	 * 			| (new this).getObjectOne() == objectOne
	 * @post	The second space object of the new collision is equal to the given space object.
	 * 			| (new this).getObjectTwo() == objectTwo
	 * @throws	IllegalArgumentException
	 * 			The second space object is null
	 * 			| objectTwo == null
	 */
	public SpaceObjectCollision(SpaceObject objectOne, SpaceObject objectTwo) throws IllegalArgumentException {
		super(objectOne, objectTwo);
	}
		
	/**
	 * Returns the remaining time until the space objects collides.
	 * 
	 * @return	The time until the collision occurs.
	 * 			| result >= 0
	 * 			| && result == this.getObjectOne().getTimeToCollision(this.getObjectTwo())
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public double getTimeToCollision() throws IllegalStateException {
		return this.getObjectOne().getTimeToCollision(this.getObjectTwo());	
	}

	/**
	 * Returns the position at which the space objects collides.
	 * 
	 * @return	The position at which the collision occurs.
	 * 			| result == this.getObjectOne().getCollisionPosition(this.getObjectTwo())
	 * @throws 	IllegalStateException
	 *			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public double[] getCollisionPosition() throws IllegalStateException {
		return this.getObjectOne().getCollisionPosition(this.getObjectTwo());
	}

	/**
	 * Sends the collision data to the collision listener for a visual result.
	 * 
	 * @param 	collisionListener
	 * 			The collisionlistener that will process the data.
	 * @effect	If the collision doesn't result in the space objects ignoring each other,
	 * 			the collision listener will display an object collision at their collision position.
	 * 			| if(!this.getObjectOne().hasAsSource(this.getObjectTwo()) && !this.getObjectTwo().hasAsSource(this.getObjectOne()))
	 * 			|	then	collisionListener.objectCollision(getObjectOne(), getObjectTwo(), 
				|			getCollisionPosition()[0], getCollisionPosition()[1]);
	 * @throws 	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public void toCollisionListener(CollisionListener collisionListener)
			throws IllegalStateException {
		if (!this.getObjectOne().hasAsSource(this.getObjectTwo()) && !this.getObjectTwo().hasAsSource(this.getObjectOne())){
			collisionListener.objectCollision(getObjectOne(), getObjectTwo(), 
					getCollisionPosition()[0], getCollisionPosition()[1]);
		}
	}
	
	@Override
	public boolean isValidObjectTwo(SpaceObject object) {
		return object != null && getObjectOne().hasAsWorld(object.getWorld()) && (object.isShip() ^ object.isAsteroid() ^ object.isBullet());
	}
}
