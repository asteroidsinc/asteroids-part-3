package asteroids.model;

import be.kuleuven.cs.som.annotate.*;
import asteroids.model.programs.Program;
import asteroids.util.*;

/**
 * A class for dealing with spaceships, containing all methods affecting their parameters,
 * such as position, velocity, orientation, radius, mass, speed limit and minimal radius.<br>
 * It also deals with possible actions specific for the spaceship, like turning, thrusting or firing a
 * bullet. It inherits the methods written in SpaceObject as well.
 * 
 * @invar	The velocity of each spaceship must be a valid velocity for a spaceship.<br>
 * 	<code>	| isValidVelocity(getVelocity()) </code>
 * @invar 	The direction of each spaceship must be a valid direction for a spaceship.<br>
 * 	<code>	| isValidDirection(getDirection())</code>
 * @invar	The radius of each spaceship must be a valid radius for a spaceship.<br>
 * 	<code>	| isValidRadius(getRadius())</code>
 * @invar	The mass of each spaceship must be a valid mass for a spaceship.<br>
 * 	<code>	| isValidMass(getMass())</code>
 * @invar	The number of bullet of this ship in its world is a valid number of 
 * 			bullets for a spaceship.<br>
 * 	<code>	| isValidNumberBulletsFromShip(amount) </code>
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */

public class Ship extends SpaceObject {
	
	/**
	 * Initialise the new spaceship with a given horizontal and vertical position,
	 * horizontal and vertical velocity, direction, radius, speed limit and mass.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the new spaceship.
	 * @param 	yPosition
	 * 			The vertical position of the new spaceship.
	 * @param 	xVelocity
	 * 			The horizontal velocity of the new spaceship.
	 * @param	yVelocity
	 * 			The vertical velocity of the new spaceship.
	 * @param 	direction
	 * 			The direction of the new spaceship.
	 * @param 	radius
	 * 			The radius of the new spaceship.
	 * @param	mass
	 * 			The mass of the new spaceship. 
	 * @pre		The given direction must be a valid direction for the spaceship.<br>
	 * 	<code> 	| isValidDirection(direction) </code><br>
	 * @post	The horizontal position of the new spaceship is equal to the given horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == xPosition</code><br>
	 * @post	The vertical position of the new spaceship is equal to the given vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == yPosition</code><br>
	 * @post	The horizontal velocity of the new spaceship is equal to the given horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == xVelocity</code><br>
	 * @post	The vertical velocity of the new spaceship is equal to the given vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == yVelocity</code><br>
	 * @post	The direction of the new spaceship is equal to the given direction.<br>
	 * 	<code> 	| (new this).getDirection() == direction</code><br>
	 * @post    The radius of the new spaceship is equal to the given radius.<br>
	 * 	<code> 	| (new this).getRadius() == radius</code><br>
	 * @post	The mass of the new spaceship is equal to the given mass.<br>
	 * 	<code>	| (new this).getMass() == mass</code><br>
	 * @post	The speed limit of the new spaceship is equal to the speed of light.<br>
	 * 	<code>	| (new this).getSpeedLimit() == LIGHT_SPEED</code><br>
	 * @post	The minimal radius of the new spaceship is equal to 0.0 kilometers.<br>
	 * 	<code>	| (new this).getMinRadius() == 0.0</code><br>
	 * @post	The thruster of the new spaceship is equal to 1.1E21 Newton.<br>
	 *  <code>	| (new this).getThruster() == 1.1E21 </code><br>
	 * @post	The number of bullets of this ship in its world is equal to 0. <br>
	 * 	<code>	| (new this).getNbOwnBulletsInWorld() == 0 </code><br>
	 * @throws 	IllegalArgumentException
	 * 			The given radius is not a valid radius for the given spaceship.<br>
	 * 	<code> 	| !isValidRadius(radius)</code><br>
	 * @throws	IllegalArgumentException
	 * 			The given mass is not a valid mass for the given spaceship.<br>
	 * 	<code>	| !isValidMass(mass)</code><br>
	 */
	@Raw
	public Ship(double xPosition, double yPosition, double xVelocity, double yVelocity, 
					double direction, double radius, double mass) 
					throws IllegalArgumentException {
		super(xPosition, yPosition, xVelocity, yVelocity, radius, mass, LIGHT_SPEED, 0.0);
		setDirection(direction);
		thruster = 1.1E21;
		nbOwnBulletsInWorld = 0;
	}
	
	/**
	 * Initialise a default spaceship with a default horizontal and vertical position,
	 * horizontal and vertical velocity, direction, radius, mass, speed limit and minimal radius.
	 * 
	 * @post	The horizontal position of the new spaceship is equal to the default horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == 100.0</code>
	 * @post	The vertical position of the new spaceship is equal to the default vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == 100.0</code>
	 * @post	The horizontal velocity of the new spaceship is equal to the default horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == 10.0</code>
	 * @post	The vertical velocity of the new spaceship is equal to the default vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == 20.0</code>
	 * @post	The direction of the new spaceship is equal to the default direction.<br>
	 * 	<code> 	| (new this).getDirection() == Math.PI/5</code>
	 * @post    The radius of the new spaceship is equal to the default radius.<br>
	 * 	<code> 	| (new this).getRadius() == 16.0</code>
	 * @post	The mass of the new spaceship is equal to the default mass.<br>
	 * 	<code>	| (new this).getMass() == 5E15</code><br>
	 * @post	The speed limit of the new spaceship is equal to the speed of light.<br>
	 * 	<code>	| (new this).getSpeedLimit() == LIGHT_SPEED</code><br>
	 * @post	The minimal radius of the new spaceship is equal to 0.0 kilometers.<br>
	 * 	<code>	| (new this).getMinRadius() == 0.0</code><br>
	 * @post	The thruster of the new spaceship is equal to 1.1E21 Newton.<br>
	 *  <code>	| (new this).getThruster() == 1.1E21 </code><br>
	 * @post	The number of bullets of this ship in its world is equal to 0. <br>
	 * 	<code>	| (new this).getNbOwnBulletsInWorld() == 0 </code><br>
	 * @throws 	IllegalArgumentException
	 * 			The given radius is not a valid radius for the given spaceship.<br>
	 * 	<code> 	| !isValidRadius(radius)</code>
	 * @throws	IllegalArgumentException
	 * 			The given mass is not a valid mass for the given spaceship.<br>
	 * 	<code>	| !isValidMass(mass)</code>
	 */
	public Ship() throws IllegalArgumentException, IllegalStateException {
		this(100.0,100.0,10.0,20.0,Math.PI/5,16.0,5E15);
	}
	
	/**
	 * Return the direction of the spaceship, expressed in radians.<br>
	 * 	
	 * @return 	The direction of the spaceship
	 */
	@Basic @Raw
	public double getDirection(){
		return this.direction;
	}
	
	/**
	 * Sets the direction of the spaceship to the given entry.
	 * 
	 * @param 	direction
	 * 			The new direction of the spaceship
	 * @pre		The direction of the spaceship must be a valid direction.<br>
	 * 	<code>	| isValidDirection(direction)</code><br>
	 * @post	The direction of the spaceship is equal to the given direction.<br>
	 * 	<code>	| (new this).getDirection() == direction</code><br>
	 */
	@Model @Raw
	private void setDirection(double direction){	
		assert isValidDirection(direction);				
		this.direction = direction;
	}
	
	/**
	 * Checks if the given direction is a valid direction for the spaceship. 
	 * 
	 * @param 	direction
	 * 			The direction that has to be checked.
	 * @return	True if and only if the given direction is a valid number. <br>
	 * 	<code>	| result == !Double.isNaN(direction) </code>
	 */
	public static boolean isValidDirection(double direction){	
		return !Double.isNaN(direction); 						
	}
	
	/**
	 * Variable storing the direction of the spaceship.
	 */
	private double direction;
	
	/**
	 * Checks whether the space object is a ship.
	 * 
	 * @return	True
	 * @note	This method overrides the same method in the class SpaceObject.
	 */
	@Override @Immutable
	public boolean isShip() {
		return true;
	}

	/**
	 * Rotates the direction of the spaceship over a given angle.
	 * 
	 * @param 	angle
	 * 			The angle over which the spaceship rotates.
	 * @pre		The new direction of the spaceship must be a valid direction.<br>
	 * 	<code>	| isValidDirection(getDirection() + angle)</code><br>
	 * @post	The new direction of the spaceship is equal to its actual direction increased
	 * 			by the given angle.<br>
	 *	<code>	| (new this).getDirection() == (this.getDirection() + angle) </code><br>
	 * @note	@Raw because the new angle may violate the class invariant during the method.
	 */
	@Raw
	public void turn(double angle) {
		assert isValidDirection(getDirection() + angle);
		this.setDirection(getDirection() + angle);
	}
	
	/**
	 * Thrusts the spaceship for a given duration, according to the law of motion.
	 * 
	 * @param 	duration
	 * 			The duration of the thrust.
	 * @post	If the thruster is on, 
	 * 			the velocity is increased for a given duration after being adapted.
	 * 			The acceleration is proportional to its mass and the given duration.<br>
	 * <code><p>| if(this.checkThrusterIsOn())<br>
	 * 			|	then let acceleration = this.getThruster()*duration/(this.getMass()*1000)</p>
	 * 		<p>	|		 in<br>
	 * 			| 			(new this).getXVelocity() == this.getXVelocity() + acceleration*Math.cos(this.getDirection())<br>
	 * 			| 			(new this).getYVelocity() == this.getYVelocity() + acceleration*Math.sin(this.getDirection())<br>
	 * @post	The velocity does not change if the given duration is not a positive number.</p></code>
	 * 	<code>	| if((Double.isNaN(duration) || (duration < 0))) <br>
	 * 			| 	then ((new this).getXVelocity() == this.getXVelocity() <br>
	 * 			|	&&	  (new this).getYVelocity() == this.getYVelocity()) </code>
	 */
	@Model
	private void thrust(double duration){
		if(!isValidDuration(duration))
			duration = 0.0;
		if(this.checkThrusterIsOn()){
			//*1000 in the denominator because we have to convert m/s� into km/s�
			double acceleration = this.getThruster()*duration/(this.getMass()*1000); 
			Vector thrust = Vector.polarVector(acceleration, this.getDirection());
			this.setVelocity(thrust.X_COORD + this.getXVelocity(), thrust.Y_COORD + this.getYVelocity());
		}
	}
	
	/**
	 * Returns the value of the thruster of the spaceship.
	 * 
	 * @return	The value of the thruster of the spaceship.
	 */
	@Basic @Immutable
	public double getThruster() {
		return this.thruster;
	}
	
	/**
	 * Constant storing the power of the spaceship's thruster.
	 */
	private final double thruster;
	
	/**
	 * Checks whether the spaceship's thruster is enabled.
	 * 
	 * @return	True if the spaceship's thruster is on.
	 */
	@Basic
	public boolean checkThrusterIsOn(){
		return this.thrusterIsOn;
	}
	
	/**
	 * Sets the state of the thruster on or off.
	 * 
	 * @param 	active
	 * 			The state in which the thruster has to be set.
	 */
	public void setThrusterOn(boolean active) {
		this.thrusterIsOn = active;
	}
	
	/**
	 * Variable storing whether the spaceship's thruster is enabled.
	 */
	private boolean thrusterIsOn;
	
	/**
	 * Returns the position vector of the fired bullet.
	 * 
	 * @return	The horizontal position of the fired bullet is equal to the actual horizontal 
	 * 			position of the spaceship augmented by the ship's radius plus 3.0 projected 
	 * 			on the horizontal axis.
	 * <code>	| result.getXPosition() == this.getXPosition() + (this.getRadius() + 3.0)*Math.cos(this.getDirection())</code><br>
	 * @return	The vertical position of the fired bullet is equal to the actual vertical 
	 * 			position of the spaceship augmented by the ship's radius plus 3.0 projected 
	 * 			on the vertical axis.
	 * <code>	| result.getYPosition() == this.getYPosition() + (this.getRadius() + 3.0)*Math.sin(this.getDirection())</code><br>
	 */
	private Vector getFiredBulletPosition() {
		return Vector.addition(this.getPosition(), Vector.polarVector(this.getRadius() + 3.0, this.getDirection()));
	}
	
	/**
	 * Returns the initial velocity vector of the fired bullet.
	 * 	
	 * @return	The horizontal velocity of the fired bullet is equal to its default initial speed
	 * 			projected on the horizontal axis.
	 * 	<code>	| result.getXVelocity() == Bullet.INITIAL_SPEED*Math.cos(this.getDirection())</code><br>
	 * @return	The vertical velocity of the fired bullet is equal to its default initial speed
	 * 			projected on the vertical axis.
	 * 	<code>	| result.getYVelocity() == Bullet.INITIAL_SPEED*Math.sin(this.getDirection())</code><br>
	 */
	public Vector getInitialBulletVelocity() {
		return Vector.polarVector(Bullet.INITIAL_SPEED, this.getDirection());
	}
	
	/**
	 * Makes the ship fire a bullet at the same place as the canon position.
	 * 
	 * @result	If the spaceship has a world attached to it,
	 * 			creates a new bullet against the canon, with the default initial velocity
	 * 			with the default bullet radius and with the spaceship as a source.<br> 
	 * 	<code>	| if (!this.hasAsWorld(null))<br>
	 * 			|	then (result.getXPosition() == this.getCanonPosition().X_COORD<br>
	 * 			|		&& result.getYPosition() == this.getCanonPosition().Y_COORD<br>
	 * 			|		&& result.getXVelocity() == this.getInitialBulletVelocity().X_COORD<br>
	 * 			|		&& result.getYVelocity() == this.getInitialBulletVelocity().Y_COORD<br>
	 * 			|		&& result.getRadius() == 3.0<br>
	 * 			|		&& result.getSource() == this)<br></code>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the spaceship doesn't have a world attached to it.<br>
	 * 	<code>	| (this.hasAsWorld(null)) </code><br>
	 * @throws 	IllegalArgumentException
	 * 			Will throw an exception if one of the constructor of Bullet returns IllegalArgumentException.
	 */
	public Bullet fireBullet() throws IllegalStateException, IllegalArgumentException {
		if (this.hasAsWorld(null)){
			throw new IllegalStateException();
		}	
		else {
			return new Bullet(this.getFiredBulletPosition().X_COORD, this.getFiredBulletPosition().Y_COORD, 
				this.getInitialBulletVelocity().X_COORD, this.getInitialBulletVelocity().Y_COORD, 3.0, this);
		}
	}
	
	/**
	 * Returns the next collision of the spaceship with another space object.
	 * 
	 * @param	spaceObject
	 * 			The concurrent space object
	 * @return	If the concurrent space object is a bullet and the bullet has
	 * 			this ship as a source, the method returns a new future collision which has
	 * 			no effect upon them.
	 *			| if (spaceObject.isBullet() && spaceObject.hasAsSource(this))
	 *			| 	result == new IneffectiveCollision(this, spaceObject)
	 * @return	If the concurrent space object is a bullet and the bullet doesn't
	 * 			have this ship as a source, the method returns a new future collision 
	 * 			making both space objects explode.
	 * 			| if (spaceObject.isBullet() && !spaceObject.hasAsSource(this))
	 *			| 	result == new BothExplodingCollision(this, spaceObject)
	 * @return	If the concurrent space object is an asteroid, the method returns a new 
	 * 			future collision making this ship explode.
	 * 			| if (spaceObject.isAsteroid())
	 *			| 	result == new OtherExplodingCollision(spaceObject, this)
	 * @return	If the concurrent space object is a ship, the method returns a new 
	 * 			future collision making both ships bounce off each other.
	 * 			| if (spaceObject.isShip())
	 *			| 	result == new BouncingCollision(this, spaceObject)
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the ship or the concurrent space object  
	 * 			doesn't navigate within a world.<br>
	 * 			| (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
	 * @throws	IllegalStateException	
	 * 			Will throw an exception if the ship and the concurrent space object
	 * 			don't navigate within the same world.
	 * 			| (!this.hasAsWorld(spaceObject.getWorld()))
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the space object is not of the type asteroid, bullet or ship.
	 * 			| !(spaceObject.isBullet() || spaceObject.isAsteroid() || spaceObject.isShip()) 
	 */
	@Override
	public SpaceObjectCollision getNextCollision(SpaceObject spaceObject) throws IllegalStateException {
		if (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		if (spaceObject.isBullet()) {
			if (spaceObject.hasAsSource(this))
				return new IneffectiveCollision(this, spaceObject);
			return new BothExplodingCollision(this, spaceObject);
		}
		if (spaceObject.isAsteroid())
			return new OtherExplodingCollision(spaceObject, this);
		if (spaceObject.isShip()) 
			return new BouncingCollision(this, spaceObject);
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Makes the spaceship evolve together with its world for a given duration.
	 * 
	 * @param 	duration
	 * 			The given duration of the evolution.
	 * @effect	The spaceship will move for a given duration.<br>
	 * 	<code>	| this.move(duration) </code><br>
	 * @effect	If the duration of the evolution is greater than 0.0,
	 * 			remember that the space object hasn't collided recently.
	 * 			| if (duration > 0.0)
	 * 			|	then this.setRecentCollision(false)
	 * @effect	The spaceship will thrust for a given duration.<br>
	 * 	<code>	| this.thrust(duration) </code><br>
	 * @effect	If the ship's program is not <code>null</code>, the ship
	 * 			executes its program for a given duration. <br>
	 * 	<code>	| if (this.getProgram() != null)<br>
	 * 			| 	then this.getProgram().execute(duration)</code><br>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the spaceship doesn't have a world attached to it.<br>
	 *  <code>	| (this.hasAsWorld(null)) </code><br>
	 */
	@Override
	public void evolve(double duration) throws IllegalStateException {
		super.evolve(duration);
		this.thrust(duration);
		if(this.getProgram() != null)
			this.getProgram().execute(duration);
	}
	
	/**
	 * Returns the number of bullets shot by the spaceship in the world in which 
	 * it is navigating.
	 * 
	 * @return	The number of bullets shot by the spaceship
	 */
	public int getNbOwnBulletsInWorld() {
		return this.nbOwnBulletsInWorld;
	}
	
	/**
	 * Increases the number of bullets of the spaceship in its world by 1.
	 * 
	 * @post	The number of bullets of the spaceship in its world is increased by 1.
	 * 			| (new this).getNbOwnBulletsInWorld() == this.getNbOwnBulletsInWorld() + 1
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the spaceship doesn't navigate within a world
	 * 			or if the spaceship didn't have any recent collision. <br>
	 * 	<code>	| (this.hasAsWorld(null) && !this.hasRecentCollision()) </code><br>.
	 */
	@Raw
	public void incrementNbOwnBulletsInWorld() throws IllegalStateException {
		if(this.hasAsWorld(null) && !this.hasRecentCollision()) {
			System.out.println("no world");
			throw new IllegalStateException();
		}
		this.setNbOwnBulletsInWorld(getNbOwnBulletsInWorld() + 1);
	}
	
	/**
	 * Decreases the number of bullets of the spaceship in its world by 1.
	 * 
	 * @post	The number of bullets of the spaceship in its world is decreased by 1.
	 * 			| (new this).getNbOwnBulletsInWorld() == this.getNbOwnBulletsInWorld() - 1
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the spaceship didn't have any recent collision. <br>
	 * 	<code>	| (!this.hasRecentCollision()) </code><br>
	 */
	public void decrementNbOwnBulletsInWorld() throws IllegalStateException {
		this.setNbOwnBulletsInWorld(getNbOwnBulletsInWorld() - 1);
	}
	
	/**
	 * Sets the number of bullets of this ship in its world to the given entry.
	 * 
	 * @param 	amount
	 * 			The amount of bullets of the spaceship
	 * @post	If the given amount of bullets is a valid amount for the spaceship,
	 * 			of if the amount of bullets needs to temporarily exceed the upper limit, 
	 * 			the number of bullets of the spaceships in its world is equal to the given amount.
	 * 			| if (isValidNumberBulletsFromShip(amount) || isValidNumberBulletsFromShip(amount-1))
	 * 			|   then ((new this).nbOwnBulletsInWorld == amount)
	 */
	@Model
	@Raw
	private void setNbOwnBulletsInWorld(int amount) throws IllegalStateException {
		if (isValidNumberBulletsFromShip(amount) || isValidNumberBulletsFromShip(amount-1)) 
			this.nbOwnBulletsInWorld = amount;
	}
	
	/**
	 * Checks whether the number of bullets of the spaceship in its world 
	 * is a valid number of bullets for the spaceship.
	 * 
	 * @param 	amount
	 * 			The amount of bullets to be checked
	 * @return	True if and only if the number of bullet of the spaceship in its world
	 * 			is more than 0 and doesn't exceed 3.
	 * 			| result == (amount <= 3 && amount >= 0)
	 */
	@Raw
	public boolean isValidNumberBulletsFromShip(double amount) {
		return (amount <= 3 && amount >= 0);
	}
	
	/**
	 * Variable storing the number of bullets shot by the spaceship in the world 
	 * in which it is navigating.
	 */
	private int nbOwnBulletsInWorld;
	
	/**
	 * Returns the program of this ship.
	 * 
	 * @return	The program of this ship
	 */
	@Basic
	public Program getProgram() {
		return this.program;
	}
	
	/**
	 * Sets the program of this ship to the given entry.
	 * 
	 * @param 	program
	 * 			The program of the ship
	 * @post	The new program of this ship is equal to the given program.
	 * 			| (new this).program = program
	 * @post	If the given program is not null, the program sets this ship as its ship.
	 * 			| if (program != null)
	 * 			| 	then (new program).getShip() == this 
	 */
	public void setProgram(Program program) {
		this.program = program;
		if (program != null)
			program.setShip(this);
	}
	
	/**
	 * Removes the program of this ship.
	 * 
	 * @post	If the ship does have a program, the new program of the ship becomes <code>null</code>.<br>
	 * 			| if (!this.hasAsProgram(null))<br>
	 * 			|	then ((new this).getProgram() == null)<br>
	 * @post	If the program does have this ship as its ship, the new ship of the program becomes 
	 * 			<code>null</code>.<br>
	 * 			| if (this.getProgram().hasAsShip(this))<br>
	 * 			| 	then ((new this).getProgram().getShip() == null)<br>
	 */
	public void removeProgram() {
		if (!this.hasAsProgram(null)) {
			if (this.getProgram().hasAsShip(this))
				this.getProgram().removeShip();
			this.setProgram(null);
		}
	}
	
	/**
	 * Checks whether the ship has the given program as a program.
	 * 
	 * @param 	program
	 * 			The program that has to be checked
	 * @return	True if and only if the ship's program is equal to the given program.
	 * 			| result == (this.getProgram() == program)
	 */
	public boolean hasAsProgram(Program program) {
		return (this.getProgram() == program);
	}
	
	/**
	 * Variable storing the ship's program.
	 */
	private Program program;

}
