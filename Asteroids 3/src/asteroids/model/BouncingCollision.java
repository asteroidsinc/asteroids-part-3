/**
 * 
 */
package asteroids.model;

/**
 * A class for dealing with space object collisions (bouncing), containing all methods affecting the participating space objects,
 * such as time to collision, collision position and resolving the collision.
 * 
 * @invar	The space objects of this collision must be valid space objects for a collision.
 * 			| hasProperSpaceObjects()
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 *
 */
public class BouncingCollision extends SpaceObjectCollision {

	/**
	 * Initialise the new collision with given first and second space objects,
	 * 
	 * @param 	objectOne
	 * 			The first space object of the new collision.
	 * @param 	objectTwo
	 * 			The second space object of the new collision.
	 * @post	The first space object of the new collision is equal to the given space object.
	 * 			| (new this).getObjectOne() == objectOne
	 * @post	The second space object of the new collision is equal to the given space object.
	 * 			| (new this).getObjectTwo() == objectTwo
	 * @throws	IllegalArgumentException
	 * 			The second space object is null
	 * 			| objectTwo == null
	 */
	public BouncingCollision(SpaceObject objectOne, SpaceObject objectTwo) throws IllegalArgumentException{
		super(objectOne, objectTwo);
	}

	/**
	 * Resolves the collision between both objects.
	 * 
	 * @effect	The first space object of the collision bounces off the second one.
	 * 			| getObjectOne().bounce(getObjectTwo())
	 * @throws 	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public void resolveCollision() throws IllegalStateException {
		if(this.isTerminated())
			throw new IllegalStateException();
		getObjectOne().bounce(getObjectTwo());
	}

}
