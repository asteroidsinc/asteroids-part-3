package asteroids.model;

import be.kuleuven.cs.som.annotate.*;

import java.util.Random;
import asteroids.util.*;

/**
 * A class for dealing with asteroids, containing all methods affecting their parameters,
 * such as position, velocity, radius, speed limit and mass. It also deals with possible 
 * actions for the asteroid, like colliding and exploding in a specific way.
 * It inherits the methods written in SpaceObject as well.
 * 
 * @invar	The velocity of each asteroid must be a valid velocity for a asteroid.
 * 			| isValidVelocity(getVelocity())
 * @invar	The radius of each asteroid must be a valid radius for a asteroid.
 * 			| isValidRadius(getRadius())
 * @invar	The mass of each asteroid must be a valid mass for a asteroid.
 * 			| isValidMass(getMass())
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Asteroid extends SpaceObject {
	
	/**
	 * Initialise the new asteroid with a given horizontal and vertical position,
	 * horizontal and vertical velocity, a radius, a speed limit and a mass.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the new asteroid.
	 * @param 	yPosition
	 * 			The vertical position of the new asteroid.
	 * @param 	xVelocity
	 * 			The horizontal velocity of the new asteroid.
	 * @param	yVelocity
	 * 			The vertical velocity of the new asteroid.
	 * @param 	radius
	 * 			The radius of the new asteroid.
	 * @post	The horizontal position of the new asteroid is equal to the given horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == xPosition </code>
	 * @post	The vertical position of the new asteroid is equal to the given vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == yPosition </code>
	 * @post	The horizontal velocity of the new asteroid is equal to the given horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == xVelocity </code>
	 * @post	The vertical velocity of the new asteroid is equal to the given vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == yVelocity </code>
	 * @post    The radius of the new asteroid is equal to the given radius.<br>
	 * 	<code> 	| (new this).getRadius() == radius </code>
	 * @post	The minimal radius of the new asteroid is equal to 0.0 kilometres.<br>
	 * 	<code>	| (new this).getMinRadius() == 0.0 </code>
	 * @post	The speed limit of the new asteroid is equal to the speed of light.<br>
	 * 	<code>	| (new this).getSpeedLimit() == LIGHT_SPEED </code>
	 * @post	The mass of the new asteroid is equal to the calculated mass.<br>
	 * 	<code>	| (new this).getMass() == (4/3)*Math.PI*Math.pow(radius, 3)*DENSITY </code>
	 * @post	The spawn direction of the children of the asteroid is equal to a new random
	 * 			double between 0..2*Pi.<br>
	 * 	<code>	| (new this).getSpawnDirection() == new Random().nextDouble()*2*Math.PI </code>
	 * @throws 	IllegalArgumentException<br>
	 * 			The given radius is not a valid radius for the given asteroid.<br>
	 * 	<code> 	| !isValidRadius(radius) </code>
	 * @throws	IllegalArgumentException<br>				
	 * 			The calculated mass is not a valid mass for the given asteroid.<br>
	 * 	<code>	| !isValidMass((4/3)*Math.PI*Math.pow(radius, 3)*DENSITY)</code>
	 */
	@Raw
	public Asteroid(double xPosition, double yPosition, double xVelocity, double yVelocity, 
			double radius) throws IllegalArgumentException {
		this(xPosition, yPosition, xVelocity, yVelocity, radius, new Random());
	}
	
	/**
	 * Initialise the new asteroid with a given horizontal and vertical position,
	 * horizontal and vertical velocity, a radius, a random generator, a speed limit and a mass.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the new asteroid.
	 * @param 	yPosition
	 * 			The vertical position of the new asteroid.
	 * @param 	xVelocity
	 * 			The horizontal velocity of the new asteroid.
	 * @param	yVelocity
	 * 			The vertical velocity of the new asteroid.
	 * @param 	radius
	 * 			The radius of the new asteroid.
	 * @param	random
	 * 			A random number generator for determining the direction 
	 * 			of the children of the asteroid, if it had to split.
	 * @post	The horizontal position of the new asteroid is equal to the given horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == xPosition </code>
	 * @post	The vertical position of the new asteroid is equal to the given vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == yPosition </code>
	 * @post	The horizontal velocity of the new asteroid is equal to the given horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == xVelocity </code>
	 * @post	The vertical velocity of the new asteroid is equal to the given vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == yVelocity </code>
	 * @post    The radius of the new asteroid is equal to the given radius.<br>
	 * 	<code> 	| (new this).getRadius() == radius </code>
	 * @post	The minimal radius of the new asteroid is equal to 0.0 kilometres.<br>
	 * 	<code>	| (new this).getMinRadius() == 0.0 </code>
	 * @post	The speed limit of the new asteroid is equal to the speed of light.<br>
	 * 	<code>	| (new this).getSpeedLimit() == LIGHT_SPEED </code>
	 * @post	The mass of the new asteroid is equal to the calculated mass.<br>
	 * 	<code>	| (new this).getMass() == (4/3)*Math.PI*Math.pow(radius, 3)*DENSITY </code>
	 * @post	The spawn direction of the children of the asteroid is equal to a new random
	 * 			double between 0..2*Pi.<br>
	 * 	<code>	| (new this).getSpawnDirection() == random.nextDouble()*2*Math.PI </code>
	 * @throws 	IllegalArgumentException<br>
	 * 			The given radius is not a valid radius for the given asteroid.<br>
	 * 	<code> 	| !isValidRadius(radius) </code>
	 * @throws	IllegalArgumentException<br>
	 * 			The calculated mass is not a valid mass for the given asteroid.<br>
	 * 	<code>	| !isValidMass((4/3)*Math.PI*Math.pow(radius, 3)*DENSITY)</code>
	 */
	@Raw
	public Asteroid(double xPosition, double yPosition, double xVelocity, double yVelocity, 
			double radius, Random random) throws IllegalArgumentException {
			super(xPosition, yPosition, xVelocity, yVelocity, radius, (4/3)*Math.PI*Math.pow(radius, 3)*DENSITY, LIGHT_SPEED, 0.0);
		spawnDirection = random.nextDouble()*2*Math.PI;
	}
	
	/**
	 * Initialise the new asteroid with a default horizontal and vertical position,
	 * horizontal and vertical velocity, a radius, a speed limit and a mass.
	 * 
	 * @post	The horizontal position of the new asteroid is equal to the default horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == 100.0 </code>
	 * @post	The vertical position of the new asteroid is equal to the default vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == 100.0 </code>
	 * @post	The horizontal velocity of the new asteroid is equal to the default horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == 10.0 </code>
	 * @post	The vertical velocity of the new asteroid is equal to the default vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == 20.0 </code>
	 * @post    The radius of the new asteroid is equal to the default radius.<br>
	 * 	<code> 	| (new this).getRadius() == 16.0 </code>
	 * @post	The minimal radius of the new asteroid is equal to 0.0 kilometres.<br>
	 * 	<code>	| (new this).getMinRadius() == 0.0 </code>
	 * @post	The speed limit of the new asteroid is equal to the speed of light.<br>
	 * 	<code>	| (new this).getSpeedLimit() == LIGHT_SPEED </code>
	 * @post	The mass of the new asteroid is equal to the calculated mass.<br>
	 * 	<code>	| (new this).getMass() == (4/3)*Math.PI*Math.pow(radius, 3)*DENSITY </code>
	 * @throws 	IllegalArgumentException<br>
	 * 			The given radius is not a valid radius for the given asteroid.<br>
	 * 	<code> 	| !isValidRadius(radius) </code>
	 * @throws	IllegalArgumentException<br>
	 * 			The calculated mass is not a valid mass for the given asteroid.<br>
	 * 	<code>	| !isValidMass((4/3)*Math.PI*Math.pow(radius, 3)*DENSITY)</code>
	 */
	public Asteroid() throws IllegalArgumentException {
		this(100.0,100.0,10.0,20.0,16.0);
	}
	
	/**
	 * Constant storing the density of a asteroid.
	 */
	public static final double DENSITY = 2.65E12;
	
	/**
	 * Checks whether the space object is an asteroid.
	 * 
	 * @return	True 
	 */
	@Override @Immutable
	public boolean isAsteroid(){
		return true;
	}

	/**
	 * Checks whether the asteroid can be split in two asteroids of smaller size.
	 *  
	 * @return	True if and only if the asteroid is attached to a world,
	 * 			and if the radius of the asteroid is greater than or equal to 30.0.<br>
	 * 	<code>	| result == (!this.hasAsWorld(null) <br>
	 * 			|	&& Util.fuzzyGreaterThanOrEqualTo(this.getRadius(), 30.0)) </code>
	 */
	public boolean isSplitable() {
		return (!this.hasAsWorld(null) && Util.fuzzyGreaterThanOrEqualTo(this.getRadius(), 30.0));
	}
	
	/**
	 * Returns the direction of the spawned asteroids.
	 * 
	 * @return	The direction of the spawned asteroids
	 */
	@Basic @Immutable
	public double getSpawnDirection() {
		return this.spawnDirection;
	}

	/**
	 * Constant storing the randomly generated direction of the asteroid's children.
	 */
	private final double spawnDirection;

	/**
	 * Returns the next collision of the asteroid with another space object.
	 * 
	 * @param	spaceObject
	 * 			The concurrent space object
	 * @return	If the concurrent space object is a bullet, the method returns a new future collision 
	 * 			making both space objects explode.
	 * 			| if (spaceObject.isBullet())
	 *			| 	result == new BothExplodingCollision(this, spaceObject)
	 * @return	If the concurrent space object is a ship, the method returns a new 
	 * 			future collision making the ship explode.
	 * 			| if (spaceObject.isShip())
	 *			| 	result == new OtherExplodingCollision(this, spaceObject)
	 * @return	If the concurrent space object is an asteroid, the method returns a new 
	 * 			future collision making both asteroids bounce off each other.
	 * 			| if (spaceObject.isAsteroid())
	 *			| 	result == new BouncingCollision(this, spaceObject)
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the asteroid or the concurrent space object  
	 * 			doesn't navigate within a world.<br>
	 * 			| (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
	 * @throws	IllegalStateException	
	 * 			Will throw an exception if the asteroid and the concurrent space object
	 * 			don't navigate within the same world.
	 * 			| (!this.hasAsWorld(spaceObject.getWorld()))
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the space object is not of the type asteroid, bullet or ship.
	 * 			| !(spaceObject.isBullet() || spaceObject.isAsteroid() || spaceObject.isShip()) 
	 */
	@Override
	public SpaceObjectCollision getNextCollision(SpaceObject spaceObject) throws IllegalStateException {
		if (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		if (spaceObject.isBullet())
			return new BothExplodingCollision(this, spaceObject);
		if (spaceObject.isShip()) 
			return new OtherExplodingCollision(this, spaceObject);
		if (spaceObject.isAsteroid()){
			return new BouncingCollision(this, spaceObject);
		} else
			throw new IllegalArgumentException();
	}

	/**
	 * Makes the asteroid explode when hitting an obstacle. If the asteroid is splitable, it will be
	 * replaced by two smaller asteroids at the same place, moving in random, opposite directions.
	 * 
	 * @post	If the asteroid is splitable, two smaller asteroids will be created at approximately 
	 * 			the same place. Both asteroids' centres lie at opposite locations on the greater 
	 * 			asteroid's boundary.<br>
	 * <code><p>| let asteroidOne, asteroidTwo be new asteroids
	 * 			| if (this.isSplitable)<br>
	 * 			|	then ((new asteroidOne).getXPosition() == this.getXPosition() + this.getRadius()*Math.cos(getSpawnDirection())<br>
	 * 			|	  && (new asteroidOne).getYPosition() == this.getYPosition() + this.getRadius()*Math.sin(getSpawnDirection())<br>
	 * 			|	  && (new asteroidTwo).getXPosition() == this.getXPosition() - this.getRadius()*Math.cos(getSpawnDirection())<br>
	 *  		|	  && (new asteroidTwo).getYPosition() == this.getYPosition() - this.getRadius()*Math.sin(getSpawnDirection())<br></p></code>
	 * @effect	If the asteroid is splitable, the newly created asteroids move away 
	 * 			at the same velocity in random, opposite directions.<br>
	 * <code><p>| let asteroidOne, asteroidTwo be new asteroids
	 * 			| if (this.isSplitable)<br>
	 * 			| 	then (new asteroidOne).setVelocity(this.getVelocity()*1.5*Math.cos(getSpawnDirection()),<br>
	 *  		|			this.getVelocity()*1.5*Math.sin(getSpawnDirection())<br>
	 *  		| 	&&	 (new asteroidTwo).setVelocity(this.getVelocity()*1.5*Math.cos(getSpawnDirection() + Math.PI),<br>
	 *  		|			this.getVelocity()*1.5*Math.sin(getSpawnDirection() + Math.PI)<br></p></code>	
	 * @post	The asteroid will explode in the same way as other space objects,
	 * 			and so it will be terminated.<br>
	 * 	<code>	| (new this).isTerminated() == true<br>
	 * 			| && (new this).hasRecentCollision() == true </code><br>
	 */
	@Override
	public void explode() {
		if(this.isSplitable()){
			double direction = this.getSpawnDirection();
			World world = this.getWorld();
			Vector positionOne = Vector.addition(getPosition(), Vector.polarVector(this.getRadius(), direction));
			Vector velocityOne = Vector.polarVector(Vector.norm(getVelocity())*1.5, direction);
				
			Vector positionTwo = Vector.addition(getPosition(), Vector.polarVector(-this.getRadius(), direction));
			Vector velocityTwo = Vector.polarVector(Vector.norm(getVelocity())*1.5, direction + Math.PI);
			super.explode();
			try {
				world.addSpaceObject(new Asteroid(positionOne.X_COORD,positionOne.Y_COORD,velocityOne.X_COORD,velocityOne.Y_COORD,this.getRadius()/2));
			} catch (IllegalArgumentException exc) {
			}
			try {
				world.addSpaceObject(new Asteroid(positionTwo.X_COORD,positionTwo.Y_COORD,velocityTwo.X_COORD,velocityTwo.Y_COORD,this.getRadius()/2));
			} catch (IllegalArgumentException exc) {
			}
		}
		else {
			super.explode();
		}
	}
}
