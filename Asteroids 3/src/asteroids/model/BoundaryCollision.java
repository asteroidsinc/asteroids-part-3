package asteroids.model;

import asteroids.view.CollisionListener;

/**
 * A class for dealing with boundary collisions, containing all methods affecting the participating object,
 * such as time to collision, collision position and resolving the collision.
 * 
 * @invar	The space objects of this collision must be valid space objects for a collision.
 * 			| hasProperSpaceObjects()
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 *
 */
public class BoundaryCollision extends Collision {

	/**
	 * Initialise the new collision with given first space object,
	 * 
	 * @param 	objectOne
	 * 			The first space object of the new collision.
	 * @post	The first space object of the new collision is equal to the given space object.
	 * 			| (new this).getObjectOne() == objectOne
	 * @post	The second space object of the new collision is null.
	 * 			| (new this).getObjectTwo() == null
	 */
	public BoundaryCollision(SpaceObject objectOne) {
		super(objectOne, null);
 	}

	/**
	 * Returns the remaining time until the space object collides with a boundary.
	 * 
	 * @return	The time until the collision occurs.
	 * 			| result >= 0
	 * 			| && result == this.getObjectOne().getTimeToCollision()
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public double getTimeToCollision() throws IllegalStateException {
		return this.getObjectOne().getTimeToCollision();
	}

	/**
	 * Returns the position at which the space object collides with a boundary.
	 * 
	 * @return	The position at which the collision occurs.
	 * 			| result == this.getObjectOne().getCollisionPosition()
	 * @throws 	IllegalStateException
	 *			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public double[] getCollisionPosition() throws IllegalStateException {
		return this.getObjectOne().getCollisionPosition();
	}

	/**
	 * Sends the collision data to the collision listener for a visual result.
	 * 
	 * @param 	collisionListener
	 * 			The collisionlistener that will process the data.
	 * @effect	The collision listener will display a boundary collision at the collision position.
	 * 			| collisionListener.boundaryCollision(getObjectOne(), 
				|	getCollisionPosition()[0], getCollisionPosition()[1])
	 * @throws 	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public void toCollisionListener(CollisionListener collisionListener)
			throws IllegalStateException, NullPointerException {
		collisionListener.boundaryCollision(getObjectOne(), 
				getCollisionPosition()[0], getCollisionPosition()[1]);
	}
	
	@Override
	public boolean isValidObjectTwo(SpaceObject object) {
		return object == null;
	}
	
	/**
	 * Resolves the collision between both objects.
	 * 
	 * @effect	The space objectof the collision bounces off the boundary.
	 * 			| getObjectOne().bounce();
	 * @throws 	IllegalStateException
	 * Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Override
	public void resolveCollision() throws IllegalStateException {
		if(this.isTerminated())
			throw new IllegalStateException();
		getObjectOne().bounce();
	}

}
