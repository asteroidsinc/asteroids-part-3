package asteroids.model;

import java.util.*;

import asteroids.util.Util;
import asteroids.util.Vector;
import asteroids.view.CollisionListener;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class for dealing with worlds, containing all methods affecting their parameters,
 * such as height and width.<br>
 * It also deals with possible actions for the world: it can add space objects to a world while
 * checking collisions with other space objects at creation, it can remove space objects from the
 * world, it checks whether the world can contain a space object or if it already does, and
 * stores all possible collisions that can occur during the evolution of the world.
 *
 * @invar	The height of each world must be a valid height for a world.
 * 			| isValidDimension(getHeight())
 * @invar	The width of each world must be a valid width for a world.
 * 			| isValidDimension(getWidth())
 * @invar	The world must have valid space objects.
 * 			| hasProperSpaceObjects()
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class World {
	
	/**
	 * Initialise the new world with a given height and width, and a default upper bound for 
	 * the dimensions.
	 * 
	 * @param	height
	 * 			The height of the new world.
	 * @param 	width
	 * 			The width of the new world.
	 * @post	The upper bound of the new world is equal to MAX_VALUE
	 * 			| (new this).getUpperBound() == Double.MAX_VALUE
	 * @post	The height of the new world is equal to the given height.
	 * 			| (new this).getHeight() == height
	 * @post	The width of the new world is equal to the given width.
	 * 			| (new this).getWidth() == width
	 * @throws	IllegalArgumentException
	 * 			The given height is not a valid height for the world.
	 * 			| (!isValidDimension(height))
	 * @throws	IllegalArgumentException
	 * 			The given width is not a valid width for the world.
	 * 			| (!isValidDimension(width))
	 */
	@Raw
	public World(double height, double width) throws IllegalArgumentException {
		this.upperBound = Double.MAX_VALUE;
		if(!isValidDimension(height))
			throw new IllegalArgumentException("The height is not valid.");
		if(!isValidDimension(width))
			throw new IllegalArgumentException("The width is not valid.");
		this.height = height;
		this.width = width;
	}
	
	/**
	 * Returns the height of the world.
	 * 
	 * @return	The height of the world
	 */
	@Basic @Immutable @Raw
	public double getHeight() {
		return this.height;
	}
	
	/**
	 * Constant storing the height of the world.
	 */
	private final double height;
	
	/**
	 * Returns the width of the world.
	 * 
	 * @return	The width of the world
	 */
	@Basic @Immutable @Raw
	public double getWidth() {
		return this.width;
	}
	
	/**
	 * Constant storing the width of the world.
	 */
	private final double width;
	
	/**
	 * Check if the given dimension is a valid dimension.
	 * 
	 * @param 	dimension
	 * 			The dimension that has to be checked.
	 * @return 	True if and only if the dimension is a positive number and it is smaller than the upper bound.
	 * 			| result == (dimension >= 0.0) && (dimension <= getUpperBound())
	 */
	public boolean isValidDimension(double dimension) {
		return (dimension >= 0.0) && (dimension <= getUpperBound());
	}
	
	/**
	 * Returns the upper bound of the world.
	 * 
	 * @return	The upper bound of the world
	 */
	@Basic @Immutable
	public double getUpperBound() {
		return this.upperBound;
	}
	
	/**
	 * Constant storing the upper bound for a dimension of a world.
	 */
	private final double upperBound;
		
	/**
	 * Terminates the world if it is still existing.
	 * 
	 * @post   	This world is terminated.
	 *       	| (new this).isTerminated()
	 * @post	The world no longer contains any space objects.
	 * 			| (new this).getNbSpaceObjects() == 0
	 * @post   	All space objects belonging to this world
	 *         	upon entry, no longer have a world.
	 *       	| for each spaceObject in this.getSpaceObjects():
	 *      	|  	((new spaceObject).hasAsWorld(null))     
	 */
	public void terminate() throws IllegalArgumentException, NullPointerException{
		if (!isTerminated()) {
			for (SpaceObject spaceObject : this.getSpaceObjects())
				this.removeSpaceObject(spaceObject);
			isTerminated = true;
		}
	}
		
	/**
	 * Checks if the world is already terminated.
	 */
	@Basic
	public boolean isTerminated() {
		return this.isTerminated;
	}

	/**
	 * Variable storing the state of the world.
	 */
	private boolean isTerminated;

	/**
	 * Adds the given space object to the space objects of the world.
	 * 
	 * @param 	spaceObject
	 * 			The space object to be added
	 * @effect	The local boolean variable is assigned according to the type of the 
	 * 			space object of which the initial collisions are checked.
	 * 			| if (spaceObject.isShip())
	 * 			|	 then let hasToBeTerminated == this.checkAddingShip(spaceObject)
	 * 			| if (spaceObject.isAsteroid())
	 * 			|	 then let hasToBeTerminated == this.checkAddingAsteroid(spaceObject)
	 * 			| if (spaceObject.isBullet() 
	 * 			|	&& canHaveAnotherBulletFromSource(((Bullet) spaceObject).getSource()))
	 * 			|	 then let hasToBeTerminated == this.checkAddingBullet(spaceObject)
	 * 			|	 	&& (spaceObject).getSource().incrementNbOwnBulletsInWorld()
	 * @post	If given space object doesn't occasion any collision at its creation, the 
	 * 			number of space objects of this world is incremented by 1, the new world 
	 * 			of the given space object is equal to this world, and the world has this 
	 * 			ship as a space object.
	 * 			|	if (!hasToBeTerminated)
	 * 			| 		then (new this).getNbSpaceObjects() = this.getNbSpaceObjects() + 1
	 * 			|			&& (new spaceObject).hasAsWorld(this)
	 * 			|			&& (new this).hasAsSpaceObject(spaceObject)
	 * @effect	If the space object has to be terminated, it explodes.	
	 * 			| if(hasToBeTerminated)
	 * 			|	then spaceObject.explode()
	 * @effect	If the world still contains the given space object, its future collisions are calculated.
	 * 			| if((new this).hasAsSpaceObject(spaceObject))
	 * 			| 	then this.simulateCollisions(spaceObject)
	 * @throws	IllegalArgumentException
	 * 			The world cannot have given space object as a space object.
	 * 			| !this.canHaveAsSpaceObject(spaceObject)
	 * @throws	IllegalArgumentException
	 * 			The world is trying to add a space object that already has a different world as its world.
	 * 			| !spaceObject.hasAsWorld(null) && !spaceObject.hasAsWorld(this)
	 * @throws	IllegalArgumentException
	 * 			The world cannot have another bullet from a specific ship.
	 * 			| (spaceObject.isBullet() 
	 * 			|	&& !canHaveAnotherBulletFromSource(((Bullet) spaceObject).getSource()))
	 * @throws	IllegalArgumentException
	 * 			The given space object is neither an asteroid, nor a ship, nor a bullet.
	 * 			| (!spaceObject.isAsteroid() && !spaceObject.isShip() && !spaceObject.isBullet())
	 * @throws	IllegalStateException
	 * 			addShip, addAsteroid or AddBullet can throw an IllegalStateException.
	 * @note	@Raw for the argument because the given space object is maybe already
	 * 			navigating inside another world. As this method is public, the stated case
	 * 			may happen because of the user.
	 */
	public void addSpaceObject(@Raw SpaceObject spaceObject) 
			throws IllegalArgumentException, IllegalStateException {
		if (!this.canHaveAsSpaceObject(spaceObject))
			throw new IllegalArgumentException();
		boolean hasToBeTerminated = false;
		spaceObject.setWorld(this);
		spaceObjects.add(spaceObject);
		if (spaceObject.isShip())
			hasToBeTerminated = this.checkAddingShip((Ship) spaceObject);
		else if (spaceObject.isAsteroid())
			hasToBeTerminated = this.checkAddingAsteroid((Asteroid) spaceObject);
		else if (spaceObject.isBullet()) {
			
			if (canHaveAnotherBulletFromSource(((Bullet) spaceObject).getSource())) {
					
				hasToBeTerminated = this.checkAddingBullet((Bullet) spaceObject);
				((Bullet) spaceObject).getSource().incrementNbOwnBulletsInWorld();
			}
			else {
				((Bullet) spaceObject).getSource().incrementNbOwnBulletsInWorld();
				throw new IllegalArgumentException();
			}
		}
		else
			//We need an exception here (while not in removeSpaceObject) because if we try
			//to introduce another kind of space object and its initial position is not 
			//checked, it may overlap with another space object.
			throw new IllegalArgumentException();
		
		if (hasToBeTerminated)
			spaceObject.explode();
		else
			simulateCollisions(spaceObject);
	}
	
	/**
	 * Removes the given space object from the space objects of the world.
	 * 
	 * @param 	spaceObject
	 * 			The space object to be removed
	 * @post	The world doesn't have this space object as a space object anymore
	 * 			and the space object doesn't have this world as a world anymore.
	 * 			| !((new this).hasAsSpaceObject(spaceObject)) && (new spaceObject).hasAsWorld(null)
	 * @post	If the space object is a bullet, the number of bullets of the bullet's source in 
	 * 			this world is decremented by 1.
	 * 			| if(spaceObject.isBullet())
	 * 			|	then (new spaceObject).getSource().getNbOwnBulletsInWorld()
	 * 			|		== spaceObject.getSource().getNbOwnBulletsInWorld() - 1
	 * @effect	The collisions of the space object in this world are removed.
	 * 			| this.removeCollisionsWith(spaceObject)
	 * @throws 	IllegalArgumentException
	 * 			The world does not contain given space object
	 * 			| !this.hasAsSpaceObject(spaceObject)
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 * @note	@Raw for the argument because the given space object is maybe already
	 * 			navigating inside another world. As this method is public, the stated case
	 * 			may happen because of the user.
	 */
	@Raw
	public void removeSpaceObject(@Raw SpaceObject spaceObject)
			throws IllegalArgumentException, NullPointerException {
		if (!this.hasAsSpaceObject(spaceObject))
			throw new IllegalArgumentException();
	
		spaceObjects.remove(spaceObject);
		if (spaceObject.isBullet())
			((Bullet) spaceObject).getSource().decrementNbOwnBulletsInWorld();
		spaceObject.removeWorld();
		this.removeCollisionsWith(spaceObject);
	}
	
	/**
	 * Checks whether the given ship collides with another space object at its apparition.
	 * Returns true if the added ship has to be terminated because of an overlap.
	 * May explode existing space objects of the world.
	 * 
	 * @param  	ship
	 *         	The ship to be checked.
	 * @return	True if and only if the created ship overlaps an existing space object.
	 * 			| for each spaceObjectTemp in this.getSpaceObjects():
	 * 			|	if (ship.overlap(spaceObjectTemp))
	 * 			|		then result == true
	 * @post	If the concurrent space object is a bullet, the bullet immediately explodes.
	 *			| for each spaceObjectTemp in this.getSpaceObjects():
	 * 			|	if (spaceObjectTemp.isBullet())
	 * 			| 		if (ship.overlap(spaceObjectTemp))
	 * 			|			then (new spaceObjectTemp).isTerminated() == true
	 * 			|			&&  (new spaceObjectTemp).hasRecentCollision() == true
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Model
	@Raw	//door de iteraties over de lijst spaceObjects die zelf @Raw is
	private boolean checkAddingShip(@Raw Ship ship) throws IllegalStateException {
		boolean hasToBeTerminated = false;
		for (SpaceObject spaceObjectTemp : this.getSpaceObjects()) {
			if (spaceObjectTemp.isBullet()) {
				if (ship.overlap(spaceObjectTemp)) {
					spaceObjectTemp.explode();
					hasToBeTerminated = true;
				}
			}
			if (spaceObjectTemp.isAsteroid()) {
				if (ship.overlap(spaceObjectTemp)) {
					hasToBeTerminated = true;
				}
			}
			if (spaceObjectTemp.isShip()) {
				if (ship.overlap(spaceObjectTemp)) {
					hasToBeTerminated = true;			
				}
			}
		}
		return hasToBeTerminated;
	}
	
	/**
	 * Checks whether the given asteroid collides with another space object at its apparition.	
	 * Returns true if the added asteroid has to be terminated because of an overlap.
	 * May explode existing space objects of the world.
	 * 
	 * @param  	asteroid
	 *         	The asteroid to be checked.
	 * @return	True if and only if the concurrent space object is an asteroid or a bullet,
	 * 			and if the created asteroid overlaps an existing space object.
	 * 			| for each spaceObjectTemp in this.getSpaceObjects():
	 * 			|	if (spaceObjectTemp.isAsteroid() || spaceObjectTemp.isBullet())
	 * 			|		if (asteroid.overlap(spaceObjectTemp))
	 * 			|			then result == true
	 * @post	If the concurrent space object is a bullet or a ship, it immediately explodes.
	 *			| for each spaceObjectTemp in this.getSpaceObjects():
	 * 			|	if (spaceObjectTemp.isBullet() || spaceObjectTemp.isShip())
	 * 			| 		if (asteroid.overlap(spaceObjectTemp))
	 * 			|			then (new spaceObjectTemp).isTerminated() == true
	 * 			|			&&  (new spaceObjectTemp).hasRecentCollision() == true
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Model
	@Raw
	private boolean checkAddingAsteroid(Asteroid asteroid) throws IllegalStateException {
		boolean hasToBeTerminated = false;
		for (SpaceObject spaceObjectTemp : this.getSpaceObjects()) {
			if (spaceObjectTemp.isBullet()) {
				if (asteroid.overlap(spaceObjectTemp)) {
					spaceObjectTemp.explode();
					hasToBeTerminated = true;
				}
			}
			if (spaceObjectTemp.isAsteroid()) {
				if (asteroid.overlap(spaceObjectTemp)) {
					hasToBeTerminated = true;		
				}
			}
			if (spaceObjectTemp.isShip()) {
				if (asteroid.overlap(spaceObjectTemp)) {
					spaceObjectTemp.explode();
				}
			}
		}
		return hasToBeTerminated;
	}
	
	/**
	 * Checks whether the given bullet collides with another space object at its apparition.
	 * Returns true if the added bullet has to be terminated because of an overlap.
	 * May explode existing space objects of the world.	
	 * 
	 * @param  	bullet
	 *         	The bullet to be checked.
	 * @return	True if and only if the created bullet overlaps an existing space object.
	 * 			| for each spaceObjectTemp in this.getSpaceObjects():
	 * 			|	if (bullet.overlap(spaceObjectTemp)
	 * 			|		then result == true
	 * @effect	Whatever the concurrent space object is, it immediately explodes.
	 *			| for each spaceObjectTemp in this.getSpaceObjects():
	 * 			| 	if (bullet.overlap(spaceObjectTemp)
	 * 			|		then spaceObjectTemp.explode()
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Model
	@Raw
	private boolean checkAddingBullet(Bullet bullet) throws IllegalStateException {
			boolean hasToBeTerminated = false;
			for (SpaceObject spaceObjectTemp : this.getBullets()) {
				if (bullet.overlap(spaceObjectTemp)) {
						spaceObjectTemp.explode();
						hasToBeTerminated = true;
				}
			}
			for (SpaceObject spaceObjectTemp : this.getShips()) {
				if (bullet.overlap(spaceObjectTemp) && !bullet.hasAsSource(spaceObjectTemp)) {
					spaceObjectTemp.explode();
					hasToBeTerminated = true;
				}
			}
			for (SpaceObject spaceObjectTemp : this.getAsteroids()) {
				if (bullet.overlap(spaceObjectTemp)) {
					spaceObjectTemp.explode();
					hasToBeTerminated = true;
				}
			}
			return hasToBeTerminated;
	}
		
	/**
	 * Returns the number of ships stored in the set ships.
	 * 
	 * @return	The number of ships stored in the set ships.
	 * 			| result == getShips().size()
	 */
	@Raw 
	public int getNbShips() {
		return getShips().size();
	}
	
	/**
	 * Returns the number of asteroids stored in the set asteroids.
	 * 
	 * @return	The number of asteroids stored in the set asteroids
	 * 			| result == getAsteroids().size()
	 */
	@Raw 
	public int getNbAsteroids() {
		return getAsteroids().size();
	}
	
	/**
	 * Returns the number of bullets stored in the set bullets.
	 * 
	 * @return	The number of bullets stored in the set bullets
	 * 			| result == getBullets().size()
	 */
	@Raw 
	public int getNbBullets() {
		return getBullets().size();
	}
	
	/**
	 * Returns the number of space objects stored in the set of space objects.
	 * 
	 * @return	The number of space objects stored in the set space objects
	 * 			| result == getSpaceObjects().size()
	 */
	@Raw
	public int getNbSpaceObjects() {
		return this.getSpaceObjects().size();
	}
	
	/**
	 * Checks whether the world can contain another bullet from the given ship.
	 * 
	 * @param 	ship
	 * 			The ship to be checked
	 * @return	True if and only if the world has this ship as one of its space objects
	 * 			and if the actual number of bullets incremented by 1 is still a valid number 
	 * 			of bullets for this ship in this world.
	 * 			| result == (this.hasAsSpaceObject(ship) 
	 *			|	&& ship.isValidNumberBulletsFromShip(ship.getNbOwnBulletsInWorld() + 1))
	 */
	@Raw
	public boolean canHaveAnotherBulletFromSource(Ship ship) {
		return (this.hasAsSpaceObject(ship) 
				&& ship.isValidNumberBulletsFromShip(ship.getNbOwnBulletsInWorld() + 1));
	}
			
	/**
	 * Checks whether the world has the given space object as one of its space objects 
	 * and vice versa (bidirectional).
	 * 
	 * @param 	spaceObject
	 * 			The space object to check.
	 * @return	True if and only if the given space object is not null and it has this world as its world
	 * 			and the given space object is saved as a space object of the world.
	 * 			| result == (spaceObject != null && spaceObject.hasAsWorld(this)
	 * 			| 	&& (this.getSpaceObjects().contains(spaceObject)))
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public boolean hasAsSpaceObject(@Raw SpaceObject spaceObject) throws IllegalStateException {
		return (spaceObject != null && spaceObject.hasAsWorld(this)
				&& this.getSpaceObjects().contains(spaceObject));
	}

	/**
	 * Checks whether the world can have the given space object as its space object.
	 * 
	 * @param   spaceObject
	 *          The space object to check.
	 * @return	True if and only if the world is not terminated, the space object is not null,
	 * 			the space object is not terminated and the space object lies within the boundaries
	 * 			of the world.  
	 * 			| result == ((!this.isTerminated()) && (this.isWithinBounds(spaceObject.getPosition(), spaceObject.getRadius())) 
	 * 			|	&& (spaceObject != null) && (!spaceObject.isTerminated()))
	 */
	@Raw
	public boolean canHaveAsSpaceObject(@Raw SpaceObject spaceObject) {
		return (!this.isTerminated()) && (spaceObject != null)
				&& (!spaceObject.isTerminated())
				&& (this.isWithinBounds(spaceObject.getPosition(), spaceObject.getRadius()));
	}
		
	/**
	 * Checks whether the world has proper space objects attached to it.
	 * 
	 * @return	True if and only if each of these space objects references the world as
	 *         	the world to which they are attached
	 *          and if each of these space objects lies within the boundaries of the world.
	 *       	|  result ==
	 *       	|   for each spaceObject in this.getSpaceObjects():
	 *       	|     ( (spaceObject.hasAsWorld(this)) 
	 *       	|	    && (isWithinBounds(spaceObject.getPosition(), spaceObject.getRadius())) )
	 * @note	This is a class invariant, and still it is @Raw because it invokes 
	 * 			getSpaceObjects() which is @Raw itself.
	 */
	@Raw
	public boolean hasProperSpaceObjects() {
		for (SpaceObject spaceObject : this.getSpaceObjects()) {
			if (!spaceObject.hasAsWorld(this))
				return false;
			if (!isWithinBounds(spaceObject.getPosition(), spaceObject.getRadius()))
				return false;
			if (spaceObject.isShip()) {
				if(((Ship) spaceObject).getNbOwnBulletsInWorld() > 3 
						|| ((Ship) spaceObject).getNbOwnBulletsInWorld() < 0)
					return false;
			}
		}
		return true;	
	}
	
	/**
	 * Returns a set of the world's ships.
	 * 
	 * @return	Returns a set of space objects only of the type ship.
	 * 			| for each spaceObject in this.getSpaceObjects():
	 * 			|	if (spaceObject.isShip())
	 * 			|		then result.contains(spaceObject)
	 */
	public Set<Ship> getShips() {
		HashSet<Ship> ships = new HashSet<Ship>();
		for (SpaceObject spaceObject : this.getSpaceObjects()) {
			if (spaceObject.isShip())
				ships.add((Ship) spaceObject);
		}
		return ships;
	}
	
	/**
	 * Returns a set of the world's asteroids.
	 * 
	 * @return	Returns a set of space objects only of the type asteroid.
	 * 			| for each spaceObject in this.getSpaceObjects():
	 * 			|	if (spaceObject.isAsteroid())
	 * 			|		then result.contains(spaceObject)
	 */
	public Set<Asteroid> getAsteroids() {
		HashSet<Asteroid> asteroids = new HashSet<Asteroid>();
		for (SpaceObject spaceObject : this.getSpaceObjects()) {
			if (spaceObject.isAsteroid())
				asteroids.add((Asteroid) spaceObject);
		}
		return asteroids;
	}
	
	/**
	 * Returns a set of the world's bullets.
	 * 
	 * @return	Returns a set of space objects only of the type bullet.
	 * 			| for each spaceObject in this.getSpaceObjects():
	 * 			|	if (spaceObject.isBullet())
	 * 			|		then result.contains(spaceObject)
	 */
	public Set<Bullet> getBullets() {
		HashSet<Bullet> bullets = new HashSet<Bullet>();
		for (SpaceObject spaceObject : this.getSpaceObjects()) {
			if (spaceObject.isBullet())
				bullets.add((Bullet) spaceObject);
		}
		return bullets;
	}
	
	/**
	 * Returns a copy of the world's space objects.
	 * 
	 * @return	A new set of space objects, identical to the world's space objects.
	 * @return  The size of the resulting set is equal to the number of
	 *          space objects of this world.
	 *        | result.size() == getNbSpaceObjects()
	 */
	@Basic @Raw @Immutable
	public Set<SpaceObject> getSpaceObjects() {
		return new HashSet<SpaceObject>(spaceObjects);
	}
	
	/**
	 * Constant set containing the world's space objects.
	 * 
	 * @invar	The referenced set is effective.
	 * 			| spaceObjects != null
	 * @invar  	Each space object registered in the referenced set is
	 *         	effective and not yet terminated.
	 *       	| for each spaceObject in spaceObjects:
	 *       	|   ( (spaceObject != null) &&
	 *       	|     (! spaceObject.isTerminated()) )
	 * @invar 	The space objects of a non-terminated world are registered
	 * 			in the set of hashed space objects.
	 * 			| for each spaceObject in SpaceObject:
	 * 			| 	if (! world.isTerminated() && spaceObject.hasAsWorld(world))
	 * 			| 		then spaceObjects.contains(spaceObject)
	 * @note	No invariant to avoid storing the same element twice in the set,
	 * 			because storing each element only once is a property of a set.
	 */
	private final Set<SpaceObject> spaceObjects = new HashSet<SpaceObject>();
	
	/**
	 * Checks whether the given horizontal position of a space object
	 * is a valid horizontal position within the world.
	 * 
	 * @param 	xPosition
	 * 			A horizontal position in the Euclidian plane.
	 * @return	True if and only if the horizontal position of a space object lies between 0.0 
	 * 			and the width of this world.
	 * 			| result == (Util.fuzzyLessThan(xPosition, getWidth()) 
	 * 			|				&& Util.fuzzyGreaterThan(xPosition, 0.0))
	 */
	public boolean isValidXPosition(double xPosition){
		return (Util.fuzzyLessThan(xPosition, getWidth()) && Util.fuzzyGreaterThan(xPosition, 0.0));
	}
	
	/**
	 * Checks whether the given vertical position of a space object
	 * is a valid vertical position within the world.
	 * 
	 * @param 	yPosition
	 * 			A vertical position in the Euclidian plane.
	 * @return	True if and only if the vertical position of a space object lies between 0.0 
	 * 			and the height of this world.
	 * 			| result == (Util.fuzzyLessThan(yPosition, getHeight()) 
	 * 			|				&& Util.fuzzyGreaterThan(yPosition, 0.0))
	 */
	public boolean isValidYPosition(double yPosition){
		return (Util.fuzzyLessThan(yPosition, getHeight()) && Util.fuzzyGreaterThan(yPosition, 0.0));
	}
	
	/**
	 * Checks whether the given space object lies within the boundaries of the world.
	 * 
	 * @param	spaceObject
	 * 			The given space object.
	 * @return	True if and only if the space object's boundaries lie fully within 
	 * 			the boundaries of the world.
	 * 			| result == (isValidXPosition(position.X_COORD + radius)
	 * 			|			&& isValidXPosition(position.X_COORD - radius)
	 * 			|			&& isValidYPosition(position.Y_COORD + radius)
	 * 			|			&& isValidYPosition(position.Y_COORD - radius))
	 * @throws	IllegalArgumentException
	 * 			
	 */
	@Raw
	public boolean isWithinBounds(Vector position, double radius) {
		if (!isValidXPosition(position.X_COORD + radius))
			return false;
		if (!isValidXPosition(position.X_COORD - radius))
			return false;
		if (!isValidYPosition(position.Y_COORD + radius))
			return false;
		if (!isValidYPosition(position.Y_COORD - radius))
			return false;
		return true;
	}
	
	/**
	 * Simulates all possible future collisions for given space object and adds those that don't exist yet.
	 * 
	 * @param 	spaceObject
	 * 			The space object for which future collisions will be calculated.
	 * @post	The list of collisions of the world will contain each future collision
	 * 			of the given space object with one of the world's space objects.
	 * 			| for each spaceObjectTemp in getSpaceObjects():
	 * 			|	((new this).getAllCollisions().contains(spaceObject.getNextCollision(spaceObjectTemp))
	 * 			|	^ (new this).getAllCollisions().contains(spaceObjectTemp.getNextCollision(spaceObject)))
	 * @post	The list of collisions of the world will contain each collision
	 * 			by given space object with one of the world's boundaries.
	 * 			| (new this).getAllCollisions().contains(spaceObject.getNextCollisionToBoundary())
	 * @throws 	IllegalArgumentException
	 * 			| spaceObject == null
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Model
	private void simulateCollisions(SpaceObject spaceObject) 
			throws IllegalArgumentException, IllegalStateException {
		if (spaceObject == null)
			throw new IllegalArgumentException();
		for (SpaceObject spaceObjectTemp: this.getSpaceObjects()) {
			if (spaceObject != spaceObjectTemp) {
				SpaceObjectCollision currentCollision = spaceObject.getNextCollision(spaceObjectTemp);
				allCollisions.add(currentCollision);
			}
		}
		allCollisions.add(spaceObject.getNextCollisionToBoundary());
	}
	
	/**
	 * Evolves this world and all its space objects for a given duration, 
	 * and resolve all collisions that happened in the meantime, if any.
	 * 
	 * @param	duration
	 * 			The duration for which the world will evolve.
	 * @param	collisionListener
	 * 			
	 * @effect	If the duration is a positive number and if the world has collisions,
	 * 			the collisions are sorted. Then, if the first collision time is smaller than given duration,
	 * 			the space objects of the world evolve for that time, afterwards the first collision occurs
	 * 			and is resolved. Finally the world evolves for the remaining time.
	 * 			Otherwise, if the first collision time is greater than given duration,
	 * 			the space objects of the world evolve for given duration.
	 * 			| if(Util.fuzzyGreaterThan(duration, 0.0) && !getAllCollisions().isEmpty())
	 * 			|	then 
	 * 			|		this.sortCollisions()
	 * 			|	 &&	let nextCollision = this.getNextCollision()
	 * 			|			in
	 * 			| 			let newDuration = duration - nextCollision.getTimeToCollision()
	 * 			| 				in
	 * 			| 				if(Util.fuzzyGreaterThan(newDuration, 0.0))
	 * 			|					then (this.evolveSpaceObjects(nextCollision.getTimeToCollision())
	 * 			|					 &&	 if (collisionListener != null)
	 * 			|							then nextCollision.toCollisionListener(collisionListener)
	 * 			|					 &&	 nextCollision.resolveCollision()
	 * 			|					 &&	 this.evolve(newDuration, collisionListener))	
	 *			| 				else 
	 *			|					this.evolveSpaceObjects(duration)
	 */
	public void evolve(double duration, CollisionListener collisionListener) throws NullPointerException {
		if (Util.fuzzyGreaterThan(duration, 0.0) && (!getAllCollisions().isEmpty())) {
			this.sortCollisions();
			Collision nextCollision = this.getNextCollision();
			double newDuration = duration - nextCollision.getTimeToCollision();
			
			//if timeToCollision = Infinity => newDuration is negative => the next test fails
			if (Util.fuzzyGreaterThan(newDuration, 0.0)) {
				this.evolveSpaceObjects(nextCollision.getTimeToCollision());
				if (collisionListener != null)
					nextCollision.toCollisionListener(collisionListener);
				nextCollision.resolveCollision();
				this.evolve(newDuration, collisionListener);
			}
			else {
				this.evolveSpaceObjects(duration);
			}		
		}
	}

	/**
	 * Removes all collisions between given space object and any other or one the world boundaries
	 * from the world's collisions.
	 * 
	 * @param 	spaceObject
	 * 			The space object whose collisions will be removed from the world's collisions.
	 * @post	The collisions of the world don't have this space object as an actor anymore.
	 * 			| for each collision in (new this).getAllCollisions():<br>
	 * 			|	collision.getObjectOne() != spaceObject 
	 * 			|		&& collision.getObjectTwo() != spaceObject
	 */
	@Raw
	@Model
	private void removeCollisionsWith(SpaceObject spaceObject) {
		for (Collision currentCollision: this.getAllCollisions()) {
			if(currentCollision.getObjectOne() == spaceObject 
					|| currentCollision.getObjectTwo() == spaceObject) {
				allCollisions.remove(currentCollision);
				currentCollision.terminate();
			}
		}
	}
	
	/**
	 * Makes the space objects of the world evolve for a given duration.
	 * 
	 * @param 	duration
	 * 			The duration for which the space objects will evolve.
	 * @effect	Each space object in the world will evolve for the given duration.
	 * 			| for each spaceObject in this.getSpaceObjects():<br>
	 * 			| 	spaceObject.evolve(duration)<br>
	 */
	@Model													
	private void evolveSpaceObjects(double duration) {		
		for (SpaceObject spaceObject : this.getSpaceObjects())
			spaceObject.evolve(duration);
	}
	
	/**
	 * Returns the next collision occurring in the world.
	 * 
	 * @return	The next collision occurring in the world
	 * 			| result == this.getAllCollisions().get(0)
	 */
	public Collision getNextCollision() {
		return this.getAllCollisions().get(0);
	}
	
	/**
	 * Returns a copy of the list containing all the world's upcoming collisions.
	 * 
	 * @return	A new list containing all the world's collisions.
	 * @return  The size of the resulting list is equal to the number of
	 *          collisions of this world.
	 *        | result.size() == getNbCollisions()
	 */
	@Basic @Raw @Immutable
	public ArrayList<Collision> getAllCollisions() {
		return new ArrayList<Collision>(allCollisions);
	}
	
	/**
	 * Returns the number of saved collisions in the world.<br>
	 * 	
	 * @return	The number of collisions stored in the list allCollisions.
	 * 			| result == this.getAllCollisions().size()<br>
	 */
	@Basic @Raw
	public double getNbCollisions() {
		return this.getAllCollisions().size();
	}
	
	/**
	 * Constant list containing the world's collisions.<br>
	 * 
	 * @invar	The referenced list is effective.<br>
	 * 			| allCollisions != null<br>
	 * @invar  	Each collision registered in the referenced list is<br>
	 *         	effective.<br>
	 *       	| for each collision in allCollisions:<br>
	 *       	|   (collision != null)<br>
	 * @invar	Each collision registered in the referenced list appears only once.<br>
	 * 			| for each I,J in 0..allCollisions.size()-1:
	 *			| 	( (I == J)
	 * 			| 	|| (allCollisions.get(I) != allCollisions.get(J)))
	 */
	private final ArrayList<Collision> allCollisions = new ArrayList<Collision>();

	/**
	 * Sorts the collisions of the world using insertion sort, 
	 * so that the first to occur is at index 1 and so on.<br>
	 * 
	 * @post	The list is sorted in increasing size of time until a collision occurs.<br>
	 * 			| for each i, j in 0...getNbCollisions():<br>
	 * 			|	if(i < j)<br>
	 * 			|		then (Util.fuzzyLessThanOrEqual(getAllcollisions().get(i).getTimeToCollision(), getAllcollisions().get(j).getTimeToCollision()))<br>
	 * @invar	The time to collision of the collision at position i is always smaller than 
	 * 			the time to collision of the collision at the last position in the list. 
	 * 			| Outer loop:
	 *			|	for each i in 0..getNbCollisions()-1:
	 *			|		allCollisions.get(i).getTimeToCollision() <= allCollisions.get(this.getNbCollisions()-1).getTimeToCollision()
	 * @invar	The time to collision of the collision at position i is always smaller than 
	 * 			the time to collision of the collision at position (i+1) in the list. 
	 * 			| Inner loop:
	 *			| 	for each j in 0..i:
	 *			|		allCollisions.get(j).getTimeToCollision() <= allCollisions.get(j+1).getTimeToCollision()
	 * @variant	| i starts from 0, increases by 1 during each iteration and will eventually become equal to getNbCollisions()-1
	 * @variant | j starts from i, decreases by 1 during each iteration and will eventually become equal to 0
	 */
	@Model
	private void sortCollisions() {
        boolean finished;
        for (int i = 0 ; i < allCollisions.size(); i++){
            int j = i;
            Collision collision2 = allCollisions.get(j);
            if (i > 0) { 
                double time2 = collision2.getTimeToCollision();

                finished = false;
                while (j != 0 && !finished) {
                	Collision collision1 = allCollisions.get(j-1);
                	double time1 = collision1.getTimeToCollision();
                	if (Util.fuzzyLessThan(time2, time1)) {
                        wissel(j, j-1);	
                    }
                    else {
                        finished = true;
                    }
                    j--;
                }
            }
        }
    }
	
	/**
	 * Interchanges 2 elements of the world's collisions at given indices.<br>
	 * 
	 * @param	index2<br>
	 * 			The index of the first collision in the old ranking.<br>
	 * @param 	index1<br>
	 * 			The index of the second collision in the old ranking.<br>
	 * @post	The collision previously found at index1 will be located at index2.<br>
	 * 			The collision previously found at index2 will be located at index1.<br>
	 * 			| (new this).getAllCollisions().get(index2) == this.getAllCollisions().get(index1)<br>
	 * 			| (new this).getAllCollisions().get(index1) == this.getAllCollisions().get(index2)<br>
	 */
	@Model
	private void wissel(int index2, int index1) {
		Collision collision2 = allCollisions.remove(index2);
		Collision collision1 = allCollisions.remove(index1);
		allCollisions.add(index1, collision2);
		allCollisions.add(index2, collision1);
	}
	
}