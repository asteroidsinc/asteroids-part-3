package asteroids.model;

import be.kuleuven.cs.som.annotate.*;
import asteroids.util.*;

/**
 * A class for dealing with space objects, containing all methods affecting their parameters,
 * such as position, velocity, radius, mass, speed limit and minimal radius.<br>It also deals with
 * possible actions for the space object (move, evolve), can link the space object to an existing world
 * or unlink it from an existing world, makes easy calculations concerning an eventual collision possible
 * and enables it to react in case of such a collision (by bouncing, exploding or continuing its way).
 * 
 * @invar	The velocity of each space object must be a valid velocity for a space object.<br>
 * 	<code>	| isValidVelocity(getVelocity()) </code><br>
 * @invar	The radius of each space object must be a valid radius for a space object.<br>
 * 	<code>	| isValidRadius(getRadius()) </code><br>
 * @invar	The mass of each space object must be a valid mass for a space object.<br>
 * 	<code>	| isValidMass(getMass()) </code><br>
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */

public abstract class SpaceObject {
	
	/**
	 * Initialise the new space object with a given horizontal and vertical position,
	 * horizontal and vertical velocity, and a given radius.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the new space object.
	 * @param 	yPosition
	 * 			The vertical position of the new space object.
	 * @param 	xVelocity
	 * 			The horizontal velocity of the new space object.
	 * @param 	yVelocity
	 * 			The vertical velocity of the new space object.
	 * @param 	radius
	 * 			The radius of the new space object.
	 * @param	mass
	 * 			The mass of the new space object.
	 * @param	speedLimit
	 * 			The speed limit of the new space object.
	 * @param	minRadius
	 * 			The minimal radius of the new space object.
	 * @post	The horizontal position of the new space object is equal to the given horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == xPosition </code><br>
	 * @post	The vertical position of the new space object is equal to the given vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == yPosition </code><br>
	 * @post	The horizontal velocity of the new space object is equal to the given horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == xVelocity </code><br>
	 * @post	The vertical velocity of the new space object is equal to the given vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == yVelocity </code><br>
	 * @post    The radius of the new space object is equal to the given radius.<br>
	 * 	<code> 	| (new this).getRadius() == radius </code><br>
	 * @post	The mass of the new space object is equal to the given mass.<br>
	 * 	<code>	| (new this).getMass() == mass </code><br>
	 * @post	The speed limit of the new space object is equal to the given speed limit. <br>
	 * 	<code>	| (new this).getSpeedLimit() == speedLimit </code><br>
	 * @post	The minimal radius of the new space object is equal to the given minimal radius. <br>
	 * 	<code>	| (new this).getMinRadius() == minRadius </code><br>
	 * @throws 	IllegalArgumentException<br>
	 * 			The given radius is not a valid radius for the given space object.<br>
	 * 	<code> 	| !isValidRadius(radius)</code><br>
	 * @throws	IllegalArgumentException<br>
	 * 			The given mass is not a valid mass for the given space object.<br>
	 * 	<code>	| !isValidMass(mass) </code><br>
	 * @note	Because this class is abstract, no object of the type SpaceObject will ever be created.
	 *			The class's purpose is to define general methods which will be inherited by the classes
	 *			Ship, Asteroid and Bullet.
	 */
	@Raw
	public SpaceObject(double xPosition, double yPosition, double xVelocity, double yVelocity,
			double radius, double mass, double speedLimit, double minRadius) throws IllegalArgumentException {
		//speedLimit must be initialised before velocity, otherwise setVelocity() won't succeed.
		if (Double.isNaN(speedLimit))
			speedLimit = LIGHT_SPEED;
		this.speedLimit = speedLimit;
		//minRadius must be initialised before radius, otherwise isValidRadius() won't succeed.
		if (Double.isNaN(minRadius))
			minRadius = 0;
		this.minRadius = minRadius;	
		this.setPosition(xPosition, yPosition);
		this.setVelocity(xVelocity, yVelocity);
		if (!isValidRadius(radius))
			throw new IllegalArgumentException();
		this.radius = radius;
		if (!isValidMass(mass))
			throw new IllegalArgumentException();
		this.mass = mass;
	}
	
	/**
	 * Returns the horizontal position of the space object, expressed in kilometers.
	 * 
	 * @return 	The horizontal position of the space object. 
	 */
	@Basic
	public double getXPosition(){
		return this.position.X_COORD;
	}
	
	/**
	 * Returns the vertical position of the space object, expressed in kilometers.
	 * 
	 * @return 	The vertical position of the space object.
	 */
	@Basic
	public double getYPosition(){
		return this.position.Y_COORD;
	}

	/**
	 * Returns the position vector of the space ship.
	 * 
	 * @return	The position of the space ship.
	 */
	@Basic
	public Vector getPosition() {
		return this.position;
	}
	
	/**
	 * Sets the position vector of the space ship to the given horizontal and vertical positions.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the space object
	 * @param 	yPosition
	 * 			The vertical position of the space object
	 * @post	The new horizontal position of the space object becomes the given horizontal position.<br>
	 * 	<code>	| (new this).getXPosition() == xPosition </code><br>
	 * @post	The new vertical position of the space object becomes the given vertical position.<br>
	 * 	<code>	| (new this).getYPosition() == yPosition </code><br>
	 * @throws 	IllegalArgumentException	
	 * 			Will throw an exception if the horizontal position is not a number.<br>
	 * 	<code>	| Double.isNaN(xPosition) </code> <br>
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the vertical position is not a number. <br>
	 * 	<code>	| Double.isNaN(yPosition) </code><br>
	 */
	protected void setPosition(double xPosition, double yPosition) throws IllegalArgumentException {
		this.position = new Vector(xPosition, yPosition);
	}
	
	/**
	 * Variable storing the position vector of the space object.
	 */
	private Vector position;
	
	/**
	 * Returns the horizontal velocity of the space object, expressed in kilometers/second.
	 * 
	 * @return 	The horizontal velocity of the space object.
	 * @note	@Raw because we don't know if the prime object already satisfies 
	 * 			all its class invariants. In setVelocity, the vector is first created
	 * 			before being checked in adaptVelocity.
	 */
	@Basic @Raw
	public double getXVelocity(){
		return this.velocity.X_COORD;
	}
	
	/**
	 * Returns the vertical velocity of the space object, expressed in kilometers/second.
	 * 
	 * @return 	The vertical velocity of the space object.
	 * @note	@Raw because we don't know if the prime object already satisfies 
	 * 			all its class invariants. In setVelocity, the vector is first created
	 * 			before being checked in adaptVelocity.
	 */
	@Basic @Raw
	public double getYVelocity(){
		return this.velocity.Y_COORD;
	}
	
	/**
	 * Returns the velocity vector of the space ship.
	 * 
	 * @return	The velocity of the space ship.
	 */
	@Basic @Raw
	public Vector getVelocity(){
		return this.velocity;
	}
	
	/**
	 * Checks whether the given velocity doesn't exceed the speed limit.
	 * 
	 * @param	velocity
	 * 			The velocity that has to be checked.
	 * @return	False if the velocity exceeds the speed limit.
	 * 	<code>	| result == (Vector.norm(velocity)) <= this.getSpeedLimit() </code>
	 */
	public boolean isValidVelocity(Vector velocity) {
		return (Vector.norm(velocity) <= this.getSpeedLimit());
	}
	
	/**
	 * Adapts the velocity vector if the total velocity 
	 * exceeds the speed limit.
	 * 
	 * @effect	The velocity vector is adapted if the total velocity exceeds the speed limit.
	 * 			The proportions of both horizontal and vertical components are preserved.<br>
	 * 	<code>	| if(!isValidVelocity(this.getVelocity())) <br>
	 * 			|	then this.setVelocity(this.getXVelocity()*(this.getSpeedLimit()/Vector.norm(this.getVelocity())),<br> 
	 * 			|		this.getYVelocity()*(this.getSpeedLimit()/Vector.norm(this.getVelocity()))) </code>
	 * @note	Private because used only in setVelocity and nowhere else.
	 */
	@Model @Raw
	private void adaptVelocity() {
		if(!isValidVelocity(this.getVelocity())){
			double proportion = this.getSpeedLimit()/Vector.norm(this.getVelocity());
			this.setVelocity(this.getXVelocity()*proportion, this.getYVelocity()*proportion);
		} 
	}
	
	/**
	 * Sets the velocity vector of the space object to the given horizontal and vertical velocities.
	 * 
	 * @param 	xVelocity
	 * 			The new horizontal velocity of the space object.
	 * @param	yVelocity
	 * 			The new vertical velocity of the space object.
	 * @post	The new horizontal velocity  of the space object becomes the given horizontal velocity.<br>
	 * 	<code>	| (new this).getXVelocity() == xVelocity </code><br>
	 * @post	The new vertical velocity  of the space object becomes the given vertical velocity.<br>
	 * 	<code>	| (new this).getYVelocity() == yVelocity </code><br>
	 * @effect	The new velocity of the space object is adapted to remain a valid velocity.
	 * 			| this.adaptVelocity()
	 */
	@Raw
	protected void setVelocity(double xVelocity, double yVelocity){
		try {													
			this.velocity = new Vector(xVelocity, yVelocity);
		} catch (IllegalArgumentException exc) { 	
			this.velocity = new Vector();
		}
		this.adaptVelocity();
	}
	
	/**
	 * Variable storing the velocity vector of the space object.
	 */
	private Vector velocity;
	
	/**
	 * Returns the speedlimit of the space object.
	 * 
	 * @return	The speedlimit of the space object.<br>
	 * 	<code>	| result <= LIGHT_SPEED </code><br>
	 * @note	This is an immutable getter because it will always return the same speed limit 
	 * 			after being initialised once.
	 */
	@Basic @Immutable
	public double getSpeedLimit() {
		return speedLimit;	 
	}
	
	/**
	 * Constant storing the speed limit of the space object.
	 */
	private final double speedLimit;
	
	/**
	 * Constant storing the speed of the light.
	 */
	public static final double LIGHT_SPEED = 300000.0;
	
	/**
	 * Returns the radius of the space object, expressed in kilometers.
	 * 
	 * @return 	The radius of the space object
	 */
	@Basic @Raw @Immutable
	public double getRadius(){
		return this.radius;
	}
	
	/**
	 * Checks if the radius is a valid radius for the space object.
	 * 
	 * @param 	radius
	 * 			The radius that needs to be checked.
	 * @return	True if and only if the radius is a number and if the radius is larger than 10.<br>
	 * 	<code>	| result == !Double.isNaN(radius) && (radius > this.getMinRadius()) </code>
	 */
	public boolean isValidRadius(double radius){
		return !Double.isNaN(radius) && (radius > this.getMinRadius());
	}
	
	/**
	 * Constant storing the radius of the space object.
	 */
	private final double radius;
	
	/**
	 * Returns the minimal radius of the space object.
	 * 
	 * @return	The minimal radius of the space object
	 */
	@Basic @Immutable
	public double getMinRadius() {
		return this.minRadius;
	}
	
	/**
	 * Constant storing the minimal radius.
	 */
	private final double minRadius;
	
	/**
	 * Returns the mass of the space object.
	 * 
	 * @return	The mass of the space object
	 */
	@Basic @Raw @Immutable
	public double getMass(){
		return this.mass;
	}
	
	/**
	 * Checks whether the given mass is a valid mass.
	 * 
	 * @param 	mass
	 * 			The mass that has to be checked.
	 * @return	True if and only if the given mass is a strictly positive number.<br>
	 * 	<code>	| result == (!Double.isNaN(mass)) && (mass > 0.0) </code>
	 */
	public static boolean isValidMass(double mass){
		return !Double.isNaN(mass) && (mass > 0.0);
	}
	
	/**
	 * Constant storing the mass of the space object.
	 */
	private final double mass;
		
	/**
	 * Checks whether the space object can be in the given world.
	 * 
	 * @param  world
	 *         The world to check.
	 * @return True if and only if the given world is null
	 *         or the world can have this space object as its space object.<br>
	 *  <code> | result == ((world == null) || world.canHaveAsSpaceObject(this)) </code>
	 */
	@Raw
	public boolean canHaveAsWorld(World world) {
		return (world == null) || (world.canHaveAsSpaceObject(this));
	}
		
	/**
	 * Returns the world in which the space object is lying.
	 * 
	 * @return	The world in which the space object is lying
	 * @note	This method may return null, as it is not compulsory 
	 * 			for a space object to lie inside a world.
	 */
	@Basic
	public World getWorld() {
		return this.world;
	}

	/**
	 * The space object joins the given world.
	 * 
	 * @param 	world
	 * 			The new world in which the space object is navigating.
	 * @effect	The world adds the space object to its space object.
	 * 			world.addSpaceObject(this)
	 * @throws 	IllegalArgumentException
	 * 			Will throw an exception if the world to join is null.
	 * 			| world == null
	 */
	public void joinWorld(World world) throws IllegalArgumentException {
		if(world != null)
			world.addSpaceObject(this);
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Sets the world in which the space object is navigating to the given entry.
	 * 
	 * @param 	world
	 * 			The new world in which the space object is navigating. 
	 * @post	The new world of the space object becomes the given world.<br>
	 * 	<code>	| (new this).getWorld() = world </code><br>
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the space object cannot have the given world as a world.<br>
	 * 	<code>	| (!this.canHaveAsWorld(world)) </code>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the given world is not null and 
	 * 			the space object already belongs to another world other than null. <br>
	 * 	<code>	| (world != null && !this.hasAsWorld(world) && !this.hasAsWorld(null)) </code><br>
	 */
	public void setWorld(World world) {
		if(!canHaveAsWorld(world))
			throw new IllegalArgumentException();
		if (world != null && !this.hasAsWorld(world) && !this.hasAsWorld(null))		
			throw new IllegalArgumentException();
		this.world = world;
	}

	/**
	 * Checks whether the space object has the given world attached to it.
	 * 
	 * @return	True if and only if the space object has the given world attached to it.<br>
	 * 	<code>	| result == (this.getWorld() == world) </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the space object is already terminated
	 * 			and the space object has a world at the same time.<br>
	 * 	<code>	| (this.isTerminated() &&  this.getWorld() != null) </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the space object has a world 
	 * 			and the space object's world is terminated at the same time.<br>
	 * 	<code>	| (this.getWorld() != null && this.getWorld().isTerminated())</code><br>
	 * @note	The if-test's second condition is needed to avoid an exception when a terminated object 
	 * 			doesn't have a world, which is a legal case.
	 */
	public boolean hasAsWorld(World world) throws IllegalStateException {
		if (this.isTerminated() && this.getWorld() != null)		
			throw new IllegalStateException();					
		if (this.getWorld() != null && this.getWorld().isTerminated()) 	
			throw new IllegalStateException();
		return (this.getWorld() == world);
	}

	/**
	 * Detaches the space object from the world and the world from the space object.
	 * 
	 * @post	If the space object has a world attached to it, the field world is emptied.<br>
	 * 	<code>	| if (!this.hasAsWorld(null))<br>
	 * 			|	then ((new this).hasAsWorld(null))</code><br>
	 * @post	If the world attached to the space object still has the space object attached to itself,
	 * 			the space object is removed from the world.<br>
	 * 	<code>	| if (this.getWorld().hasAsSpaceObject(this))<br>
	 * 			|	then ((new this).getWorld()).hasAsSpaceObject(null)</code><br>
	 * @throws 	IllegalStateException
	 * 			Will throw an IllegalStateException 
	 * 			if setWorld returns IllegalStateException.<br>
	 * @throws	IllegalArgumentException
	 * 			Will throw an IllegalArgumentException 
	 * 			if setWorld or removeSpaceObject returns IllegalArgumentException.<br>
	 */
	public void removeWorld() throws IllegalArgumentException, IllegalStateException {
		if (!this.hasAsWorld(null)) {
			if (this.getWorld().hasAsSpaceObject(this))
				this.getWorld().removeSpaceObject(this);
			this.setWorld(null);
		}
	}

	/**
	 * Variable referencing the world in which the space object is involved.
	 * 
	 * @invar	The world registered here is effective and not yet terminated or null.<br>
	 * 	<code>	| (!world.isTerminated() || world == null) </code>
	 */
	private World world;
		
	/**
	 * Checks whether the space object is a ship.
	 * 
	 * @return	True if and only if the space object is a ship.
	 * @note	This method is overriden in the class Ship.
	 */
	@Immutable
	public boolean isShip() {
		return false;
	}

	/**
	 * Checks whether the space object is a bullet.
	 * 
	 * @return	True if and only if the space object is a bullet.
	 * @note	This method is overriden in the class Bullet.
	 */
	@Immutable
	public boolean isBullet() {
		return false;
	}

	/**
	 * Checks whether the space object is an asteroid.
	 * 
	 * @return	True if and only if the space object is an asteroid.
	 * @note	This method is overriden in the class Asteroid.
	 */
	@Immutable
	public boolean isAsteroid() {
		return false;
	}

	/**
	 * Checks if the space object is already terminated or not.
	 */
	@Basic
	public boolean isTerminated() {
		return this.isTerminated;
	}

	/**
	 * Terminates the space object if it is still existing.
	 * 
	 * @post	If the space object is not yet terminated, detach it from the world it was navigating in
	 * 			and switch its state to terminated. <br> 
	 * 	<code>	| if(!this.isTerminated()) <br>
	 * 			|    then ((new this).hasAsWorld(null) <br>
	 * 			|		&& (new this).isTerminated = true)</code>
	 */
	protected void terminate() {
		if (!isTerminated()) {
			this.removeWorld();
			this.isTerminated = true;
		}
	}

	/**
	 * Variable storing the state of the world.
	 */
	private boolean isTerminated;
	
	/**
	 * Checks whether this space object has the given space object as a source.
	 * 
	 * @param 	spaceObject
	 * 			The potential source.
	 * @return	True if and only if this space object is a bullet 
	 * 			and the given space object is a ship and the bullet's source. See override in Bullet.
	 */
	public boolean hasAsSource(SpaceObject spaceObject) {
		return false;
	}
		
	/**
	 * Moves the space object according to its velocity for a given duration.
	 * 
	 * @param 	duration
	 * 			The duration of the motion of the space object.
	 * @post	The new horizontal position of the space object is equal to its actual position 
	 * 			increased by an amount equal to its horizontal velocity times the duration of the motion.<br>
	 * 	<code>	| (new this).getXPosition() == (this.getXPosition() + this.getXVelocity()*duration) </code><br>
	 * @post	The new vertical position of the space object is equal to its actual position 
	 * 			increased by an amount equal to its vertical velocity times the duration of the motion.<br>
	 *  <code>	| (new this).getYPosition() == (this.getYPosition() + this.getYVelocity()*duration) </code><br>
	 * @throws	IllegalStateException
	 *			Will throw an exception if the space object is already terminated.<br>
	 * 	<code>	| this.isTerminated()</code>
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the duration is not a valid duration.<br>
	 * 	<code>	| (!isValidDuration(duration))  </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the space object is not entirely within bounds after the motion 
	 * 			or if the given duration is (greater than or) equal to the time 
	 * 			until the space object will collide with one of the boundaries of the world.
	 * 	<code>	| (!this.getWorld().isWithinBounds(newPosition, this.getRadius()) 
	 *			|	&& !Util.fuzzyEquals(this.getWorld().getNextCollision().getTimeToCollision(), duration))</code><br>
	 */
	public void move(double duration) 
			throws IllegalArgumentException, IllegalStateException {
		if (this.isTerminated())
			throw new IllegalStateException();
		if (!isValidDuration(duration))
			throw new IllegalArgumentException();
		Vector newPosition = Vector.addition(this.getPosition(), Vector.multiple(this.getVelocity(), duration));
		
		if (!this.getWorld().isWithinBounds(newPosition, this.getRadius()) 
				&& Util.fuzzyLessThan(this.getWorld().getNextCollision().getTimeToCollision(), duration))
				throw new IllegalArgumentException();
		this.setPosition(newPosition.X_COORD, newPosition.Y_COORD);
	}
	
	/**
	 * Checks if the given duration is a valid duration.
	 * 
	 * @param 	duration
	 * 			The duration that has to be checked.
	 * @return 	True if and only if the duration is a real positive number (including 0.0).<br>
	 * 	<code>	| result == ((!Double.isNaN(duration)) && (duration >= 0.0)) </code>
	 */
	public static boolean isValidDuration(double duration) {
		return (!Double.isNaN(duration)) && (duration >= 0.0);
	}
	
	/**
	 * Returns the distance between this space object and another space object. The distance is expressed
	 * in kilometers and represents the distance between the circle boundaries.
	 * 
	 * @param 	spaceObject
	 * 			The concurrent spaceObject
	 * @return	The distance between the current space object and the concurrent space object.<br>
	 * 	<code>	| result == Vector.distance(this.getPosition(), spaceObject.getPosition())<br>
	 * 			|	- (this.getRadius() + spaceObject.getRadius()) </code><br>
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a space object.<br>
	 * 	<code>	| (spaceObject == null)</code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if one of the space objects is already terminated.<br>
	 * 	<code>	| (this.isTerminated() || spaceObject.isTerminated()) </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this space object 
	 * 			and the other space object don't belong to the same world. <br>
	 * 	<code>	| (!this.hasAsWorld(spaceObject.getWorld())) </code><br>
	 */
	public double getDistanceBetween(SpaceObject spaceObject) 
			throws IllegalStateException, IllegalArgumentException {
		if(spaceObject == null)
			throw new IllegalArgumentException();
		//more strict test than hasAsWorld(...) so needs to be ahead of the method
		if (this.isTerminated() || spaceObject.isTerminated())
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		return Vector.distance(this.getPosition(), spaceObject.getPosition())-(this.getRadius() + spaceObject.getRadius());
	}
	
	/**
	 * Checks whether the space object overlaps with the given space object.
	 * 
	 * @param 	spaceObject
	 * 			The concurrent spaceObject that has to be checked for overlapping
	 * @return	True if and only if different space objects overlap. <br>
	 * 	<code>	| if (this != spaceObject)<br>
	 * 			| 	then result == (Util.fuzzyLessThanOrEqualTo(this.getDistanceBetween(spaceObject),0.0)) </code><br>
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a space object.<br>
	 * <code>	| (spaceObject == null) </code><br>
	 * @note	A space object never overlaps with itself.
	 */
	public boolean overlap(SpaceObject spaceObject) 
			throws IllegalStateException, IllegalArgumentException {
		if (this == spaceObject)
			return false;
		return (Util.fuzzyLessThanOrEqualTo(this.getDistanceBetween(spaceObject),0.0));
	}
	
	/**
	 * Returns the time until the space object and the given space object will collide.
	 * The time will be expressed in seconds. 
	 * 
	 * @param 	spaceObject
	 * 			The concurrent space object
	 * @return	The time until the collision between a space object and itself is always equal 
	 * 			to positive infinity.
	 * 	<code>	| if (this == spaceObject) <br>
	 * 			|	then result == Double.POSITIVE_INFINITY </code><br>
	 * @return	The time until the space objects collide.
	 * <code><p>| let deltaVelocity = Vector.substraction(this.getVelocity(), spaceObject.getVelocity())<br>
	 * 			| && let deltaPosition = Vector.substraction(this.getPosition(), spaceObject.getPosition())<br>
	 * 			| && let velocityNorm = Vector.distance(this.getVelocity(), spaceObject.getVelocity())<br>
	 * 			| && let positionNorm = Vector.distance(this.getPosition(), spaceObject.getPosition())</p>
	 * 		<p>	| in<br>
	 *  		| 	dotProduct = Vector.dotProduct(deltaVelocity, deltaPosition)</p>
	 *  	<p>	|	in<br>
	 * 			| 		d = Math.pow(dotProduct,2)-(Math.pow(velocityNorm,2)*(Math.pow(positionNorm, 2)<br>
	 * 			|			-Math.pow(getDistanceBetween(spaceObject), 2)))</p>
	 * 		<p>	|		in<br>
	 * 			| 			result == -(dotProduct + Math.sqrt(d))/(Math.pow(velocityNorm,2))</p></code>
	 * @return	The time is infinite if the space objects will never collide.
	 * <code><p>| if ((Util.fuzzyGreaterThanOrEqualTo(dotProduct, 0.0)) || (Util.fuzzyLessThan(d, 0.0)))<br>
	 * 			| 	then result == Double.POSITIVE_INFINITY </p></code>
	 * @return	If the resulting time is negative, the returned time is equal to positive infinity.
	 * <code><p>| if (Util.fuzzyLessThan(result,0.0))	<br>
	 * 			|	then result == Double.POSITIVE_INFINITY </p></code>
	 * @return	If the resulting time for the collision between the space objects is equal to 0.0 
	 * 			and the space objects have just collided, the returned time is equal to positive infinity.
	 * <code><p>| if (Util.fuzzyEquals(result, 0.0) && this.hasRecentCollision())<br>
	 * 			|	then result == Double.POSITIVE_INFINITY </p></code>
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if this or the entry is not a space object.<br>
	 * 	<code>	| (spaceObject == null) </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this space object 
	 * 			or the other space object is already terminated.<br>
	 * 	<code>	| (this.isTerminated() || spaceObject.isTerminated()) </code><br>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if this space object's world is not the same as the other
	 * 			space object's world.<br>
	 * 	<code>	| (!this.hasAsWorld(spaceObject.getWorld())) </code><br>
	 */
	public double getTimeToCollision(SpaceObject spaceObject) 
			throws IllegalStateException, IllegalArgumentException {
		if(spaceObject == null)
			throw new IllegalArgumentException();
		//more strict test than hasAsWorld(...) so needs to be ahead of the method
		if (this.isTerminated() || spaceObject.isTerminated())
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		//otherwise problems in simulateCollisions(...)
		if (this == spaceObject)
			return Double.POSITIVE_INFINITY;
		
		Vector deltaVelocity = Vector.subtraction(this.getVelocity(), spaceObject.getVelocity());
		Vector deltaPosition = Vector.subtraction(this.getPosition(), spaceObject.getPosition());
		double velocityNorm = Vector.distance(this.getVelocity(), spaceObject.getVelocity());
		double positionNorm = Vector.distance(this.getPosition(), spaceObject.getPosition());
		double dotProduct = Vector.dotProduct(deltaVelocity, deltaPosition);

		double d = Math.pow(dotProduct,2)-(Math.pow(velocityNorm,2)*(Math.pow(positionNorm, 2)-Math.pow(this.getRadius() + spaceObject.getRadius(), 2)));
		if (Util.fuzzyGreaterThanOrEqualTo(dotProduct, 0.0))
			return Double.POSITIVE_INFINITY;
		if (Util.fuzzyLessThan(d, 0.0))
			return Double.POSITIVE_INFINITY;
		double result = -(dotProduct + Math.sqrt(d))/(Math.pow(velocityNorm,2));
		if (Util.fuzzyLessThan(result,0.0))			
			return Double.POSITIVE_INFINITY;
		if (Util.fuzzyEquals(result, 0.0) && this.hasRecentCollision())
			return Double.POSITIVE_INFINITY;
		else
			return result;
	}
	
	/**
	 * Returns the shortest time until the space object will collide with one of the boundaries of the world.
	 * The time will be expressed in seconds. 
	 * 
	 * @return	The time until the space object collides with one of the boundaries of the world.
	 * 			This time is obtained by calculating the collision time of the space object 
	 * 			with each of the boundaries and returning the smallest one out of them.
	 * <code><p>| let horizontalTime = Util.minimum((0.0 + this.getRadius() <br>
	 *			|	- this.getXPosition())/this.getXVelocity(), (this.getWorld().getWidth() - this.getRadius() <br>
	 *			|	- this.getXPosition())/this.getXVelocity())</p>
	 * 		<p>	| && let verticalTime = Util.minimum((0.0 + this.getRadius() <br>
	 * 			|	- this.getYPosition())/this.getYVelocity(), (this.getWorld().getHeight() - this.getRadius()<br>
	 *			|	- this.getYPosition())/this.getYVelocity())</p>
	 *		<p>	| in<br>
	 * 			| 	result == Util.minimum(horizontalTime, verticalTime)</code></p>
	 * @return	If the resulting time for the collision between the space object and the boundary 
	 * 			is equal to 0.0 and the space object has just collided with that boundary,
	 * 			the returned time is equal to positive infinity.
	 * <code><p>| if (Util.fuzzyEquals(result, 0.0) && this.hasRecentCollision())<br>
	 * 			|	then result == Double.POSITIVE_INFINITY </p></code>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the space object is already terminated
	 * 			or if it doesn't have a world in which he navigates.<br>
	 * 	<code>	| (this.isTerminated() || this.hasAsWorld(null)) </code>
	 */
	public double getTimeToCollision() throws IllegalStateException {
		if (this.isTerminated() || this.hasAsWorld(null))
			throw new IllegalStateException();
		double timeToLeftBound = (0.0 + this.getRadius() 
				- this.getXPosition())/this.getXVelocity();
		double timeToRightBound = (this.getWorld().getWidth() - this.getRadius()
				- this.getXPosition())/this.getXVelocity();
		double horizontalTime = Util.minimum(timeToLeftBound, timeToRightBound);
		
		double timeToLowerBound = (0.0 + this.getRadius() 
				- this.getYPosition())/this.getYVelocity();
		double timeToUpperBound = (this.getWorld().getHeight() - this.getRadius()
				- this.getYPosition())/this.getYVelocity();
		double verticalTime = Util.minimum(timeToLowerBound, timeToUpperBound);
		
		double result = Util.minimum(horizontalTime, verticalTime);
		if (Util.fuzzyLessThanOrEqualTo(result, 0.0) && this.hasRecentCollision())
			return Double.POSITIVE_INFINITY;
		return result;
	}
	
	/**
	 * Returns the point of collision between this space object and the other space object.
	 * 
	 * @param 	spaceObject
	 * 			The other space object
	 * @return	An array representing the collision position. This array is obtained by creating position
	 * 			vectors of the two existing space objects at the time the collision occurs and by adding 
	 * 			the exact colliding point, obtained by calculating its proportional distance between the 
	 * 			space object's vectors, to one of them. <br>
	 * <code><p>| let distanceOne = Vector.multiple(this.getVelocity(), this.getTimeToCollision(spaceObject))<br>
	 * 			| && let distanceTwo = Vector.multiple(spaceObject.getVelocity(), spaceObject.getTimeToCollision(this))<br>
	 * 			| && let proportion = this.getRadius()/(this.getRadius() + spaceObject.getRadius())</p>
	 * 		<p>	| in <br>
	 * 			|	newPositionOne = Vector.addition(this.getPosition(), distanceOne)<br>
	 * 			| && newPositionTwo = Vector.addition(spaceObject.getPosition(), distanceTwo)</p>
	 * 		<p>	|	in<br>
	 * 			|		vector = Vector.multiple(Vector.substraction(newPositionTwo, newPositionOne), proportion)</p>
	 * 		<p>	|		in<br>
	 * 			|			result == Vector.arrayVector(Vector.addition(vector, newPositionOne))</p></code><br>		
	 * @return	If the time until a collision is infinite, the returned collision position is empty.<br>
	 * 	<code>	| if(this.getTimeToCollision(spaceObject) == Double.POSITIVE_INFINITY)<br>
	 *			|	then return null</code>	
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the other space object is null.<br>
	 * 	<code>	| ((spaceObject == null)) </code><br>	 
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this or the other space object is already terminated.<br>
	 * 	<code>	| (this.isTerminated() || spaceObject.isTerminated())</code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this space object and the other space object don't navigate in the same world.<br>
	 * 	<code>	| (!this.hasAsWorld(spaceObject.getWorld()))</code><br>
	 */
	public double[] getCollisionPosition(SpaceObject spaceObject) 
			throws IllegalStateException, IllegalArgumentException {
		if(spaceObject == null)
			throw new IllegalArgumentException();
		if (this.isTerminated() || spaceObject.isTerminated())
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		if (this.getTimeToCollision(spaceObject) == Double.POSITIVE_INFINITY)
			return null;
		Vector vector = new Vector();
		Vector distanceOne = Vector.multiple(this.getVelocity(), this.getTimeToCollision(spaceObject));
		Vector newPositionOne = Vector.addition(this.getPosition(), distanceOne);
		Vector distanceTwo = Vector.multiple(spaceObject.getVelocity(), spaceObject.getTimeToCollision(this));
		Vector newPositionTwo = Vector.addition(spaceObject.getPosition(), distanceTwo);
		double proportion = this.getRadius()/(this.getRadius() + spaceObject.getRadius());
		vector = Vector.multiple(Vector.subtraction(newPositionTwo, newPositionOne), proportion);
		vector = Vector.addition(vector, newPositionOne);

		double[] vectorArray = Vector.arrayVector(vector);
		
		return vectorArray;
	}
	
	/**
	 * Return the point of collision between the space object and the boundary of the world
	 * it will encounter first. 
	 * 
	 * @return	An array representing the collision position. This array is obtained by calculating the 
	 * 			smallest collision time of the space object with a boundary of the world, updating the
	 * 			position of the space object with it and checking which side of the space object's 
	 * 			surface touches a boundary.
	 * @return	If the collision point is located at the right side of the space object, adds the space
	 * 			object's radius to the updated horizontal position of the space object and converts the new 
	 * 			position to an array.
	 * <code><p>| if (newPosition.X_COORD == getWorld().getWidth() - this.getRadius()) 
	 * 			|	then let distance = Vector.multiple(this.getVelocity(), this.getTimeToCollision())<br>
	 * 			| 	&&	 let newPosition = Vector.addition(this.getPosition(), distance) </p>
	 * 		<p> | 		 in<br> 
	 * 			|			vector = Vector.addition(newPosition,new Vector(this.getRadius(), 0.0)))</p></code>
	 * @return	If the collision point is located at the left side of the space object, subtracts the space
	 * 			object's radius from the updated horizontal position of the space object and converts the new 
	 *  		position to an array.
	 * <code><p>| if (newPosition.X_COORD == 0.0 + this.getRadius()) 
	 * 			|	then let distance = Vector.multiple(this.getVelocity(), this.getTimeToCollision())<br>
	 * 			| 	&&	 let newPosition = Vector.addition(this.getPosition(), distance) </p>
	 * 		<p> | 		 in<br> 
	 * 			|			vector = Vector.substraction(newPosition,new Vector(this.getRadius(), 0.0))</p></code>
	 * @return	If the collision point is located at the lower side of the space object, adds the space
	 * 			object's radius to the updated vertical position of the space object and converts the new 
	 * 			position to an array.
	 * <code><p>| if (newPosition.Y_COORD == getWorld().getHeight() - this.getRadius()) 
	 * 			|	then let distance = Vector.multiple(this.getVelocity(), this.getTimeToCollision())<br>
	 * 			| 	&&	 let newPosition = Vector.addition(this.getPosition(), distance) </p>
	 * 		<p> | 		 in<br> 
	 * 			|			vector = Vector.addition(newPosition,new Vector(this.getRadius(), 0.0))</p></code>
	 * @return	If the collision point is located at the upper side of the space object, subtracts the space
	 * 			object's radius from the updated vertical position of the space object and converts the new 
	 * 			position to an array.
	 * <code><p>| if (newPosition.Y_COORD == 0.0 + this.getRadius()) 
	 * 			|	then let distance = Vector.multiple(this.getVelocity(), this.getTimeToCollision())<br>
	 * 			| 	&&	 let newPosition = Vector.addition(this.getPosition(), distance) </p>
	 * 		<p> | 		 in<br> 
	 * 			|			vector = Vector.substraction(newPosition,new Vector(0.0, this.getRadius()))</p></code>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the space object is already terminated
	 * 			or if the space object doesn't belong to a world.<br>
	 * 	<code>	| (this.isTerminated() || !this.hasWorld()) </code><br>
	 */
	public double[] getCollisionPosition() throws IllegalStateException {
		if (this.isTerminated() || this.hasAsWorld(null))
			throw new IllegalStateException();
		
		Vector vector = new Vector();
		Vector distance = Vector.multiple(this.getVelocity(), this.getTimeToCollision());
		Vector newPosition = Vector.addition(this.getPosition(), distance);
		if (newPosition.X_COORD == getWorld().getWidth() - this.getRadius())
			vector = Vector.addition(newPosition,new Vector(this.getRadius(), 0.0));
		if (newPosition.X_COORD == 0.0 + this.getRadius())	
			vector = Vector.subtraction(newPosition,new Vector(this.getRadius(), 0.0));
		if (newPosition.Y_COORD == getWorld().getHeight() - this.getRadius())
			vector = Vector.addition(newPosition,new Vector(0.0, this.getRadius()));
		if (newPosition.Y_COORD == 0.0 + this.getRadius())	
			vector = Vector.subtraction(newPosition,new Vector(0.0, this.getRadius()));
		double[] vectorArray = Vector.arrayVector(vector);
		
		return vectorArray;
	}

	/**
	 * Makes the space object bounce when hitting a boundary of the world.
	 * 
	 * @post	The new horizontal velocity of the space object is equal to the inverted horizontal velocity
	 * 			if the space object hits one of the horizontal boundaries of the world. The space object
	 * 			remembers it has had a recent collision.<br>
	 * 	<code>	| if(!this.getWorld().isValidXPosition(this.getXPosition() + this.getRadius())<br>
	 *			|		|| !this.getWorld().isValidXPosition(this.getXPosition() - this.getRadius()))<br>
	 * 			| 	then (new this).getXVelocity() == -this.getXVelocity() 
	 * 			|		&& (new this).hasRecentCollision() == true </code><br>
	 * @post	The new vertical velocity of the space object is equal to the inverted vertical velocity
	 * 			if the space object hits one of the vertical boundaries of the world. The space object
	 * 			remembers it has had a recent collision.<br>
	 * 	<code>	| if(!this.getWorld().isValidYPosition(this.getYPosition() + this.getRadius())<br>
	 *			|		|| !this.getWorld().isValidYPosition(this.getYPosition() - this.getRadius()))<br>
	 *			|	then (new this).getYVelocity() == -this.getYVelocity() 
	 *			|		&& (new this).hasRecentCollision() == true</code><br>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the space object doesn't have a world attached to it
	 * 			or if the space object is terminated.<br>
	 * 	<code>	| (this.hasAsWorld(null) || this.isTerminated()) </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the space object could bounce neither horizontally
	 * 			nor vertically. This occurs when the object want to use this method while touching no boundary.
	 * <code><p>| (this.getWorld().isValidXPosition(this.getXPosition() + this.getRadius())<br>
	 *			| 	&& this.getWorld().isValidXPosition(this.getXPosition() - this.getRadius())<br>
	 * 			|	&& this.getWorld().isValidYPosition(this.getYPosition() + this.getRadius())<br>
	 *			|	&& this.getWorld().isValidYPosition(this.getYPosition() - this.getRadius()))</p></code>
	 */
	public void bounce() throws IllegalStateException {	
		boolean successful = false;
		if (this.hasAsWorld(null) || this.isTerminated())
			throw new IllegalStateException();
		//Can invert the horizontal AND vertical velocities (e.g. if collision in a corner)
		if(!this.getWorld().isValidXPosition(this.getXPosition() + this.getRadius())
				|| !this.getWorld().isValidXPosition(this.getXPosition() - this.getRadius())) {
			this.setVelocity(-this.getXVelocity(),this.getYVelocity());
			this.setRecentCollision(true);
			successful = true;
		}
		if(!this.getWorld().isValidYPosition(this.getYPosition() + this.getRadius())
				|| !this.getWorld().isValidYPosition(this.getYPosition() - this.getRadius())) {
			this.setVelocity(this.getXVelocity(),-this.getYVelocity());
			this.setRecentCollision(true);
			successful = true;
		}		
		//Cannot bounce if it doesn't touch any boundary; no doable with 'if' and 'else' only
		if (!successful) {
			throw new IllegalStateException();
		}
	}
	
	/**
	 * Makes the space object bounce when colliding another space object.
	 * 
	 * @param 	spaceObject
	 * 			The other space object
	 * @post	The new horizontal velocity of this space object is equal to its current horizontal 
	 * 			velocity augmented by the division of the horizontal impulse by its mass.<br>
	 * 			The new vertical velocity of this space object is equal to its current vertical 
	 * 			velocity augmented by the division of the vertical impulse by its mass.<br>
	 * 			This space object remembers it has had a recent collision.<br>
	 * 			The new horizontal velocity of the other space object is equal to its current horizontal 
	 * 			velocity reduced by the division of the horizontal impulse by its mass.<br>
	 * 			The new vertical velocity of the other space object is equal to its current vertical 
	 * 			velocity reduced by the division of the vertical impulse by its mass.<br>
	 * 			The other space object remembers it has had a recent collision.<br>
	 * <p><code>| let deltaVelocity = Vector.substraction(this.getVelocity(), spaceObject.getVelocity())<br>
	 * 			| && let deltaPosition = Vector.substraction(this.getPosition(), spaceObject.getPosition())<br>
	 * 			| && let touchingDistance = (this.getRadius() + spaceObject.getRadius())</p>
	 *       <p>| in <br>
	 * 			|	dotProduct = Vector.dotProduct(deltaVelocity, deltaPosition)<br>
	 * 			| && J = (2*this.getMass()*spaceObject.getMass()*dotProduct)/<br>
	 *			|		(touchingDistance*(this.getMass() + spaceObject.getMass()))</p>
	 *		 <p>|	in <br>
	 *			|		Jx = J*(spaceObject.getXPosition() - this.getXPosition())/touchingDistance<br>
	 *			|	&&	Jy = J*(spaceObject.getYPosition() - this.getYPosition())/touchingDistance</p>
	 *		 <p>|		in <br>
	 *			|			(new this).getXVelocity() == this.getXVelocity() + Jx/this.getMass()<br>
	 *			|		&&	(new this).getYVelocity() == this.getYVelocity() + Jy/this.getMass()<br>
	 *			|		&&	(new this).hasRecentCollision() == true<br>
	 *			|		&&	(new spaceObject).getXVelocity() == spaceObject.getXVelocity() - Jx/spaceObject.getMass()<br>
	 *			|		&&	(new spaceObject).getYVelocity() == spaceObject.getYVelocity() - Jy/spaceObject.getMass()<br>
	 *			|		&&	(new spaceObject).hasRecentCollision() == true</code></p>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this space object is a bullet.
	 * 			| this.isBullet()
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the other space object is null.<br>
	 * 			| spaceObject == null
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the distance between both space objects is not equal to 0.<br>
	 * 			| !Util.fuzzyEquals(this.getDistanceBetween(spaceObject), 0)
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if this space object or the other space object doesn't have a world.<br>
	 * <code>	| (this.hasAsWorld(null) || spaceObject.hasAsWorld(null)) </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this space object and the other space object don't navigate
	 * 			inside the same world.<br>
	 * <code>	| (!this.hasAsWorld(spaceObject.getWorld()))  </code><br>
	 */
	public void bounce(SpaceObject spaceObject) throws IllegalStateException, IllegalArgumentException {
		if(!Util.fuzzyEquals(this.getDistanceBetween(spaceObject), 0))
			throw new IllegalStateException();
		if (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		
		Vector deltaVelocity = Vector.subtraction(this.getVelocity(), spaceObject.getVelocity());
		Vector deltaPosition = Vector.subtraction(this.getPosition(), spaceObject.getPosition());
		double touchingDistance = (this.getRadius() + spaceObject.getRadius());
		double dotProduct = Vector.dotProduct(deltaVelocity, deltaPosition);
		double J = (2*this.getMass()*spaceObject.getMass()*dotProduct)/
				(touchingDistance*(this.getMass() + spaceObject.getMass()));
		double Jx = J*(spaceObject.getXPosition() - this.getXPosition())/touchingDistance;
		double Jy = J*(spaceObject.getYPosition() - this.getYPosition())/touchingDistance;
		this.setVelocity(this.getXVelocity() + Jx/this.getMass(),
				this.getYVelocity() + Jy/this.getMass());
		spaceObject.setVelocity(spaceObject.getXVelocity() - Jx/spaceObject.getMass(),
				spaceObject.getYVelocity() - Jy/spaceObject.getMass());			
		this.setRecentCollision(true);
		spaceObject.setRecentCollision(true);
	}

	/**
	 * Makes the space object explode when hitting an obstacle. 
	 * 
	 * @post	The new state of the space object becomes terminated. <br>
	 * 			The space object remembers it has had a recent collision.<br>
	 * 	<code>	| (new this).isTerminated() == true 
	 * 			|	&& (new this).hasRecentCollision() == true</code><br>
	 * @note	This method is overriden in the class Asteroid.
	 */
	public void explode() {
		this.setRecentCollision(true);
		this.terminate();
	}
	
	/**
	 * Makes the space object evolve together with its world for a given duration.
	 * 
	 * @param 	duration
	 * 			The given duration of the evolution.
	 * @effect	The space object will move for a given duration.
	 * 			| this.move(duration)
	 * @effect	If the duration of the evolution is greater than 0.0,
	 * 			remember that the space object hasn't collided recently.
	 * 			| if (duration > 0.0)
	 * 			|	then this.setRecentCollision(false)
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the space object doesn't have a world attached to it.<br>
	 *  <code>	| (this.hasAsWorld(null)) </code><br>
	 */
	public void evolve(double duration) throws IllegalStateException {
		if (this.hasAsWorld(null))
			throw new IllegalStateException();
		this.move(duration);
		if(Util.fuzzyGreaterThan(duration, 0.0))
			this.setRecentCollision(false);
	}
	
	/**
	 * Returns true if and only if the space object has collided at this very moment.
	 * 
	 * @return	A boolean returning true if the space object has had a collision at this very moment.
	 */
	@Basic
	public boolean hasRecentCollision() {
		return this.recentCollision;
	}
	
	/**
	 * Sets the recent collision state of the space object.
	 * 
	 * @param 	active
	 * 			The recent collision state of the space object
	 */
	public void setRecentCollision(boolean active) {
		this.recentCollision = active;
	}
	
	/**
	 * A boolean asserting that the space object has had a collision at this very moment.
	 * 
	 * @note	This boolean allows us to retain the space object from evolving during 0.0 seconds
	 * 			in case of an infinite loop due to a collision.
	 */
	private boolean recentCollision = false;

	/**
	 * Returns the next collision between the space object and another space object. 
	 * 
	 * @param 	spaceObject
	 * 			The other space object
	 * @return	An object <code>spaceObjectCollision</code> containing the two space objects involved in the collision.<br>
	 * 	<code>	| result == new SpaceObjectCollision(this, spaceObject) </code>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this space object or the concurrent space object  
	 * 			doesn't navigate within a world.<br>
	 * 			| (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
	 * @throws	IllegalStateException	
	 * 			Will throw an exception if this space object and the concurrent space object
	 * 			don't navigate within the same world.
	 * 			| (!this.hasAsWorld(spaceObject.getWorld()))
	 * @throws 	IllegalArgumentException
	 * 			Will throw an exception if the space object is not of the type asteroid, bullet or ship.
	 * 			| !(spaceObject.isBullet() || spaceObject.isAsteroid() || spaceObject.isShip()) 
	 * 			
	 */
	protected abstract SpaceObjectCollision getNextCollision(SpaceObject spaceObject) 
			throws IllegalStateException, IllegalArgumentException;
	
	/**
	 * Returns the next collision between the space object and a boundary of the world. 
	 * 
	 * @return	An object <code>boundaryCollision</code> containing the current space object involved in the collision.<br>
	 *  <code>  | result == new BoundaryCollision(this)</code><br>
	 */
	public BoundaryCollision getNextCollisionToBoundary() {
		return new BoundaryCollision(this);
	}
	
}
