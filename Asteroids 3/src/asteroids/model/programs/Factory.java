package asteroids.model.programs;

import java.util.*;

import asteroids.model.programs.expressions.*;
import asteroids.model.programs.parsing.ProgramFactory;
import asteroids.model.programs.parsing.ProgramFactory.ForeachType;
import asteroids.model.programs.statements.*;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Factory implements ProgramFactory<Expression, Statement, Type> {

	 @Override
	  public Expression createDoubleLiteral(int line, int column, double d) {
		  return new DoubleLiteral(d);		  
	  }
	  
	  @Override
	  public Expression createBooleanLiteral(int line, int column, boolean b) {
		  return new BooleanLiteral(b);
	  }
	  
	  @Override
	  public Expression createAnd(int line, int column, Expression e1, Expression e2) {
		  	  return new And(e1, e2);
	  }
	  
	  @Override
	  public Expression createOr(int line, int column, Expression e1, Expression e2) {
		  	  return new Or(e1, e2);
	  }
	  
	  @Override
	  public Expression createNot(int line, int column, Expression e) {
			  return new Not( e);
	  }
	  
	  @Override
	  public Expression createNull(int line, int column) {
		  return new Null();
	  }
	  
	  @Override
	  public Expression createSelf(int line, int column) {
		  return new Self();
	  }
	  
	  @Override
	  public Expression createGetX(int line, int column, Expression e) {
		  return new HorizontalPosition(e);
	  }
	  
	  @Override
	  public Expression createGetY(int line, int column, Expression e) {
		  return new VerticalPosition(e);
	  }
	  
	  @Override
	  public Expression createGetVX(int line, int column, Expression e) {
		  return new HorizontalVelocity(e);
	  }
	  
	  @Override
	  public Expression createGetVY(int line, int column, Expression e) {
		  return new VerticalVelocity(e);
	  }
	  
	  @Override
	  public Expression createGetRadius(int line, int column, Expression e) {
		  return new Radius(e);
	  }
	  
	  @Override
	  public Expression createGetDirection(int line, int column) {
		  return new Direction();
	  }

	@Override
	  public Expression createVariable(int line, int column, String name) {
		try {
			return new Variable(name);
		} catch(NullPointerException exc){
			System.out.println("NullPointer create variable");
			return new Null();
		}
	  }
	  
	  @Override
	  public Expression createLessThan(int line, int column, Expression e1, Expression e2) {
		  return new LessThan( e1,  e2); 
	  }
	  
	  @Override
	  public Expression createGreaterThan(int line, int column, Expression e1, Expression e2) {
		  return new GreaterThan( e1,  e2); 
	  }
	  
	  @Override
	  public Expression createLessThanOrEqualTo(int line, int column, Expression e1, Expression e2) {
		  return new LessThanOrEqualTo( e1,  e2); 
	  }
	  
	  @Override
	  public Expression createGreaterThanOrEqualTo(int line, int column, Expression e1, Expression e2) {
		  return new GreaterThanOrEqualTo( e1,  e2);
	  }
	  
	  @Override
	  public Expression createEquality(int line, int column, Expression e1, Expression e2) {
		  return new EqualTo( e1,  e2);
	  }
	  
	  @Override
	  public Expression createInequality(int line, int column, Expression e1, Expression e2) {
		  return new NotEqualTo( e1,  e2);
	  }
	  
	  @Override
	  public Expression createAdd(int line, int column, Expression e1, Expression e2) {
		  return new Addition( e1, e2);
	  }
	  
	  @Override
	  public Expression createSubtraction(int line, int column, Expression e1, Expression e2) {
		  return new Subtraction( e1, e2);
	  }
	  
	  @Override
	  public Expression createMul(int line, int column, Expression e1, Expression e2) {
		  return new Multiplication( e1, e2);
	  }
	  
	  @Override
	  public Expression createDivision(int line, int column, Expression e1, Expression e2) {
		  return new Division( e1, e2);
	  }
	  
	  @Override
	  public Expression createSqrt(int line, int column, Expression e) {
		  return new SquareRoot( e);
	  }
	  
	  @Override
	  public Expression createSin(int line, int column, Expression e) {
		  return new Sine( e);
	  }
	  
	  @Override
	  public Expression createCos(int line, int column, Expression e) {
		  return new Cosine( e);
	  }

	  
	  @Override
	  public Statement createEnableThruster(int line, int column) {
		  return new EnableThruster((Self) createSelf(line, column));
	  }
	  
	  @Override
	  public Statement createDisableThruster(int line, int column) {
		  return new DisableThruster((Self) createSelf(line, column));
	  }
	  
	  @Override
	  public Statement createFire(int line, int column) {
		  return new Shoot((Self) createSelf(line, column));
	  }
	  
	  @Override
	  public Statement createTurn(int line, int column, Expression angle) {
		  return new Turn((Self) createSelf(line, column),  angle);
	  }
	  
	  @Override
	  public Statement createAssignment(int line, int column, String variable, Expression rhs) {
		  return new Assignment(variable, rhs);
	  }
	  
	  @Override
	  public Statement createIf(int line, int column, Expression condition, Statement then, Statement otherwise) {
		  return new IfThenElse( condition, then, otherwise);
	  }
	  
	  @Override
	  public Statement createWhile(int line, int column, Expression condition, Statement body) {
		  return new While( condition, body);
	  }
	  
	  @Override
	  public Statement createForeach(int line, int column, ForeachType type, String variableName, Statement body) {
	  switch (type) {
		  case SHIP:
			  return new ForEachShip(variableName, body);
		  case ASTEROID:
			  return new ForEachAsteroid(variableName, body);
		  case BULLET:
			  return new ForEachBullet(variableName, body);
		  case ANY:
			  return new ForEachAny(variableName, body);
		  default:
			  return null;
		  }
	  }
	  
	  @Override
	  public Statement createSkip(int line, int column) {
		  return new Skip((Self) createSelf(line, column));
	  }
	  
	  @Override
	  public Statement createSequence(int line, int column, List<Statement> statements) {
		  return new Sequence(statements);
	  }
	  
	  @Override
	  public Statement createPrint(int line, int column, Expression e) {
		  return new Print(e);
	  }
	  
	  
	  @Override
	  public Type createDoubleType() {
		  return new Doub(0);
	  }
	  
	  @Override
	  public Type createBooleanType() {
		  return new Bool(false);
	  }
	  
	  @Override
	  public Type createEntityType() {
		  return new Entity(null);
	  }
	
}