package asteroids.model.programs;

import java.util.*;

import be.kuleuven.cs.som.annotate.*;

import asteroids.model.Ship;
import asteroids.model.programs.expressions.*;
import asteroids.model.programs.statements.*;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Program {
	
	public Program(Map<String,Type> globals, Statement statement) {
		System.out.println("Started program creation");
		this.globals = globals;
		this.statements = new ArrayList<Statement>();
		this.variables = new HashMap<String, Expression>();
		dissecate(statement);
		fillVariables();
		timeSinceLastExecution = 0;
	}

	@Basic @Immutable
	public Map<String, Type> getGlobals() {
	return this.globals;
	}
	  
	@Basic @Immutable
	public Map<String, Expression> getVariables() {
		return this.variables;
	}
	
	private void fillVariables() {
		for(String string: globals.keySet()) {
			Variable newVar = new Variable(string, globals.get(string).getClass());
			System.out.println("filled variable: "+string);
			newVar.setProgram(this);
			variables.put(string, newVar);
		}
	}
	
	private final Map<String, Type> globals;
	private final Map<String, Expression> variables;
	  
	public boolean actionEncountered() {
		return this.actionEncountered;
	}
	
	public void encounteredAction(boolean encountered) {
		this.actionEncountered = encountered;
	}
	
	private boolean actionEncountered;
	
	@Basic
	public Ship getShip() {
		return this.ship;
	}
	  
	public void setShip(Ship ship) {
		this.ship = ship;
		for(Statement statement : getStatements()) {
			statement.applyTo(ship);
			statement.setProgram(this);
		}
		if (ship != null && !ship.hasAsProgram(this))
			  ship.setProgram(this);
	  }
	  
	  public void removeShip() {
		  Ship shipTemp = this.getShip();
		  this.setShip(null);
		  shipTemp.removeProgram();
	  }
	  
	  public boolean hasAsShip(Ship ship) {
		  return (this.getShip() == ship);
	  }
	  
	  private Ship ship;
	  
	  public void execute(double duration) {
		  actionEncountered = false;
		  if (this.typeCheck())
			  System.out.println("Type checking succeeded in execute in Program");
		  else
			  System.out.println("Type checking failed in execute in Program");
		  this.setTimeSinceLastExecution(this.getTimeSinceLastExecution() + duration);
		  while(this.getTimeSinceLastExecution() > TIME_BETWEEN_EXECUTIONS)  {
			  try {
				  while(!actionEncountered) {
					  Statement nextStatement = getStatements().get(index);
					  System.out.println("executing one program statement: "+index);
					  nextStatement.execute();
					  if((ConditionalStatement.class.isAssignableFrom(nextStatement.getClass()) 
							  && !((ConditionalStatement) nextStatement).loopCompleted()))
						  index --;
					  index++;
					  if(index == getStatements().size()) {
						  index = 0;
						  System.out.println("Completed program run");
						  System.out.println("New Program starts here ###########################################");
					  }
				  }
			  } catch (NullPointerException exc) {
				  System.out.println("null statements in Program");
			  } 
			  this.setTimeSinceLastExecution(this.getTimeSinceLastExecution() - TIME_BETWEEN_EXECUTIONS);
		  }
	  }
	  
	  private int index = 0;

	@Basic
	  public double getTimeSinceLastExecution() {
		  return this.timeSinceLastExecution;
	  }
	  
	  public void setTimeSinceLastExecution(double timeSinceLastExecution) {
		  this.timeSinceLastExecution = timeSinceLastExecution;
	  }
	  
	  public static final double TIME_BETWEEN_EXECUTIONS = 0.2;
	  
	  private double timeSinceLastExecution;
	  
	  @Basic @Immutable
	  public ArrayList<Statement> getStatements() {
		  return this.statements;
	  }

	private void dissecate(Statement statement) {
		if(statement instanceof Sequence) {
			for(Statement st : ((Sequence) statement).getStatements()) {
				this.dissecate(st);
			}
		} else 
			this.getStatements().add(statement);	
	}

	private final ArrayList<Statement> statements;
	
	public boolean typeCheck() {
		for (Statement statement : this.getStatements()) {
			if (!statement.typeCheck())
				return false;
		}
		return true;
	}
	 	
}
