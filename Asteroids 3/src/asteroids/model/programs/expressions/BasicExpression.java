package asteroids.model.programs.expressions;

import asteroids.model.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class BasicExpression extends Expression {

	@Override
	//final so that this method cannot be overriden
	public final boolean hasAsSubExpression(Expression expression) {	
		return expression == this;
	}
	
	@Override
	public boolean equals(Object other) {
		return this.getResult() == ((Expression) other).getResult();
	}
	
	@Override
	public void assignTo(SpaceObject spaceObject) {
		
	}

}
