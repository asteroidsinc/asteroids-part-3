package asteroids.model.programs.expressions;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class Literal extends BasicExpression {
	
	@Override
	public boolean typeCheck() {
		return true;
	}
	
}
