package asteroids.model.programs.expressions;

import asteroids.model.programs.types.Bool;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class BooleanLiteral extends Literal {

	public BooleanLiteral(boolean result) {
		this.result = result;
	}

	public BooleanLiteral() {
		// We must explicitly initialize the final instance variable value in
		// this constructor, either in a direct way or in an indirect way.
		this(false);
	}

	public final static BooleanLiteral FALSE = new BooleanLiteral();

	@Override
	@Basic @Immutable
	public Bool getResult() {
		return new Bool(result);
	}

	private final boolean result;

	@Override
	public boolean equals(Object other) {
		return (other instanceof BooleanLiteral)
				&& (this.getResult() == ((BooleanLiteral) other).getResult());
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Bool.class;
	}
	
	@Override
	public String toString() {
		return Boolean.toString(this.getResult().getValue());
	}
	
}
