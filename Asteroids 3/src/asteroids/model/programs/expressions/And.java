package asteroids.model.programs.expressions;

import asteroids.exception.IllegalOperandException;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class And extends LogicExpression {

	public And(Expression left, Expression right)
			throws IllegalOperandException {
		super(left, right);
	}

	@Override
	public String getOperatorSymbol() {
		return "&&";
	}

	@Override
	public Bool getResult() {
		try{
			return new Bool(((Bool) getLeftOperand().getResult()).getValue() && ((Bool) getRightOperand().getResult()).getValue());
		}catch(Exception exc){
			System.out.println("failed to get AND result");
			return new Bool(false);
		}
	}
	
	@Override
	public boolean typeCheck() {
		if(super.typeCheck())
			return true;
		else {
			System.out.println("Type check failed in And");
			return false;
		}
	}

}
