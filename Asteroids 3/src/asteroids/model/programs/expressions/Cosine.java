package asteroids.model.programs.expressions;

import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Cosine extends UnaryExpression {

	public Cosine(Expression operand) throws IllegalArgumentException {
		super(operand);
	}

	@Override
	public String getOperatorSymbol() {
		return "cos";
	}
	
	public Doub getResult() {
		try{
			return new Doub(Math.cos(((Doub) this.getOperand().getResult()).getValue()));
			}catch(Exception exc){
				System.out.println("failed to get cosine result");
				return new Doub(0);
			}
		
	}
	
	@Override
	public boolean typeCheck() {
		if(super.typeCheck())
			return true;
		else {
			System.out.println("Type check failed in Cosine");
			return false;
		}
	}
	
}
