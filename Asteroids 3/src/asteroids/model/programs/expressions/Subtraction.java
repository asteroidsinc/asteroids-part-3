package asteroids.model.programs.expressions;

import asteroids.exception.IllegalOperandException;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Subtraction extends ArithmeticExpression {

	public Subtraction(Expression left, Expression right)
			throws IllegalOperandException {
		super(left, right);
	}

	@Override
	public Doub getResult() {
		try {
			return new Doub(((Doub) getLeftOperand().getResult()).getValue() - ((Doub) getRightOperand().getResult()).getValue());
		} catch(Exception exc){
			System.out.println("failed to get subtraction result");
			return new Doub(0);
		}
	}

	@Override
	public String getOperatorSymbol() {
		return "-";
	}
	
	@Override
	public boolean typeCheck() {
		if(super.typeCheck())
			return true;
		else {
			System.out.println("Type check failed in Subtraction");
			return false;
		}
	}

}