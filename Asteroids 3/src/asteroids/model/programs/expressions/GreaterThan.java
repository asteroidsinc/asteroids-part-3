package asteroids.model.programs.expressions;

import asteroids.exception.IllegalOperandException;
import asteroids.model.programs.types.*;

import asteroids.util.Util;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class GreaterThan extends ComparisonExpression {

	public GreaterThan(Expression left, Expression right)
			throws IllegalOperandException {
		super(left, right);
	}

	@Override
	public String getOperatorSymbol() {
		return ">";
	}

	@Override
	public Bool getResult() {
		try {
			return new Bool(Util.fuzzyGreaterThan(((Doub) this.getLeftOperand().getResult()).getValue(), ((Doub) this.getRightOperand().getResult()).getValue()));
		} catch(Exception exc){
			System.out.println("failed to get greater than result");
			return new Bool(false);
		}
		
	}
	
	@Override
	public boolean typeCheck() {
		if(super.typeCheck())
			return true;
		else {
			System.out.println("Type check failed in Greater Than");
			return false;
		}
	}

}
