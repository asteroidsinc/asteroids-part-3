package asteroids.model.programs.expressions;

import be.kuleuven.cs.som.annotate.*;
import asteroids.exception.IllegalOperandException;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class ArithmeticExpression extends BinaryExpression {

	public ArithmeticExpression(Expression left, Expression right)
			throws IllegalOperandException {
		super(left, right);
	}
	
	@Override
	public boolean typeCheck() {
		return (Doub.class == getLeftOperand().getType()) && (Doub.class == getRightOperand().getType())
				&& (this.getLeftOperand().typeCheck()) && (this.getRightOperand().typeCheck());
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	@Immutable
	public Class getType() {
		return Doub.class;
	}

}
