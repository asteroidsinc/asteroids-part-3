package asteroids.model.programs.expressions;

import asteroids.model.programs.types.Doub;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class VerticalPosition extends EntityExpression {

	public VerticalPosition(Expression subject) {
		super(subject);
	}

	@Override
	public Doub getResult() {
		if (!(this.getSubject() instanceof Null))
			return new Doub((((Entity) this.getSubject().getResult()).getValue()).getYPosition());
		else
			return new Doub(0);
	}

	@Override
	public String toString() {
		return "gety";
	}
	
	@Override
	public boolean typeCheck() {
		if(super.typeCheck())
			return true;
		else {
			System.out.println("Type check failed in Vertical Position");
			return false;
		}
	}

}
