package asteroids.model.programs.expressions;

import asteroids.model.programs.Program;
import be.kuleuven.cs.som.annotate.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class ComposedExpression extends Expression {
	
	@Override
	public boolean equals(Object other) {
		if ((other == null) || (getClass() != other.getClass()))
			return false;
		ComposedExpression otherExpr = (ComposedExpression) other;
		if (getNbOperands() != otherExpr.getNbOperands())
			return false;
		for (int pos = 1; pos <= getNbOperands(); pos++)
			if (!getOperandAt(pos).equals(otherExpr.getOperandAt(pos)))
				return false;
		return true;
	}
	
	@Basic @Raw
	public abstract int getNbOperands();

	@Basic @Raw
	public abstract Expression getOperandAt(int index)
			throws IndexOutOfBoundsException;
	
	public boolean canHaveAsOperandAt(Expression expression, int index) {
		return (expression != null) && (!expression.hasAsSubExpression(this)) && (index > 0);
	}
	
	protected abstract void setOperandAt(int index, Expression operand);
	
	@Override
	public boolean hasAsSubExpression(Expression expression) {
		if (expression == this)
			return true;
		for (int pos = 1; pos <= getNbOperands(); pos++)
			if (getOperandAt(pos).hasAsSubExpression(expression))
				return true;
		return false;
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		this.getOperandAt(1).setProgram(program);
		try {
			this.getOperandAt(2).setProgram(program);
		} catch(Exception exc) {
			System.out.println("Unary expressions heeft geen 2e operand");
		}
	}
	
	public abstract String getOperatorSymbol();
}
