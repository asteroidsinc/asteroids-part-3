package asteroids.model.programs.expressions;

import asteroids.model.programs.types.*;

import asteroids.exception.IllegalOperandException;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class ComparisonExpression extends BinaryExpression {

	public ComparisonExpression(Expression left, Expression right)
			throws IllegalOperandException {
		super(left, right);
	}

	@Override
	public boolean typeCheck() {
		return (Doub.class == getLeftOperand().getType()) && (Doub.class == getRightOperand().getType())
				&& (this.getLeftOperand().typeCheck()) && (this.getRightOperand().typeCheck());
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Bool.class;
	}
	
}
