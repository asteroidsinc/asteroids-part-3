package asteroids.model.programs.expressions;

import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Not extends UnaryExpression {

	public Not(Expression operand)
			throws IllegalArgumentException {
		super(operand);
	}
	

	@Override
	public String getOperatorSymbol() {
		return "!";
	}

	@Override
	public Bool getResult() {
		try{
			return new Bool(!((Bool) getOperand().getResult()).getValue());
			}catch(Exception exc){
				System.out.println("failed to get not result");
				return new Bool(false);
			}
		
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Bool.class;
	}
	
	@Override
	public boolean typeCheck() {
		if(this.getOperand().typeCheck() && (Bool.class == getOperand().getType()))
			return true;
		else {
			System.out.println("Type check failed in Not");
			return false;
		}
	}
	
}
