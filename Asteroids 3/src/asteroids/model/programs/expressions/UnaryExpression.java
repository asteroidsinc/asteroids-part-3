package asteroids.model.programs.expressions;

import asteroids.model.SpaceObject;
import asteroids.model.programs.types.Doub;
import be.kuleuven.cs.som.annotate.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class UnaryExpression extends ComposedExpression {

	@Model
	protected UnaryExpression(Expression operand)
			throws IllegalArgumentException {
		if (!canHaveAsOperandAt(operand,1))
			throw new IllegalArgumentException();
		setOperandAt(1, operand);
	}

	@Override
	@Basic
	public final int getNbOperands() {
		return 1;
	}

	@Override
	public final Expression getOperandAt(int index)
			throws IndexOutOfBoundsException {
		if (index != 1)
			throw new IndexOutOfBoundsException();
		return getOperand();
	}

	@Basic
	public Expression getOperand() {
		return operand;
	}
	
	@Override
	public boolean canHaveAsOperandAt(Expression expression, int index) {
		return super.canHaveAsOperandAt(expression, index) && (index == 1);
	}

	@Override
	protected void setOperandAt(int index, Expression operand) {
		this.operand = operand;
	}

	private Expression operand;

	@Override
	public String toString() {
		if (Expression.class.isAssignableFrom(getOperand().getClass()))
			return getOperatorSymbol() + "(" + getOperand().toString() + ")";
		throw new Error("Unknown expression type!");
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Doub.class;
	}
	
	@Override
	public void assignTo(SpaceObject spaceObject) {
		this.getOperand().assignTo(spaceObject);
	}
	
	@Override
	public boolean typeCheck() {
		return this.getOperand().typeCheck() && (Doub.class == getOperand().getType());
		
	}

}
