package asteroids.model.programs.expressions;

import java.util.Date;
import java.util.Map;

import be.kuleuven.cs.som.annotate.Basic;

import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import asteroids.model.programs.types.Type;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class Expression {
	
	@Override
	public abstract String toString();
	
	@Override
	public abstract boolean equals(Object other);
	
	public abstract Type getResult();
	
	public abstract boolean hasAsSubExpression(Expression expression);
	
	@SuppressWarnings("rawtypes")
	public abstract Class getType();
	
	public abstract boolean typeCheck();
	
	public abstract void assignTo(SpaceObject spaceObject);
	
	public Map<String, Expression> getVariables() {
		try{
			return this.getProgram().getVariables();
		}catch(NullPointerException exc){
			Date date = new Date();
			System.out.println("Expression NullPointer (fetching variables) at: "+date.getTime());
			return null;
		}
	}
	
	public Map<String, Type> getGlobals() {
		return this.getProgram().getGlobals();
	}
	
	@Basic
	public Program getProgram() {
		if(program == null)
			System.out.println("Returning null program in expression: ");
		return this.program;
	}
	
	public void setProgram(Program program) {
		this.program = program;
	}
	
	private Program program;
	
}
