package asteroids.model.programs.expressions;

import asteroids.model.Ship;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Direction extends EntityExpression {

	public Direction() {
		super(new Self());
	}

	@Override
	public Doub getResult() {
		return new Doub(((Ship) ((Self) this.getSubject()).getSpaceObject()).getDirection());
	}

	@Override
	public String toString() {
		return "getdir";
	}
	
	@Override
	public boolean typeCheck() {
		if(super.typeCheck())
			return true;
		else {
			System.out.println("Type check failed in Direction");
			return false;
		}
	}

}
