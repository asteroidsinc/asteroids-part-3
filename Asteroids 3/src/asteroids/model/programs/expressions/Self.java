package asteroids.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import asteroids.model.Ship;
import asteroids.model.SpaceObject;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Self extends Expression {

	public Self() {
	}

	@Override
	public Entity getResult() {
		return new Entity(this.getSpaceObject());
	}
	
	@Override
	public String toString() {
		return "self";
	}
	
	@Basic
	public SpaceObject getSpaceObject() {
		return this.spaceObject;
	}

	public void setSpaceObject(SpaceObject spaceObject) {
		if (spaceObject instanceof Ship)
			this.spaceObject = spaceObject;
	}
	
	private SpaceObject spaceObject;

	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Entity.class;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Self))
			return false;
		return (this.getSpaceObject() == ((Self) other).getSpaceObject());
	}

	@Override
	public boolean hasAsSubExpression(Expression expression) {
		return (expression == this);
	}

	@Override
	public boolean typeCheck() {
		return true;
	}
	
	@Override
	public void assignTo(SpaceObject spaceObject) {
		if (spaceObject instanceof Ship)
			this.setSpaceObject(spaceObject);
	}

}
