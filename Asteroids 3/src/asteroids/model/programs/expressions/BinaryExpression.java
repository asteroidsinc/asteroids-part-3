package asteroids.model.programs.expressions;

import asteroids.exception.IllegalOperandException;
import asteroids.model.SpaceObject;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class BinaryExpression extends ComposedExpression {

	@Model
	protected BinaryExpression(Expression left, Expression right)
			throws IllegalOperandException {
		if (!canHaveAsOperandAt(left,1))
			throw new IllegalOperandException(this, left);
		if (!canHaveAsOperandAt(right,2))
			throw new IllegalOperandException(this, right);
		setOperandAt(1, left);
		setOperandAt(2, right);
	}
	
	@Override
	@Raw
	protected void setOperandAt(int index, Expression operand) throws IllegalOperandException {
		if (!this.canHaveAsOperandAt(operand, index))
			throw new IllegalOperandException(this, operand);
		if (index == 1)
			this.leftOperand = operand;
		else
			this.rightOperand = operand;
	}

	@Basic
	public Expression getLeftOperand() {
		return leftOperand;
	}

	private Expression leftOperand;

	@Basic
	public Expression getRightOperand() {
		return rightOperand;
	}

	private Expression rightOperand;
	

	@Override
	@Basic
	public final int getNbOperands() {
		return 2;
	}
	
	@Override
	@Raw
	public final Expression getOperandAt(int index)
			throws IndexOutOfBoundsException {
		if ((index != 1) && (index != 2))
			throw new IndexOutOfBoundsException();
		if (index == 1)
			return getLeftOperand();
		else
			return getRightOperand();
	}
	
	@Override
	public boolean canHaveAsOperandAt(Expression expression, int index) {
		return super.canHaveAsOperandAt(expression, index) && ( (index == 1) || (index == 2) );
	}
	
	@Override
	public String toString() {
		String result;
		if (getLeftOperand() instanceof BasicExpression)
			result = getLeftOperand().toString();
		else if (getLeftOperand() instanceof ComposedExpression)
			result = "(" + getLeftOperand().toString() + ")";
		else
			throw new Error("Unknown expression type!");
		result += getOperatorSymbol();
		if (getRightOperand() instanceof BasicExpression)
			result += getRightOperand().toString();
		else if (getRightOperand() instanceof ComposedExpression)
			result += "(" + getRightOperand().toString() + ")";
		else
			throw new Error("Unknown expression type!");
		return result;
	}
	
	@Override
	public void assignTo(SpaceObject spaceObject) {
		this.getLeftOperand().assignTo(spaceObject);
		this.getRightOperand().assignTo(spaceObject);
	}
	
}
