package asteroids.model.programs.expressions;

import be.kuleuven.cs.som.annotate.*;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Variable extends BasicExpression {

	public Variable(String name) {
		this.name = name;
	}
	
	public Variable(String name, @SuppressWarnings("rawtypes") Class type) {
		this.name = name;
		this.setType(type);
	}

	@Override
	@Basic @Immutable
	public String toString() {
		if (this.getResult() instanceof Entity)
			return this.name;
		return (this.getResult()).toString();
	}
	
	@Override
	public boolean equals(Object other) {
		return this.getResult().equals(((Expression) other).getResult());
	}
	
	public String getName() {
		return this.name;
	}
	
	private final String name;
	
	@SuppressWarnings("rawtypes")
	@Override
	@Raw
	public Class getType() {
		if(type !=  null)
			return this.type;
		return this.getVariables().get(getName()).getType();
	}
	
	private void setType(@SuppressWarnings("rawtypes") Class type) {
		this.type = type;
	}

	@SuppressWarnings("rawtypes")
	private Class type;
	
	@Override
	@Basic
	public Type getResult() {
		return getGlobals().get(this.getName());
	}
	
	@Override
	public boolean typeCheck() {
		try {
			if (this.getResult().getClass() == this.getType())
				return true;
			else {
				System.out.println("Type check failed in Variable");
				return false;
			}
		} catch (NullPointerException exc) {
			System.out.println("Variable doesn't have access to globals");
			return true;
		}
	}
	
}
