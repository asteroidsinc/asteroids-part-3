package asteroids.model.programs.expressions;

import asteroids.exception.IllegalOperandException;
import asteroids.model.programs.types.*;

import asteroids.util.Util;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class EqualTo extends ComparisonExpression {

	public EqualTo(Expression left, Expression right)
			throws IllegalOperandException {
		super(left, right);
	}

	@Override
	public String getOperatorSymbol() {
		return "==";
	}

	@Override
	public Bool getResult() {
		
		try{
			return new Bool(Util.fuzzyEquals(((Doub) this.getLeftOperand().getResult()).getValue(), ((Doub) this.getRightOperand().getResult()).getValue()));
		}catch(Exception exc){
			System.out.println("failed to get equal to result");
			if(getLeftOperand().equals(getRightOperand()))
				return new Bool(true);
			return new Bool(false);
		}
	}
	
	@Override
	public boolean typeCheck() {
		if (getLeftOperand().getType() == getRightOperand().getType())
			return (this.getLeftOperand().typeCheck()) && (this.getRightOperand().typeCheck());
		if(super.typeCheck())
			return true;
		else {
			System.out.println("Type check failed in Equal To");
			return false;
		}
	}

}
