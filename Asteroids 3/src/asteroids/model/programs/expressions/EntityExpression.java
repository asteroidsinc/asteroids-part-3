package asteroids.model.programs.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import asteroids.model.programs.types.Doub;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class EntityExpression extends Expression {

	public EntityExpression(Expression subject) {
		this.subject = subject;
	}
	
	public boolean isValidSubject(Expression subject) {
		return (subject instanceof Self) || (subject instanceof Null) || ((subject instanceof Variable) 
						&& this.getGlobals().get(((Variable) subject).getName()).getClass() == Entity.class);
	}
	
	@Basic
	public Expression getSubject() {
		return this.subject;
	}
	
	private /*final*/ Expression subject;
	
	@Override
	public boolean hasAsSubExpression(Expression expression) {
		return expression == this;
	}

	@Override
	public boolean equals(Object other) {
		if ((other == null) || (getClass() != other.getClass()))
			return false;
		EntityExpression otherExpr = (EntityExpression) other;
		if (!this.getResult().equals(otherExpr.getResult()))
			return false;
		return true;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Doub.class;
	}
	
	@Override
	public boolean typeCheck() {
		return isValidSubject(this.getSubject());
	}

	@Override
	public void assignTo(SpaceObject spaceObject) {
		this.getSubject().assignTo(spaceObject);
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		getSubject().setProgram(program);
	}
	
}
