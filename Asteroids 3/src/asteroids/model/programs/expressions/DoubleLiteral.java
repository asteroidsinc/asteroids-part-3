package asteroids.model.programs.expressions;

import be.kuleuven.cs.som.annotate.*;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class DoubleLiteral extends Literal {

	public DoubleLiteral(double result) {
		this.result = result;
	}

	public DoubleLiteral() {
		// We must explicitly initialize the final instance variable value in
		// this constructor, either in a direct way or in an indirect way.
		this(0);
	}

	public final static DoubleLiteral ZERO = new DoubleLiteral();

	@Override
	@Basic @Immutable
	public Doub getResult() {
		return new Doub(result);
	}

	private final double result;

	@Override
	public boolean equals(Object other) {
		return (other instanceof DoubleLiteral)
				&& (this.getResult() == ((DoubleLiteral) other).getResult());
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Doub.class;
	}
	
	@Override
	public String toString() {
		return Double.toString(this.getResult().getValue());
	}
	
}
