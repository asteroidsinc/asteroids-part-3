package asteroids.model.programs.expressions;

import asteroids.model.SpaceObject;
import asteroids.model.programs.types.Entity;
import asteroids.model.programs.types.Type;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Null extends Expression {
	
	public Null() {
	}

	@Override
	public String toString() {
		return "null";
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof Null);
	}
	
	@Override
	public Type getResult() {
		return new Entity(null);
	}

	@Override
	public boolean hasAsSubExpression(Expression expression) {
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class getType() {
		return Entity.class;
	}

	@Override
	public boolean typeCheck() {
		return true;
	}
	
	@Override
	public void assignTo(SpaceObject spaceObject) {
		
	}

}
