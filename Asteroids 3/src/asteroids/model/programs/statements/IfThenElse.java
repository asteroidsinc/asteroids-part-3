package asteroids.model.programs.statements;

import java.util.ArrayList;
import java.util.List;

import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import asteroids.model.programs.expressions.Expression;
import be.kuleuven.cs.som.annotate.*;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class IfThenElse extends ConditionalStatement {

	public IfThenElse(Expression condition, Statement mainExecution, Statement elseExecution) {
		super(condition, mainExecution);
		elseStatements = dissecate(elseExecution);
	}
	
	@Override
	public void execute() {
		if(loopCompleted()) {
			  setActiveIndex(0);
			  System.out.println("Starting new if-run");
		}
		if(getActiveIndex() == 0)
			condition = ((Bool) this.getCondition().getResult()).getValue();
		if (condition)
			executeInnerStatements(getMainStatements());
		else
			executeInnerStatements(getElseStatements());
	}

	private void executeInnerStatements(List<Statement> statements) {
		while(getActiveIndex() < statements.size() && !getProgram().actionEncountered()) {
			System.out.println("executing one loop statement: "+getActiveIndex());
			statements.get(getActiveIndex()).execute();
			setActiveIndex(getActiveIndex() +1 );
		}
	}
	
	private boolean condition = false;
	
	@Override
	public void applyTo(SpaceObject spaceObject) {
		super.applyTo(spaceObject);
		for(Statement elseStatement: getElseStatements())
			elseStatement.applyTo(spaceObject);
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		for(Statement elseStatement: getElseStatements())
			elseStatement.setProgram(program);
	}
	
	@Basic @Immutable
	public ArrayList<Statement> getElseStatements() {
		return this.elseStatements;
	}
	
	private final ArrayList<Statement> elseStatements;
	
	@Override
	public boolean loopCompleted() {
		return getActiveIndex() == getActiveStatements().size();
	}
	
	@Basic
	public int getElseStatementIndex() {
		return this.elseStatementIndex;
	}
	
	protected void setElseStatementIndex(int index) {
		this.elseStatementIndex = index;
	}
	
	private int elseStatementIndex = 0;
	
	public int getActiveIndex() {
		if (condition)
			return getMainStatementIndex();
		else
			return getElseStatementIndex();
	}
	
	private void setActiveIndex(int index) {
		if (condition)
			setMainStatementIndex(index);
		else
			setElseStatementIndex(index);
	}
	
	public List<Statement> getActiveStatements() {
		if (condition)
			return getMainStatements();
		else
			return getElseStatements();
	}
	
	@Override
	public boolean typeCheck() {
		for (Statement statement: this.getElseStatements()) {
			if (!statement.typeCheck())
				return false;
		}
		return super.typeCheck();
	}

}
