package asteroids.model.programs.statements;

import asteroids.model.Ship;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class ForEachShip extends ForEach {

	public ForEachShip(String variable, Statement mainExecution) {
		super(variable, mainExecution);
	}

	@Override
	public void execute() {
		if (this.isValidMainExecution() && this.isExistingVariable()){
			for (Ship shipTemp: this.getWorld().getShips()){
				getGlobals().put(this.getVariable(), new Entity(shipTemp));
				this.applyTo(shipTemp);
				System.out.println("ForEachShip for-loop");
				for(Statement mainStatement: getMainStatements())
					mainStatement.execute();
			}
		}
	}

}
