package asteroids.model.programs.statements;

import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import asteroids.model.programs.expressions.Expression;
import asteroids.model.programs.types.Bool;
import be.kuleuven.cs.som.annotate.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class ConditionalStatement extends CurlyBracketsStatement {

	public ConditionalStatement(Expression condition, Statement mainExecution) {
		super(mainExecution);
		this.condition = condition;
	}
	
	@Basic @Immutable
	public Expression getCondition() {
		return this.condition;
	}
	
	private final Expression condition;
	
	@Override
	public void applyTo(SpaceObject spaceObject) {
		super.applyTo(spaceObject);
		getCondition().assignTo(spaceObject);
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		getCondition().setProgram(program);
	}
	
	@Basic
	public int getMainStatementIndex() {
		return this.mainStatementIndex;
	}
	
	protected void setMainStatementIndex(int index) {
		this.mainStatementIndex = index;
	}
	
	private int mainStatementIndex = 0;
	
	public abstract boolean loopCompleted();
	
	@Override
	public boolean typeCheck() {
		return super.typeCheck() && this.getCondition().getType() == Bool.class 
				&& this.getCondition().typeCheck();
	}

}
