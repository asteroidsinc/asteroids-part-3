package asteroids.model.programs.statements;

import asteroids.model.*;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class ForEachAny extends ForEach {

	public ForEachAny(String variable, Statement mainExecution) {
		super(variable, mainExecution);
	}

	@Override
	public void execute() {
		if (this.isValidMainExecution() && this.isExistingVariable()){
			for (SpaceObject anyTemp: this.getWorld().getSpaceObjects()){
				getGlobals().put(this.getVariable(), new Entity(anyTemp));
				this.applyTo(anyTemp);
				for(Statement mainStatement: getMainStatements())
					mainStatement.execute();
			}
		}
	}

}
