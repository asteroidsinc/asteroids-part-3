package asteroids.model.programs.statements;

import java.util.ArrayList;

import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import be.kuleuven.cs.som.annotate.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class CurlyBracketsStatement extends Statement {

	public CurlyBracketsStatement(Statement mainExecution) {
		mainStatements = dissecate(mainExecution);
	}
	
	 @Basic @Immutable
	  public ArrayList<Statement> getMainStatements() {
		  return this.mainStatements;
	  }

	private final ArrayList<Statement> mainStatements;
	
	
	@Override
	public void applyTo(SpaceObject spaceObject) {
		for (Statement mainStatement: getMainStatements())
			mainStatement.applyTo(spaceObject);
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		for (Statement mainStatement: getMainStatements())
			mainStatement.setProgram(program);
	}
	
	protected ArrayList<Statement> dissecate(Statement statement) {
		ArrayList<Statement> statements = new ArrayList<Statement>();
		if(statement instanceof Sequence) {
			for(Statement st : ((Sequence) statement).getStatements()) {
				statements.addAll(dissecate(st));
			}
		} else 
			statements.add(statement);	
		return statements;
	}

	@Override
	public boolean typeCheck() {
		for (Statement statement: this.getMainStatements()) {
			if (!statement.typeCheck())
				return false;
		}
		return true;
	}
	
}
