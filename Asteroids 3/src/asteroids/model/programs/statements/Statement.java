package asteroids.model.programs.statements;

import java.util.Map;

import asteroids.model.Ship;
import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import asteroids.model.programs.expressions.Expression;
import asteroids.model.programs.types.Type;
import be.kuleuven.cs.som.annotate.Basic;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class Statement {
	
	public abstract void execute();
	
	@Basic
	public Ship getShip() {
		return this.ship;
	}
	
	public void setShip(Ship ship) {
		this.ship = ship;
	}
	
	private Ship ship;
	
	public void applyTo(SpaceObject spaceObject) {
		if(spaceObject instanceof Ship)
			setShip((Ship) spaceObject);
	}
	
	public Map<String, Expression> getVariables() {
		return this.getProgram().getVariables();
	}
	
	public Map<String, Type> getGlobals() {
		return this.getProgram().getGlobals();
	}
	
	@Basic
	public Program getProgram() {
		return this.program;
	}
	
	public void setProgram(Program program) {
		this.program = program;
	}
	
	private Program program;
	
	public abstract boolean typeCheck();
}
