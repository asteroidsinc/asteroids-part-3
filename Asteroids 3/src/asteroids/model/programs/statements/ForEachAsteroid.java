package asteroids.model.programs.statements;

import asteroids.model.Asteroid;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class ForEachAsteroid extends ForEach {

	public ForEachAsteroid(String variable, Statement mainExecution) {
		super(variable, mainExecution);
	}

	@Override
	public void execute() {
		if (this.isValidMainExecution() && this.isExistingVariable()){
			for (Asteroid asteroidTemp: this.getWorld().getAsteroids()){
				System.out.println("ForEachAsteroid for-loop");
				getGlobals().put(this.getVariable(), new Entity(asteroidTemp));
				this.applyTo(asteroidTemp);
				for(Statement mainStatement: getMainStatements())
					mainStatement.execute();
			}
		}
	}
}
