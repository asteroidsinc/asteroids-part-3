package asteroids.model.programs.statements;

import asteroids.model.Ship;
import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import asteroids.model.programs.expressions.Self;
import asteroids.model.programs.types.Doub;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class Action extends Statement {

	public Action(Self self) {
		this.self = self;
	}

	public Self getSelf() {
		return this.self;
	}
	
	@Override
	public void applyTo(SpaceObject spaceObject) {
		this.getSelf().setSpaceObject((Ship) spaceObject);
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		this.getSelf().setProgram(program);
	}
	
	private final Self self;
	
	@Override
	public void execute() {
		getProgram().encounteredAction(true);
	}
	
	@Override
	public boolean typeCheck() {
		return self.getType() == Entity.class && self.typeCheck();
	}
	
}
