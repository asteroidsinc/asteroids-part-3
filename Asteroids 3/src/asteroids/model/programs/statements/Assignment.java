package asteroids.model.programs.statements;

import java.util.Date;

import be.kuleuven.cs.som.annotate.*;
import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import asteroids.model.programs.expressions.Expression;
import asteroids.model.programs.types.Doub;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Assignment extends Statement {

	public Assignment(String variableName, Expression value) {
		this.variableName = variableName;
		this.value = value;
	}
	
	@Basic @Immutable
	public String getVariableName() {
		return this.variableName;
	}
	
	@Basic 
	public Expression getValue() {
		return this.value;
	}
	
	private final String variableName;
	
	private Expression value;

	@Override
	public void execute() {
		this.getGlobals().put(getVariableName(), getValue().getResult());
		Date date = new Date();
		System.out.println("Assigned variable: "+getVariableName()+". Expected value: "+value.getResult()+ ".  Current time: "+ date.getTime());
	}
	
	@Override
	public void applyTo(SpaceObject	spaceObject) {
		this.getValue().assignTo(spaceObject);
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		this.getValue().setProgram(program);
	}
	
	@Override
	public boolean typeCheck() {
		try {
			return (value.getType() == this.getGlobals().get(variableName).getClass()) 
				&& value.typeCheck() && getVariables().get(variableName).typeCheck(); //misschien overkill
		} catch (NullPointerException exc) {
			System.out.println("Assignment doesn't have access to globals");
			return value.typeCheck();
		}
	}
	
}
