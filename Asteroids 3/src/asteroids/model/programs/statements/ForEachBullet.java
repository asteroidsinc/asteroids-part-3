package asteroids.model.programs.statements;

import asteroids.model.Bullet;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class ForEachBullet extends ForEach {

	public ForEachBullet( String variable, Statement mainExecution) {
		super(variable, mainExecution);
	}

	@Override
	public void execute() {
		if (this.isValidMainExecution() && this.isExistingVariable()){
			for (Bullet bulletTemp: this.getWorld().getBullets()){
				getGlobals().put(this.getVariable(), new Entity(bulletTemp));
				this.applyTo(bulletTemp);
				System.out.println("ForEachBullet for-loop");
				for(Statement mainStatement: getMainStatements())
					mainStatement.execute();
			}
		}
	}
}
