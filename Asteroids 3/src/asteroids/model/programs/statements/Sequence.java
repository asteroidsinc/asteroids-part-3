package asteroids.model.programs.statements;

import java.util.*;

import asteroids.model.SpaceObject;
import asteroids.model.programs.Program;
import be.kuleuven.cs.som.annotate.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Sequence extends Statement {

	public Sequence(List<Statement> statements) {
		this.statements = statements;
	}
	
	@Basic @Immutable
	public List<Statement> getStatements() {
		return this.statements;
	}

	private final List<Statement> statements;
	
	@Override
	public void execute() {
		while(i < this.getStatements().size() && !getProgram().actionEncountered()) {
			this.getStatements().get(i).execute();
			System.out.println("executing in seq in loop; statement: "+i);
			i++;
			if(i == getStatements().size())
				i = 0;
		}
	}
	
	private int i = 0;
	
	@Override
	public void applyTo(SpaceObject spaceObject) {
		for (Statement statement: this.getStatements())
			statement.applyTo(spaceObject);
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		for (Statement statement: this.getStatements())
			statement.setProgram(program);
	}
	
	@Override
	public boolean typeCheck() {
		for (Statement statement: this.getStatements()) {
			if (!statement.typeCheck())
				return false;
		}
		return true;
	}

}
