package asteroids.model.programs.statements;

import be.kuleuven.cs.som.annotate.*;
import asteroids.model.programs.Program;
import asteroids.model.programs.expressions.Expression;
import asteroids.model.programs.types.Type;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Print extends Statement {

	public Print(Expression expression) {
		this.expression = expression;
	}
	
	@Basic @Immutable
	public Expression getExpression() {
		return this.expression;
	}
	
	private final Expression expression;
	
	@Override
	public void execute() {
		this.result = expression.getResult();
		System.out.println(this.result);
	}
	
	private Type result;
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		getExpression().setProgram(program);
	}
	
	@Override
	public boolean typeCheck() {
		return expression.typeCheck();
	}

}
