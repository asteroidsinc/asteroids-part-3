package asteroids.model.programs.statements;

import asteroids.model.programs.expressions.Self;
import asteroids.model.Ship;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class EnableThruster extends Action {

	public EnableThruster(Self ship) {
		super(ship);
	}
	
	@Override
	public void execute() {
		super.execute();
		((Ship) this.getSelf().getSpaceObject()).setThrusterOn(true);
	}

}
