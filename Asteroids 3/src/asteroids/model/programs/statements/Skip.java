package asteroids.model.programs.statements;

import asteroids.model.programs.expressions.Self;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Skip extends Action {

	public Skip(Self ship) {
		super(ship);
	}

}
