package asteroids.model.programs.statements;

import be.kuleuven.cs.som.annotate.*;
import asteroids.model.SpaceObject;
import asteroids.model.World;
import asteroids.model.programs.Program;
import asteroids.model.programs.types.Entity;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public abstract class ForEach extends CurlyBracketsStatement {

	public ForEach(String variable, Statement mainExecution) {
		super(mainExecution);
		this.variable = variable;
	}
	
	public String getVariable() {
		return this.variable;
	}

	private String variable;
	
	@Basic
	public World getWorld() {
		return this.world;
	}
	
	private World world;

	public boolean isExistingVariable() {
		return (this.getGlobals().containsKey(variable));
	}
	
	public boolean isValidMainExecution() {
		for(Statement statement: getMainStatements()) {
			if(Action.class.isAssignableFrom(statement.getClass()))
				return false;
		}
		return true;
	}
	
	@Override
	public void applyTo(SpaceObject spaceObject) {
		super.applyTo(spaceObject);
		this.world = spaceObject.getWorld();
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		getVariables().get(getVariable()).setProgram(program);
	}
	
	@Override
	public boolean typeCheck() {
		return super.typeCheck() && this.getGlobals().get(variable).getClass() == Entity.class ;
	}
	
}
