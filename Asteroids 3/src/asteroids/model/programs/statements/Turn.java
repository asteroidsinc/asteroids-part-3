package asteroids.model.programs.statements;

import be.kuleuven.cs.som.annotate.*;
import asteroids.model.programs.Program;
import asteroids.model.programs.expressions.*;
import asteroids.model.Ship;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Turn extends Action {

	public Turn(Self ship, Expression angle) {
		super(ship);
		this.angle = angle;
	}

	@Basic @Immutable
	public Expression getAngle() {
		return this.angle;
	}
	
	private Expression angle;
	
	@Override
	public void execute() {
		super.execute();
		((Ship) this.getSelf().getSpaceObject()).turn(((Doub) this.getAngle().getResult()).getValue());
	}
	
	@Override
	public void setProgram(Program program) {
		super.setProgram(program);
		getAngle().setProgram(program);
	}
	
	@Override
	public boolean typeCheck() {
		return super.typeCheck() && angle.getType() == Doub.class && angle.typeCheck();
	}

}
