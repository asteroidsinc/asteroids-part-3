package asteroids.model.programs.statements;

import asteroids.model.SpaceObject;
import asteroids.model.programs.expressions.Expression;
import asteroids.model.programs.types.*;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class While extends ConditionalStatement {

	public While(Expression condition, Statement mainExecution) {
		super(condition, mainExecution);
	}

	@Override
	public void execute() {
		while(getMainStatementIndex() < getMainStatements().size() && !getProgram().actionEncountered()) {
				getMainStatements().get(getMainStatementIndex()).execute();
				setMainStatementIndex(getMainStatementIndex() +1 );
			  
			}
			if(getMainStatementIndex() == getMainStatements().size()) {
				  setMainStatementIndex(0);
				  System.out.println("Completed loop run");
			}
	}

	@Override
	public boolean loopCompleted() {
		return !(((Bool) this.getCondition().getResult()).getValue());
	}

	@Override
	public void applyTo(SpaceObject spaceObject) {
		for (Statement statement: this.getMainStatements())
			statement.applyTo(spaceObject);
	}
	
}
