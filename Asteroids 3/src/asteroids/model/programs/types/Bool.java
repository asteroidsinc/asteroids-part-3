package asteroids.model.programs.types;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Bool extends Type {

	public Bool(boolean value) {
		this.value = value;
	}
	
	public boolean getValue() {
		return this.value;
	}
	
	private final boolean value;

	@Override
	public String toString() {
		return Boolean.toString(getValue());
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof Bool)
			return getValue() == ((Bool) other).getValue();
		return false;
	}
	
}
