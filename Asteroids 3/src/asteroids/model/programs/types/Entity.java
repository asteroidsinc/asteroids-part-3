package asteroids.model.programs.types;

import asteroids.model.SpaceObject;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Entity extends Type {

	public Entity(SpaceObject spaceObject) {
		this.spaceObject = spaceObject;
	}
	
	public SpaceObject getValue() {
		return this.spaceObject;
	}
	
	private final SpaceObject spaceObject;
	
	@Override
	public String toString() {
		return this.getValue() + "";
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof Entity)
			return getValue() == ((Entity) other).getValue();
		return false;
	}
	
}
