package asteroids.model.programs.types;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 */
public class Doub extends Type {

	public Doub(double value) {
		this.value = value;
	}
	
	public double getValue() {
		return this.value;
	}
	
	private final double value;
	
	@Override
	public String toString() {
		return Double.toString(getValue());
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof Doub)
			return getValue() == ((Doub) other).getValue();
		return false;
	}

}
