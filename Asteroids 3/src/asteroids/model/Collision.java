package asteroids.model;

import asteroids.view.CollisionListener;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class for dealing with collisions, containing all methods affecting the participating object(s),
 * such as time to collision, collision position and resolving the collision.
 * 
 * @invar	The space objects of this collision must be valid space objects for a collision.
 * 			| hasProperSpaceObjects()
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 3.6
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-3
 *
 */
public abstract class Collision {
	
	/**
	 * Initialise the new collision with given first and second space objects,
	 * 
	 * @param 	objectOne
	 * 			The first space object of the new collision.
	 * @param 	objectTwo
	 * 			The first space object of the new collision.
	 * @post	The first space object of the new collision is equal to the given space object.
	 * 			| (new this).getObjectOne() == objectOne
	 * @post	The second space object of the new collision is equal to the given space object.
	 * 			| (new this).getObjectTwo() == objectTwo
	 */
	public Collision(SpaceObject objectOne, SpaceObject objectTwo) throws IllegalArgumentException {
		if(!isValidObjectOne(objectOne))
			throw new IllegalArgumentException();
		this.objectOne = objectOne;
		if(!isValidObjectTwo(objectTwo))
			throw new IllegalArgumentException();
		this.objectTwo = objectTwo;
	}
	
	/**
	 * Returns the first space object of the collision, which cannot be null.
	 * 
	 * @return 	The first space object of the collision.
	 * 			| result != null
	 */
	@Basic @Immutable
	public SpaceObject getObjectOne() {
		return this.objectOne;
	}
	
	/**
	 * Returns the second space object of the collision, which can be null.
	 * 
	 * @return 	The second space object of the collision.
	 */
	@Basic @Immutable
	public SpaceObject getObjectTwo() {
		return this.objectTwo;
	}
	
	/**
	 * Returns the remaining time until the collision occurs.
	 * 
	 * @return	The time until the collision occurs.
	 * 			| result >= 0
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public abstract double getTimeToCollision() throws IllegalStateException;	
	
	/**
	 * Returns the position at which the collision occurs.
	 * 
	 * @return	The position at which the collision occurs.
	 * @throws 	IllegalStateException
	 *			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public abstract double[] getCollisionPosition() throws IllegalStateException;
	
	/**
	 * Sends the collision data to the collision listener for a visual result.
	 * 
	 * @param 	collisionListener
	 * 			The collisionlistener that will process the data.
	 * @throws 	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public abstract void toCollisionListener(CollisionListener collisionListener) throws IllegalStateException;
	
	/**
	 * Constant storing the first space object of the collision.
	 */
	private final SpaceObject objectOne;
	
	/**
	 * Constant storing the second space object of the collision.
	 */
	private final SpaceObject objectTwo;

	/**
	 * Checks whether the collision has proper space objects attached to it.
	 * 
	 * @return	True if and only if the first space object of the collision is not null
	 * 			| result == (getObjectOne() != null)
	 */
	public boolean hasProperSpaceObjects() {
		return isValidObjectOne(getObjectOne()) && isValidObjectTwo(getObjectTwo());
	}
	
	public boolean isValidObjectOne(SpaceObject object) {
		return object != null && (object.isShip() ^ object.isAsteroid() ^ object.isBullet());
	}
	
	public abstract boolean isValidObjectTwo(SpaceObject object);
	
	public boolean isTerminated() {
		return this.isTerminated;
	}
	
	public void terminate() {
		this.isTerminated = true;
	}
	
	private boolean isTerminated;
	
	/**
	 * Resolves the collision between both objects.
	 * 
	 * @throws 	IllegalStateException
	 * Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public abstract void resolveCollision() throws IllegalStateException ;
}
