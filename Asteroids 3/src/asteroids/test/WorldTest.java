package asteroids.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asteroids.model.*;
import asteroids.util.Util;

/** 
  * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
  * @version 	3.6
  * @link		https://bitbucket.org/asteroidsinc/asteroids-part-3
  */
public class WorldTest {
	
	public World	world900_1440, world800_1280, world768_1024;
	
	public Ship		ship50_50_0_20_0_20_8E4,		ship250_250_40_min20_270_15_5E13,	ship800_500_0_0_0_40_5E15,
					ship512_384_0_0_0_40_5E15,		shipmin50_min250_20_30_60_20_6E12, 	ship100_100_30_min20_135_70_4E11,	
					ship250_210_0_0_0_20_6E10,		ship20_75_0_0_0_10_5E9;
	
	public Asteroid	asteroid100_100_25_50_60,		asteroid600_100_min30_min40_80, 	asteroidmin10_min60_20_10_10,
					asteroid75_75_min10_30_40,		asteroid500_90_50_min20_20,			asteroid300_233_10_0_20;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		world900_1440 = new World(900,1440);
		world800_1280 = new World(800, 1280);
		world768_1024 = new World(768, 1024);
		ship50_50_0_20_0_20_8E4 = new Ship(50,50,0,20,0,20,8E4);
		ship250_250_40_min20_270_15_5E13 = new Ship(250,250,40,-20,3*Math.PI/2,15,5E13);
		ship100_100_30_min20_135_70_4E11 = new Ship(100,100,30,-20,3*Math.PI/4,70,4E11);
		ship800_500_0_0_0_40_5E15 = new Ship(800,500,0,0,0,40,5E15);
		shipmin50_min250_20_30_60_20_6E12 = new Ship(-50,-250,20,30,Math.PI/3,20,6E12);
		ship20_75_0_0_0_10_5E9 = new Ship(20,75,0,0,0,10,5E9);
		ship250_210_0_0_0_20_6E10 = new Ship(250,210,0,0,0,20,6E10);
		ship512_384_0_0_0_40_5E15 = new Ship(1024/2.,768/ 2., 0, 0,  0, 40,5E15);
		asteroid300_233_10_0_20 = new Asteroid(300, 233, 10, 0, 20);
		asteroid100_100_25_50_60 = new Asteroid(100,100,25,50,60);
		asteroid600_100_min30_min40_80 = new Asteroid(600,100,-30,-40,80);
		asteroidmin10_min60_20_10_10 = new Asteroid(-10,-60,20,10,10);
		asteroid75_75_min10_30_40 = new Asteroid(75, 75, -10, 30, 40);
		asteroid500_90_50_min20_20 = new Asteroid(500,90,50,-20,20);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void Constructor_LegalCase() throws Exception {
		World world = new World(1500,1200);
		assert Util.fuzzyEquals(1500, world.getHeight());
		assert Util.fuzzyEquals(1200, world.getWidth());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalHeightCase() throws Exception {
		new World(Double.NaN,500);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalWidthCase() throws Exception {
		new World(600,-200);
	}
	
	@Test
	public final void isValidDimension_TrueCase() {
		assertTrue(world768_1024.isValidDimension(0));
		assertTrue(world768_1024.isValidDimension(55));
		assertTrue(world768_1024.isValidDimension(Double.MAX_VALUE));
		assertTrue(world768_1024.isValidDimension(1254));
	}
	
	@Test
	public final void isValidDimension_FalseCase() {
		assertFalse(world768_1024.isValidDimension(-100));
		assertFalse(world768_1024.isValidDimension(Double.POSITIVE_INFINITY));
		assertFalse(world768_1024.isValidDimension(Double.NaN));
	}
	
	@Test
	public final void terminate_LegalCase() throws Exception {
		assertFalse(world900_1440.isTerminated());
		world900_1440.addSpaceObject(ship250_250_40_min20_270_15_5E13);
		world900_1440.addSpaceObject(ship800_500_0_0_0_40_5E15);
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		world900_1440.addSpaceObject(asteroid600_100_min30_min40_80);
		assertTrue(world900_1440.hasAsSpaceObject(ship250_250_40_min20_270_15_5E13));
		assertTrue(world900_1440.hasAsSpaceObject(ship800_500_0_0_0_40_5E15));
		assertTrue(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertTrue(world900_1440.hasAsSpaceObject(asteroid600_100_min30_min40_80));
		ship250_250_40_min20_270_15_5E13.fireBullet();
		ship250_250_40_min20_270_15_5E13.turn(Math.PI/2);
		ship250_250_40_min20_270_15_5E13.fireBullet();
		world900_1440.terminate();
		assertFalse(world900_1440.hasAsSpaceObject(ship250_250_40_min20_270_15_5E13));
		assertFalse(world900_1440.hasAsSpaceObject(ship800_500_0_0_0_40_5E15));
		assertFalse(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertFalse(world900_1440.hasAsSpaceObject(asteroid600_100_min30_min40_80));
		assertTrue(world900_1440.isTerminated());
	}
	
	@Test
	public final void terminate_TerminatedCase() throws Exception {
		assertFalse(world900_1440.isTerminated());
		world900_1440.terminate();
		assertTrue(world900_1440.isTerminated());
		world900_1440.terminate();
		assertTrue(world900_1440.isTerminated());
	}
	
	@Test
	public final void addSpaceObject_LegalCase() throws Exception {
		world800_1280.addSpaceObject(ship50_50_0_20_0_20_8E4);
		assertTrue(world800_1280.hasAsSpaceObject(ship50_50_0_20_0_20_8E4));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void addSpaceObject_SpaceObjectNullCase() {
		world800_1280.addSpaceObject(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void addSpaceObject_WorldNullCase() {
		world900_1440.terminate();
		world900_1440.addSpaceObject(asteroid300_233_10_0_20);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void addSpaceObject_CannotHaveAsSpaceObjectCase() {
		world900_1440.addSpaceObject(shipmin50_min250_20_30_60_20_6E12);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void addSpaceObject_SpaceObjectInAnotherWorldCase() {
		world900_1440.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		world800_1280.addSpaceObject(ship100_100_30_min20_135_70_4E11);
	}

	@Test
	public final void addSpaceObject_OutOfBoundsCase() {
		world900_1440.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		ship100_100_30_min20_135_70_4E11.move(1.45);
		ship100_100_30_min20_135_70_4E11.turn(3*Math.PI/4);
		Bullet bullet = ship100_100_30_min20_135_70_4E11.fireBullet();
		assertFalse(world900_1440.hasAsSpaceObject(bullet));
		assertTrue(bullet.isTerminated());
	}
	
	@Test
	public final void removeSpaceObject_LegalCase() throws Exception {
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		world900_1440.addSpaceObject(ship250_250_40_min20_270_15_5E13);
		world900_1440.addSpaceObject(asteroid600_100_min30_min40_80);
		Bullet bullet = ship250_250_40_min20_270_15_5E13.fireBullet();
		assertTrue(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertTrue(world900_1440.hasAsSpaceObject(ship250_250_40_min20_270_15_5E13));
		assertTrue(world900_1440.hasAsSpaceObject(asteroid600_100_min30_min40_80));
		assertTrue(world900_1440.hasAsSpaceObject(bullet));
		assertEquals(1,world900_1440.getNbShips());
		assertEquals(2,world900_1440.getNbAsteroids());
		assertEquals(1,world900_1440.getNbBullets());
		world900_1440.removeSpaceObject(bullet);
		world900_1440.removeSpaceObject(asteroid100_100_25_50_60);
		world900_1440.removeSpaceObject(ship250_250_40_min20_270_15_5E13);
		world900_1440.removeSpaceObject(asteroid600_100_min30_min40_80);
		assertFalse(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertFalse(world900_1440.hasAsSpaceObject(ship250_250_40_min20_270_15_5E13));
		assertFalse(world900_1440.hasAsSpaceObject(asteroid600_100_min30_min40_80));
		assertFalse(world900_1440.hasAsSpaceObject(bullet));
		assertEquals(0,world900_1440.getNbShips());
		assertEquals(0,world900_1440.getNbAsteroids());
		assertEquals(0,world900_1440.getNbBullets());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void removeSpaceObject_NullCase() {
		world800_1280.removeSpaceObject(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void removeSpaceObject_NotHasAsSpaceObjectCase() throws Exception {
		world900_1440.removeSpaceObject(ship100_100_30_min20_135_70_4E11);
	}

	@Test
	public final void addShip_LegalCase() throws Exception {
		assertFalse(world900_1440.isTerminated());
		world900_1440.addSpaceObject(ship250_250_40_min20_270_15_5E13);		
		world900_1440.addSpaceObject(ship250_250_40_min20_270_15_5E13);	
	}
	
	@Test
	public final void addShip_OverlapAsteroidCase() throws Exception {
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		world900_1440.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		assertTrue(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertFalse(world900_1440.hasAsSpaceObject(ship100_100_30_min20_135_70_4E11));
		assertTrue(ship100_100_30_min20_135_70_4E11.isTerminated());
	}
	
	@Test
	public final void addShip_OverlapBulletCase() throws Exception {
		world800_1280.addSpaceObject(ship250_250_40_min20_270_15_5E13);
		Bullet bullet = ship250_250_40_min20_270_15_5E13.fireBullet();
		world800_1280.addSpaceObject(ship50_50_0_20_0_20_8E4);
		ship50_50_0_20_0_20_8E4.fireBullet();				
		world800_1280.addSpaceObject(ship250_210_0_0_0_20_6E10);
		assertTrue(world800_1280.hasAsSpaceObject(ship250_250_40_min20_270_15_5E13));
		assertFalse(world800_1280.hasAsSpaceObject(ship250_210_0_0_0_20_6E10));
		assertTrue(ship250_210_0_0_0_20_6E10.isTerminated());
		assertTrue(bullet.isTerminated());
	}
	
	@Test
	public final void addShip_OverlapShipCase() throws Exception  {
		world900_1440.addSpaceObject(ship50_50_0_20_0_20_8E4);
		world900_1440.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		assertTrue(world900_1440.hasAsSpaceObject(ship50_50_0_20_0_20_8E4));
		assertFalse(world900_1440.hasAsSpaceObject(ship100_100_30_min20_135_70_4E11));
		assertTrue(ship100_100_30_min20_135_70_4E11.isTerminated());
	}
	
	@Test
	public final void addAsteroid_LegalCase() throws Exception {
		assertFalse(world900_1440.isTerminated());
		world900_1440.addSpaceObject(asteroid600_100_min30_min40_80);	
		world900_1440.addSpaceObject(asteroid600_100_min30_min40_80);
	}
	
	@Test
	public final void addAsteroid_OverlapShipCase() throws Exception {
		world900_1440.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		assertTrue(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertFalse(world900_1440.hasAsSpaceObject(ship100_100_30_min20_135_70_4E11));
		assertTrue(ship100_100_30_min20_135_70_4E11.isTerminated());
	}
	
	@Test
	public final void addAsteroid_OverlapBulletCase() throws Exception {
		world800_1280.addSpaceObject(ship20_75_0_0_0_10_5E9);
		Bullet bullet = ship20_75_0_0_0_10_5E9.fireBullet();
		world800_1280.addSpaceObject(ship800_500_0_0_0_40_5E15);
		ship800_500_0_0_0_40_5E15.fireBullet();				
		world800_1280.addSpaceObject(asteroid75_75_min10_30_40);
		assertFalse(world800_1280.hasAsSpaceObject(asteroid75_75_min10_30_40));
		assertTrue(asteroid75_75_min10_30_40.isTerminated());
		assertTrue(bullet.isTerminated());
	}
	
	@Test
	public final void addAsteroid_OverlapAsteroidCase() throws Exception  {
		world900_1440.addSpaceObject(asteroid75_75_min10_30_40);
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		assertTrue(world900_1440.hasAsSpaceObject(asteroid75_75_min10_30_40));
		assertFalse(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertTrue(asteroid100_100_25_50_60.isTerminated());
	}
		
	@Test
	public final void addBullet_LegalCase() throws Exception {
		assertFalse(world900_1440.isTerminated());
		world900_1440.addSpaceObject(ship100_100_30_min20_135_70_4E11);		
		Bullet bullet = ship100_100_30_min20_135_70_4E11.fireBullet();
		assertTrue(world900_1440.hasAsSpaceObject(ship100_100_30_min20_135_70_4E11));
		assertTrue(world900_1440.hasAsSpaceObject(bullet));
	}
	
	@Test
	public final void addBullet_OverlapAsteroidCase() throws Exception {
		world900_1440.addSpaceObject(asteroid75_75_min10_30_40);
		world900_1440.addSpaceObject(ship20_75_0_0_0_10_5E9);
		Bullet bullet = ship20_75_0_0_0_10_5E9.fireBullet();
		assertFalse(world900_1440.hasAsSpaceObject(asteroid75_75_min10_30_40));
		assertFalse(world900_1440.hasAsSpaceObject(bullet));
		assertTrue(asteroid75_75_min10_30_40.isTerminated());
		assertTrue(bullet.isTerminated());
	}
	
	@Test
	public final void addBullet_OverlapBulletCase() throws Exception {
		assertFalse(world900_1440.isTerminated());
		world900_1440.addSpaceObject(ship100_100_30_min20_135_70_4E11);		
		Bullet bulletOne = ship100_100_30_min20_135_70_4E11.fireBullet();
		ship100_100_30_min20_135_70_4E11.turn(0.05);
		Bullet bulletTwo = ship100_100_30_min20_135_70_4E11.fireBullet();
		assertTrue(world900_1440.getNbBullets() == 0);
		assertTrue(world900_1440.hasAsSpaceObject(ship100_100_30_min20_135_70_4E11));
		assertFalse(world900_1440.hasAsSpaceObject(bulletOne));
		assertFalse(world900_1440.hasAsSpaceObject(bulletTwo));
		assertFalse(ship100_100_30_min20_135_70_4E11.isTerminated());
		assertTrue(bulletOne.isTerminated());
		assertTrue(bulletTwo.isTerminated());
	}
	
	@Test
	public final void addBullet_OverlapShipCase() {
		world800_1280.addSpaceObject(ship250_210_0_0_0_20_6E10);
		world800_1280.addSpaceObject(ship250_250_40_min20_270_15_5E13);
		Bullet bullet = ship250_250_40_min20_270_15_5E13.fireBullet();
		world800_1280.addSpaceObject(ship50_50_0_20_0_20_8E4);
		ship50_50_0_20_0_20_8E4.fireBullet();				
		assertTrue(world800_1280.hasAsSpaceObject(ship250_250_40_min20_270_15_5E13));
		assertFalse(world800_1280.hasAsSpaceObject(ship250_210_0_0_0_20_6E10));
		assertTrue(ship250_210_0_0_0_20_6E10.isTerminated());
		assertTrue(bullet.isTerminated());
	}
	
	@Test
	public final void addBullet_TooMuchBulletsFromShipCase() {
		world768_1024.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		ship100_100_30_min20_135_70_4E11.fireBullet();
		ship100_100_30_min20_135_70_4E11.turn(1);
		ship100_100_30_min20_135_70_4E11.fireBullet();
		ship100_100_30_min20_135_70_4E11.turn(1);
		ship100_100_30_min20_135_70_4E11.fireBullet();
		ship100_100_30_min20_135_70_4E11.turn(1);
		Bullet bullet = ship100_100_30_min20_135_70_4E11.fireBullet();
		assertFalse(world768_1024.hasAsSpaceObject(bullet));
	}
	
	@Test
	public final void removeBullet_LegalCase() {
		world800_1280.addSpaceObject(ship250_210_0_0_0_20_6E10);
		Bullet bullet = ship250_210_0_0_0_20_6E10.fireBullet();
		assertEquals(ship250_210_0_0_0_20_6E10.getNbOwnBulletsInWorld(), 1);
		world800_1280.removeSpaceObject(bullet);
		assertEquals(ship250_210_0_0_0_20_6E10.getNbOwnBulletsInWorld(), 0);
	}
	
	@Test
	public final void canHaveAnotherBulletFromSource_TrueCase() {
		world800_1280.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		assertTrue(world800_1280.canHaveAnotherBulletFromSource(ship100_100_30_min20_135_70_4E11));
	}
	
	@Test
	public final void canHaveAnotherBulletFromSource_TooManyCase() {
		world800_1280.addSpaceObject(ship100_100_30_min20_135_70_4E11);
		ship100_100_30_min20_135_70_4E11.fireBullet();
		world800_1280.evolve(0.1, null);
		ship100_100_30_min20_135_70_4E11.fireBullet();
		world800_1280.evolve(0.1, null);
		ship100_100_30_min20_135_70_4E11.fireBullet();
		assertFalse(world800_1280.canHaveAnotherBulletFromSource(ship100_100_30_min20_135_70_4E11));
	}
	
	@Test
	public final void canHaveAnotherBulletFromSource_NotInWorldCase() {
		assertFalse(world800_1280.canHaveAnotherBulletFromSource(ship100_100_30_min20_135_70_4E11));
	}
	
	@Test
	public final void hasAsSpaceObject_FalseCase() {
		assertFalse(world800_1280.hasAsSpaceObject(asteroid100_100_25_50_60));
	}
	
	@Test
	public final void canHaveAsSpaceObject_TrueCase() {
		assertTrue(world800_1280.canHaveAsSpaceObject(ship100_100_30_min20_135_70_4E11));
	}
	
	@Test
	public final void canHaveAsSpaceObject_ThisTerminatedCase() {
		world800_1280.terminate();
		assertFalse(world800_1280.canHaveAsSpaceObject(ship100_100_30_min20_135_70_4E11));
	}
	
	@Test
	public final void canHaveAsSpaceObject_SpaceObjectTerminatedCase() {
		asteroid100_100_25_50_60.explode();
		assertFalse(world800_1280.canHaveAsSpaceObject(asteroid100_100_25_50_60));
	}
	
	@Test
	public final void canHaveAsSpaceObject_OutOfBoundsCase() {
		assertFalse(world800_1280.canHaveAsSpaceObject(asteroidmin10_min60_20_10_10));
	}
	
		@Test
		public final void canHaveAsSpaceObject_SpaceObjectNullCase() {
			assertFalse(world900_1440.canHaveAsSpaceObject(null));
		}

	@Test
	public final void hasProperSpaceObjects_LegalCase() {
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		world900_1440.addSpaceObject(ship250_250_40_min20_270_15_5E13);
		world900_1440.addSpaceObject(asteroid600_100_min30_min40_80);
		@SuppressWarnings("unused")
		Bullet bullet = ship250_250_40_min20_270_15_5E13.fireBullet();
		assertTrue(world900_1440.hasProperSpaceObjects());
		world900_1440.removeSpaceObject(ship250_250_40_min20_270_15_5E13);
		assertTrue(world900_1440.hasProperSpaceObjects());
	}

	
	@Test
	public final void isWithinBounds_TrueCase() {
		assertTrue(world800_1280.isWithinBounds(ship100_100_30_min20_135_70_4E11.getPosition(),
				ship100_100_30_min20_135_70_4E11.getRadius()));
	}
	
	@Test
	public final void isWithinBounds_LeftFalseCase() {
		Asteroid asteroidTemp = new Asteroid(-10,50,0,0,15);
		assertFalse(world800_1280.isWithinBounds(asteroidTemp.getPosition(), asteroidTemp.getRadius()));
	}
	
	@Test
	public final void isWithinBounds_RightFalseCase() {
		Asteroid asteroidTemp = new Asteroid(1270,200,0,0,15);
		assertFalse(world800_1280.isWithinBounds(asteroidTemp.getPosition(), asteroidTemp.getRadius()));
	}
	
	@Test
	public final void isWithinBounds_BottomFalseCase() {
		Asteroid asteroidTemp = new Asteroid(50,-10,0,0,15);
		assertFalse(world800_1280.isWithinBounds(asteroidTemp.getPosition(), asteroidTemp.getRadius()));
	}
	
	@Test
	public final void isWithinBounds_UpFalseCase() {
		Asteroid asteroidTemp = new Asteroid(50,795,0,0,15);
		assertFalse(world800_1280.isWithinBounds(asteroidTemp.getPosition(), asteroidTemp.getRadius()));
	}
	
	@Test
	public final void evolve_LegalCase() {
		assertFalse(world900_1440.isTerminated());
		world900_1440.addSpaceObject(ship250_250_40_min20_270_15_5E13);
		world900_1440.addSpaceObject(ship800_500_0_0_0_40_5E15);
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		world900_1440.addSpaceObject(asteroid600_100_min30_min40_80);
		assertTrue(world900_1440.hasAsSpaceObject(ship250_250_40_min20_270_15_5E13));
		assertTrue(world900_1440.hasAsSpaceObject(ship800_500_0_0_0_40_5E15));
		assertTrue(world900_1440.hasAsSpaceObject(asteroid100_100_25_50_60));
		assertTrue(world900_1440.hasAsSpaceObject(asteroid600_100_min30_min40_80));
		Bullet bulletOne = ship250_250_40_min20_270_15_5E13.fireBullet();
		assertTrue(world900_1440.hasAsSpaceObject(bulletOne));
		world900_1440.evolve(0.1, null);
		assert Util.fuzzyEquals(ship250_250_40_min20_270_15_5E13.getXPosition(),254);
		assert Util.fuzzyEquals(ship250_250_40_min20_270_15_5E13.getYPosition(),248);
		world900_1440.evolve(4.9, null);
		assert Util.fuzzyEquals(asteroid600_100_min30_min40_80.getXPosition(), 450);
		assert Util.fuzzyEquals(asteroid600_100_min30_min40_80.getYPosition(), 260);
		for (int i = 0; i < 100; i++)
			world900_1440.evolve(1, null);
	}
	
	@Test
	public final void evolve_SimultaneousBounceCase() {
		world900_1440.addSpaceObject(asteroid75_75_min10_30_40);
		world900_1440.addSpaceObject(asteroid500_90_50_min20_20);
		for (int i = 0; i < 40; i++) {
			System.out.println(i);
			world900_1440.evolve(0.1, null);
		}
		assertTrue(world900_1440.hasAsSpaceObject(asteroid75_75_min10_30_40));
		assertTrue(world900_1440.hasAsSpaceObject(asteroid500_90_50_min20_20));
		assert Util.fuzzyEquals(asteroid75_75_min10_30_40.getXPosition(), 45);
		assert Util.fuzzyEquals(asteroid75_75_min10_30_40.getYPosition(), 195);
		assert Util.fuzzyEquals(asteroid500_90_50_min20_20.getXPosition(), 700);
		assert Util.fuzzyEquals(asteroid500_90_50_min20_20.getYPosition(), 30);
	}
	
	@Test
	public final void evolve_ShipThrustingCase() {
		world900_1440.addSpaceObject(ship20_75_0_0_0_10_5E9);
		ship20_75_0_0_0_10_5E9.setThrusterOn(true);
		world900_1440.evolve(4, null);
		assert !Util.fuzzyEquals(ship20_75_0_0_0_10_5E9.getXVelocity(), 0);
		assert Util.fuzzyEquals(ship20_75_0_0_0_10_5E9.getYVelocity(), 0);
	}
	
	@Test
	public final void evolve_BulletSourceCollision() {
		world768_1024.addSpaceObject(ship512_384_0_0_0_40_5E15);
		ship512_384_0_0_0_40_5E15.setThrusterOn(true);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		Bullet bullet = ship512_384_0_0_0_40_5E15.fireBullet();
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		world768_1024.evolve(0.2, null);
		assertTrue(world768_1024.hasAsSpaceObject(ship512_384_0_0_0_40_5E15));
		assertTrue(world768_1024.hasAsSpaceObject(bullet));
	}
	
	@Test
	public final void evolve_SinglePlayerGameCase() {
		world900_1440.addSpaceObject(asteroid100_100_25_50_60);
		world900_1440.addSpaceObject(asteroid600_100_min30_min40_80);
		world900_1440.addSpaceObject(ship800_500_0_0_0_40_5E15);
		world900_1440.evolve(9,null);
		assert !Util.fuzzyEquals(asteroid100_100_25_50_60.getXPosition(), 25);
		assert !Util.fuzzyEquals(asteroid100_100_25_50_60.getYPosition(), 50);
		assert !Util.fuzzyEquals(asteroid600_100_min30_min40_80.getXPosition(), -30);
		assert !Util.fuzzyEquals(asteroid600_100_min30_min40_80.getXPosition(), 40);
		world900_1440.evolve(29,null);
		world900_1440.evolve(3,null);
		assertFalse(ship800_500_0_0_0_40_5E15.isTerminated());
	}
	
	@Test
	public final void evolve_ParallelCollisionCase() {
		world800_1280.addSpaceObject(ship250_210_0_0_0_20_6E10);
		world800_1280.addSpaceObject(asteroid300_233_10_0_20);
		Bullet bullet = ship250_210_0_0_0_20_6E10.fireBullet();
		@SuppressWarnings("unused")
		double time = bullet.getTimeToCollision(asteroid300_233_10_0_20);
		world800_1280.evolve(0.5, null);
		assertFalse(world800_1280.hasAsSpaceObject(asteroid300_233_10_0_20));
		assertTrue(world800_1280.hasAsSpaceObject(ship250_210_0_0_0_20_6E10));
		assertFalse(world800_1280.hasAsSpaceObject(bullet));
	}

	@Test
	public final void evolve_EmptyWorldCase() {
		assertEquals(world800_1280.getNbSpaceObjects(), 0);
		world800_1280.evolve(1, null);
	}
	
}
